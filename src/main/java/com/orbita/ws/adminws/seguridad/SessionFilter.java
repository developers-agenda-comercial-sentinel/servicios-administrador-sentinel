package com.orbita.ws.adminws.seguridad;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.orbita.ws.adminws.model.beans.ErrorBean;
import com.orbita.ws.adminws.model.beans.LoginBean;
import com.orbita.ws.adminws.model.beans.SessionBean;
import com.orbita.ws.adminws.util.CustomRequestWrapper;
import com.orbita.ws.adminws.util.WsConstants;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@WebFilter
public class SessionFilter implements Filter {
	
	private FilterConfig filterConfig = null;
	
	@Value("${url.sentinel.login}")
	private String urlSentinelLogin;
	
	@Value("${url.sentinel.error}")
	private String urlSentinelError;
	
	RequestBody requestBody;
	Response responseClient;
	Request requestClient;
	JsonObject json;
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		
		String contextPath = httpServletRequest.getContextPath(); 
		String requestUri = httpServletRequest.getRequestURI();
		 if(requestUri.equalsIgnoreCase(contextPath + WsConstants.WS_VALIDAR_USUARIO)) {
			 request = new CustomRequestWrapper(httpServletRequest);
			 SessionBean sessionBean = new Gson().fromJson(request.getReader(), SessionBean.class);
			 
		     json = new JsonObject();
			 json.addProperty("Usuario", sessionBean.getUsuario());
			 json.addProperty("Password", sessionBean.getPassword());
			 json.addProperty("ValidaCookie", "N");
			 json.addProperty("Plataforma", "WEB");
			 json.addProperty("IdOrigen", "1");
			 json.addProperty("VersionId", "");
			 json.addProperty("VersOpc", "");
			 json.addProperty("Operador", "");
			 
			LoginBean loginBean = getResponse(json, urlSentinelLogin, LoginBean.class);
			if (loginBean.getCodigoWS().equals("0")) {
				httpServletResponse.addHeader("statusSession", "ok");  
				 
			} else {
				httpServletResponse.addHeader("statusSession", "ERROR_SESSION");
				json = new JsonObject();
				json.addProperty("CodigoWS", loginBean.getCodigoWS());
				ErrorBean errorBean = getResponse(json, urlSentinelError, ErrorBean.class);
				
				httpServletRequest.setAttribute("id", loginBean.getCodigoWS());
				httpServletRequest.setAttribute("message", errorBean.getErrorDes());
				request.getRequestDispatcher(WsConstants.WS_SEGURIDAD_NO_INGRESO).forward(httpServletRequest, httpServletResponse);
			}
			 
			chain.doFilter(request, response); 
		 } else {
			 chain.doFilter(request, response);
		 }
		
	}
	
    public void init(FilterConfig filterConfiguration) throws ServletException {
        this.filterConfig = filterConfiguration;
    }
 
    public void destroy() {
        this.filterConfig = null;
    }
	
	private <T> T getResponse(JsonObject json, String url, Class<T> clazz) throws IOException {
		OkHttpClient client = new OkHttpClient();
		requestBody = RequestBody.create(json.toString(), MediaType.parse("application/json"));
		 
		requestClient = new Request.Builder().url(url).post(requestBody).build();
		responseClient = client.newCall(requestClient).execute(); 
		
		return (T) new Gson().fromJson(responseClient.body().string(), clazz);
	}
}
