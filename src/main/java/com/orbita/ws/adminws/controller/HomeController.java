package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaBaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListDashCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.ListHomeAnalista;
import com.orbita.ws.adminws.model.services.HomeService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Home")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class HomeController {

    @Autowired
    private HomeService homeService;

    @PostMapping(WsConstants.WS_LISTAR_HOME_ACTIVIDAD)
    public ListDashActividadDTO listarActividadHome(){
        return homeService.execActividad();
    }

    @PostMapping(WsConstants.WS_LISTAR_HOME_BASE)
    public ListBaseDTO listarBaseHome(@RequestBody BaseRequest codActividad){
        return homeService.execBase(codActividad);
    }

    @PostMapping(WsConstants.WS_LISTAR_HOME_ANALISTAXBASE)
    public ListDashAnalistaBaseDTO listarAnalistaXBaseHome(@RequestBody ListAnalistaBaseRequest listAnalistaBaseRequest){
        return homeService.execAnalistaBase(listAnalistaBaseRequest);
    }

    @PostMapping(WsConstants.WS_LISTAR_HOME_CARTERA)
    public ListDashCarteraDTO listarCarteraHome(@RequestBody ListDashCarteraRequest listDashCartera){
        return homeService.execCartera(listDashCartera);
    }
    
    @PostMapping(WsConstants.WS_LISTAR_HOME_ANALISTA)
    public ListDashAnalistaDTO listarAnalistaHome(@RequestBody ListHomeAnalista listHomeAnalista){
        return homeService.execAnalista(listHomeAnalista);
    }

    @PostMapping(WsConstants.WS_LISTAR_HOME_TOPVISITA)
    public ListDashTopVisitaDTO listarTopVisitasHome(){
        return homeService.execTopVisita();
    }

    @PostMapping(WsConstants.WS_LISTAR_HOME_TOPEFECTIVA)
    public ListDashTopEfectivaDTO listarTopEfectivasHome(){
        return homeService.execTopEfectiva();
    }

    @PostMapping(WsConstants.WS_LISTAR_HOME_UBICACION)
    public ListAnalistaUbicacionDTO listarUbicacionesHome(){
        return homeService.execUbiAnalista();
    }
}
