package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.LoginDTO;
import com.orbita.ws.adminws.model.dto.request.LoginRequest;
import com.orbita.ws.adminws.model.services.LoginService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Login")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping(WsConstants.WS_VALIDAR_USUARIO)
    public LoginDTO validarUsuario (@RequestBody LoginRequest loginRequest) {
        return loginService.getUsuario(loginRequest);
    }

}
