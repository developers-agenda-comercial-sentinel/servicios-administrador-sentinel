package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.ListOportunidadDTO;
import com.orbita.ws.adminws.model.dto.request.OportunidadRequest;
import com.orbita.ws.adminws.model.services.OportunidadService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Oportunidad")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class OportunidadController {

    @Autowired
    private OportunidadService oportunidadService;

    @PostMapping(WsConstants.WS_LISTAR_OPORTUNIDADES)
    public ListOportunidadDTO listarOportunidades(@RequestBody OportunidadRequest request){
        return oportunidadService.listarOportunidades(request);
    }
}
