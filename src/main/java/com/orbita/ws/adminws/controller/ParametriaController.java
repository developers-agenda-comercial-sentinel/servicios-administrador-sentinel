package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.*;
import com.orbita.ws.adminws.model.services.ReaccionService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Parametria")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ParametriaController {

    @Autowired
    private ReaccionService reaccionService;

    @PostMapping(WsConstants.WS_LISTAR_REAC_CONTACTADO)
    public ListReaccionDTO listarContactado(){
        return reaccionService.findAllContact();
    }

    @PostMapping(WsConstants.WS_OBTENER_REAC_CONTACTADO)
    public ReacContactadoDTO obtenerContactado(@RequestBody BaseRequest codReaccion){
        return reaccionService.findByIdContact(codReaccion);
    }

    @PostMapping(WsConstants.WS_GUARDAR_REAC_CONTACTADO)
    public InsertDTO guardarContactado(@RequestBody ReaccionCSaveRequest reaccionCSaveRequest){
        return reaccionService.saveContact(reaccionCSaveRequest);
    }

    @PostMapping(WsConstants.WS_ACTUALIZAR_REAC_CONTACTADO)
    public BaseResponse actualizarContactado(@RequestBody ReaccionCUpdateRequest reaccionCUpdateRequest){
        return reaccionService.updateContact(reaccionCUpdateRequest);
    }

    @PostMapping(WsConstants.WS_ELIMINAR_REAC_CONTACTADO)
    public BaseResponse eliminarContactado(@RequestBody DeleteRequest request){
        return reaccionService.deleteContact(request);
    }

    @PostMapping(WsConstants.WS_LISTAR_REAC_NOCONTACTADO)
    public ListReaccionDTO listarNoContactado(){
        return reaccionService.findAllNoContact();
    }

    @PostMapping(WsConstants.WS_OBTENER_REAC_NOCONTACTADO)
    public ReacNoContactadoDTO obtenerNoContactado(@RequestBody BaseRequest codReaccion){
        return reaccionService.findByIdNoContact(codReaccion);
    }

    @PostMapping(WsConstants.WS_GUARDAR_REAC_NOCONTACTADO)
    public InsertDTO guardarNoContactado(@RequestBody ReaccionNCSaveRequest reaccionNCSaveRequest){
        return reaccionService.saveNoContact(reaccionNCSaveRequest);
    }

    @PostMapping(WsConstants.WS_ACTUALIZAR_REAC_NOCONTACTADO)
    public BaseResponse actualizarNoContactado(@RequestBody ReaccionNCUpdateRequest reaccionNCUpdateRequest){
        return reaccionService.updateNoContact(reaccionNCUpdateRequest);
    }

    @PostMapping(WsConstants.WS_ELIMINAR_REAC_NOCONTACTADO)
    public BaseResponse eliminarNoContactado(@RequestBody DeleteRequest request){
        return reaccionService.deleteNoContact(request);
    }
}
