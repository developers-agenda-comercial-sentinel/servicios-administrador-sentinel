package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.AnalistaDTO;
import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.services.AnalistaService;
import com.orbita.ws.adminws.model.services.SeguimientoAnalistaService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Analista")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class AnalistaController {

    @Autowired
    private AnalistaService analistaService;

    @Autowired
    private SeguimientoAnalistaService seguimientoAnalistaService;

    @PostMapping(WsConstants.WS_LISTAR_ANALISTA)
    public AnalistaDTO listarAnalistas(@RequestBody AnalistaRequest analistaRequest){
        return analistaService.listarAnalista(analistaRequest);
    }

    @PostMapping(WsConstants.WS_UBICACION_ANALISTA)
    public ListAnalistaUbicacionDTO obtenerUbicacionAnalista(@RequestBody AnalistaSentinelRequest request){
        return seguimientoAnalistaService.obtenerUbicacionAnalista(request);
    }

}
