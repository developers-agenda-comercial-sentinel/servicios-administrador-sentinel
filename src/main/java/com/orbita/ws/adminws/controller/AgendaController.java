package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListCalendarioDTO;
import com.orbita.ws.adminws.model.dto.ListCartAgendaDTO;
import com.orbita.ws.adminws.model.dto.ListRegAgendaCboDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.CalendarioRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraAgendaRequest;
import com.orbita.ws.adminws.model.dto.request.RegCitaRequest;
import com.orbita.ws.adminws.model.services.AgendaService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Agenda")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class AgendaController {

    @Autowired
    private AgendaService agendaService;

    @PostMapping(WsConstants.WS_LISTAR_CALENDARIO)
    public ListCalendarioDTO listarCalendario(@RequestBody CalendarioRequest request){
        return agendaService.execCalendario(request);
    }

    @PostMapping(WsConstants.WS_LISTAR_DISTACT_CBO)
    public ListRegAgendaCboDTO listarCboDistritoActividad(@RequestBody AnalistaSentinelRequest nroDocumento){
        return agendaService.execute(nroDocumento);
    }

    @PostMapping(WsConstants.WS_LISTAR_CARTERA_AGENDA)
    public ListCartAgendaDTO listarCarteraAgenda(@RequestBody CarteraAgendaRequest request){
        return agendaService.listarCartera(request);
    }

    @PostMapping(WsConstants.WS_AGENDAR_CITA)
    public BaseResponse registrarAgenda(@RequestBody RegCitaRequest request){
        return agendaService.registrarAgenda(request);
    }

}
