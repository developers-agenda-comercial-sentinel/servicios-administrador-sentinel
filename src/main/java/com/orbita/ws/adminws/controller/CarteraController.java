package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.AsignarCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.AsignarOportunidadRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraRequest;
import com.orbita.ws.adminws.model.services.CarteraService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Cartera")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class CarteraController {

    @Autowired
    private CarteraService carteraService;

    @PostMapping(WsConstants.WS_LISTAR_CARTERA)
    public CarteraDTO listarCartera(@RequestBody CarteraRequest carteraRequest){
        return carteraService.listarCartera(carteraRequest);
    }

    /*@PostMapping(WsConstants.WS_TRANSFERIR_CLIENTES)
    public BaseResponse transferirCliente(@RequestBody TransferirClienteRequest transferirClienteRequest){
        return carteraService.transferirCliente(transferirClienteRequest);
    }*/

    @PostMapping(WsConstants.WS_ASIGNAR_OPORTUNIDADES)
    public BaseResponse asignarOportunidad(@RequestBody AsignarOportunidadRequest asignarOportunidadRequest){
        return carteraService.asignarOportunidad(asignarOportunidadRequest);
    }

    @PostMapping(WsConstants.WS_ASIGNAR_CARTERA)
    public BaseResponse asignarCartera(@RequestBody AsignarCarteraRequest asignarCarteraRequest){
        return carteraService.asignarCartera(asignarCarteraRequest);
    }
}
