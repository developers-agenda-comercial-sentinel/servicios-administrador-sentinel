package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.ColorDTO;
import com.orbita.ws.adminws.model.dto.ListActividadDTO;
import com.orbita.ws.adminws.model.dto.RutasExcelDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.services.ActividadService;
import com.orbita.ws.adminws.model.services.CargaMasivaService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Common")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class CommonController {

    @Autowired
    private ActividadService actividadService;
    
	@Autowired
	private CargaMasivaService cargaMasivaService;

    @PostMapping(WsConstants.WS_CMN_ACTIVIDAD)
    public ListActividadDTO listarActividad(){
        return actividadService.listarActividades();
    }
    
   @PostMapping(WsConstants.WS_CMN_RUTAS_EXCEL) 
   public RutasExcelDTO listarRutasExcel(BaseRequest tipo) { 
	   return cargaMasivaService.listarRutasExcel(tipo); 
   }
	 
   @PostMapping(WsConstants.WS_CMN_COLORES) 
   public ColorDTO listarColores() { 
	   return actividadService.listarColores(); 
   }
	 
}
