package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.InsertDTO;
import com.orbita.ws.adminws.model.dto.ListPerfilDTO;
import com.orbita.ws.adminws.model.dto.PerfilDTO;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilSaveRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilUpdateRequest;
import com.orbita.ws.adminws.model.services.PerfilService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Perfil")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class PerfilController {

    @Autowired
    private PerfilService perfilService;

    @PostMapping(WsConstants.WS_LISTAR_PERFILES)
    public ListPerfilDTO listarPerfiles(){
        return perfilService.findAll();
    }

    @PostMapping(WsConstants.WS_OBTENER_PERFIL)
    public PerfilDTO obtenerPerfil(@RequestBody PerfilRequest perfilRequest){
        return perfilService.findById(perfilRequest);
    }

    @PostMapping(WsConstants.WS_GUARDAR_PERFIL)
    public InsertDTO guardarPerfil(@RequestBody PerfilSaveRequest perfilSaveRequest){
        return perfilService.save(perfilSaveRequest);
    }

    @PostMapping(WsConstants.WS_ACTUALIZAR_PERFIL)
    public BaseResponse actualizarPerfil(@RequestBody PerfilUpdateRequest perfilUpdateRequest){
        return perfilService.update(perfilUpdateRequest);
    }

    @PostMapping(WsConstants.WS_ELIMINAR_PERFIL)
    public BaseResponse eliminarPerfil(@RequestBody DeleteRequest deleteRequest){
        return perfilService.delete(deleteRequest);
    }
}
