package com.orbita.ws.adminws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.CargaCobranzaDTO;
import com.orbita.ws.adminws.model.dto.CargaInactivoDTO;
import com.orbita.ws.adminws.model.dto.CargaRecurrenteDTO;
import com.orbita.ws.adminws.model.dto.CargaReferidoDTO;
import com.orbita.ws.adminws.model.dto.ListCargaHistoricoDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.CargaCobranzaReq;
import com.orbita.ws.adminws.model.dto.request.CargaErradaRequest;
import com.orbita.ws.adminws.model.dto.request.CargaInactivoReq;
import com.orbita.ws.adminws.model.dto.request.CargaRecurrenteReq;
import com.orbita.ws.adminws.model.dto.request.CargaReferidoReq;
import com.orbita.ws.adminws.model.services.CargaMasivaService;
import com.orbita.ws.adminws.util.WsConstants;

import io.swagger.annotations.Api;

@RestController
@Api(tags = "Carga Masiva")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class CargaMasivaController {

	@Autowired
	private CargaMasivaService service;
	
	@PostMapping(WsConstants.WS_CARGAR_RECURRENTE)
	public BaseResponse cargaRecurrente(@RequestBody CargaRecurrenteReq cargaRecurrente) {
		return service.cargaRecurrente(cargaRecurrente);
	}
	
	@PostMapping(WsConstants.WS_CARGAR_COBRANZA)
	public BaseResponse cargaCobranza(@RequestBody CargaCobranzaReq cargaCobranza) {
		return service.cargaCobranza(cargaCobranza);
	}
	
	@PostMapping(WsConstants.WS_CARGAR_INACTIVO)
	public BaseResponse cargaInactivo(@RequestBody CargaInactivoReq cargaInactivo) {
		return service.cargaInactivo(cargaInactivo);
	}
	
	@PostMapping(WsConstants.WS_CARGAR_REFERIDO)
	public BaseResponse cargaReferido(@RequestBody CargaReferidoReq cargaReferido) {
		return service.cargaReferido(cargaReferido);
	}
	
	@PostMapping(WsConstants.WS_LISTAR_CARGA_HISTORICO)
	public ListCargaHistoricoDTO listarHistorico(@RequestBody BaseRequest codigoActividad) {
		return service.listarHistorico(codigoActividad);
	}
	
	@PostMapping(WsConstants.WS_CARGA_ERRADA_RECURRENTE)
	public CargaRecurrenteDTO cargaErradaRecurrente(@RequestBody CargaErradaRequest cargaObservadaRecurrente) {
		return service.cargaErradaRecurrente(cargaObservadaRecurrente);
	}
	
	@PostMapping(WsConstants.WS_CARGA_ERRADA_COBRANZA)
	public CargaCobranzaDTO cargaErradaCobranza(@RequestBody CargaErradaRequest cargaObservadaCobranza) {
		return service.cargaErradaCobranza(cargaObservadaCobranza);
	}
	
	@PostMapping(WsConstants.WS_CARGA_ERRADA_INACTIVO)
	public CargaInactivoDTO cargaErradaInactivo(@RequestBody CargaErradaRequest cargaObservadaInactivo) {
		return service.cargaErradaInactivo(cargaObservadaInactivo);
	}
	
	@PostMapping(WsConstants.WS_CARGA_ERRADA_REFERIDO)
	public CargaReferidoDTO cargaErradaReferido(@RequestBody CargaErradaRequest cargaObservadaReferido) {
		return service.cargaErradaReferido(cargaObservadaReferido);
	}
}
