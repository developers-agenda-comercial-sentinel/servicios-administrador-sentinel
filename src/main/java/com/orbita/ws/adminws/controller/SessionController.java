package com.orbita.ws.adminws.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.util.WsConstants;

@Controller
public class SessionController {

	@RequestMapping(value = WsConstants.WS_SEGURIDAD_NO_INGRESO)
	public ResponseEntity<Object> validarUsuario(HttpServletRequest request) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setCodVal(request.getAttribute("id").toString());
		baseResponse.setMensaje(request.getAttribute("message").toString());
		
		return new ResponseEntity<Object>(baseResponse, HttpStatus.FORBIDDEN);
	}
}
