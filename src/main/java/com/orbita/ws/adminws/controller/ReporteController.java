package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.ListReporteDTO;
import com.orbita.ws.adminws.model.dto.ListReporteDetalleDTO;
import com.orbita.ws.adminws.model.dto.ListReporteVisitasDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.ReporteDetalleRequest;
import com.orbita.ws.adminws.model.services.ReporteService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Reporte")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ReporteController {

    @Autowired
    private ReporteService reporteService;

    @PostMapping(WsConstants.WS_LISTAR_REPORTE)
    public ListReporteDTO listarReportes(@RequestBody AnalistaSentinelRequest nroDocumento){
        return reporteService.listarReportes(nroDocumento);
    }

    @PostMapping(WsConstants.WS_OBTENER_REPORTE)
    public ListReporteDetalleDTO listarReportes(@RequestBody ReporteDetalleRequest request){
        return reporteService.getReporte(request);
    }

    @PostMapping(WsConstants.WS_OBTENER_REP_VISITASREALIZADAS)
    public ListReporteVisitasDTO listarRepVisitasRealizadas(@RequestBody AnalistaSentinelRequest nroDocumento){
        return reporteService.listarRepVisitasRealizadas(nroDocumento);
    }

}
