package com.orbita.ws.adminws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.ListConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ListConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.request.ActividadUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.BaseSaveRequest;
import com.orbita.ws.adminws.model.dto.request.BaseUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.services.ConfiguracionService;
import com.orbita.ws.adminws.util.WsConstants;

import io.swagger.annotations.Api;

@RestController
@Api(tags = "Configuración")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ConfiguracionController {
	
	@Autowired
	private ConfiguracionService configuracionService;

	@PostMapping(WsConstants.WS_LISTAR_CONFIG_ACTIVIDAD)
	public ListConfigActividadDTO listarActividadConfig() {
		return configuracionService.findAllActivity();
	}
	
	@PostMapping(WsConstants.WS_OBTENER_CONFIG_ACTIVIDAD)
	public ConfigActividadDTO obtenerActividadConfig(@RequestBody BaseRequest request) {
		return configuracionService.findByIdActivity(request);
	}
	
	@PostMapping(WsConstants.WS_ACTUALIZAR_CONFIG_ACTIVIDAD)
	public BaseResponse actualizarActividadConfig(@RequestBody ActividadUpdateRequest request) {
		return configuracionService.updateActivity(request);
	}
	
	@PostMapping(WsConstants.WS_LISTAR_CONFIG_BASE)
	public ListConfigBaseDTO listarBaseConfig() {
		return configuracionService.findAllBase();
	}
	
	@PostMapping(WsConstants.WS_GUARDAR_CONFIG_BASE)
	public BaseResponse guardarBaseConfig(@RequestBody BaseSaveRequest request) {
		return configuracionService.saveBase(request);
	}
	
	@PostMapping(WsConstants.WS_ACTUALIZAR_CONFIG_BASE)
	public BaseResponse actualizarBaseConfig(@RequestBody BaseUpdateRequest request) {
		return configuracionService.updateBase(request);
	}
	
	@PostMapping(WsConstants.WS_OBTENER_CONFIG_BASE)
	public ConfigBaseDTO obtenerBaseConfig(@RequestBody BaseRequest request) {
		return configuracionService.findByIdBase(request);
	}
	
	@PostMapping(WsConstants.WS_ELIMINAR_CONFIG_BASE)
	public BaseResponse eliminarBaseConfig(@RequestBody DeleteRequest request) {
		return  configuracionService.deleteBase(request);
	}
}
