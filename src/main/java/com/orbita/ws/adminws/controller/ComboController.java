package com.orbita.ws.adminws.controller;

import com.orbita.ws.adminws.model.dto.ComboDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.services.ComboService;
import com.orbita.ws.adminws.util.WsConstants;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "Combo")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ComboController {

    @Autowired
    private ComboService comboService;

    @PostMapping(WsConstants.WS_CBO_AGENCIA)
    public ComboDTO cboAgencia(){
        return comboService.cboAgencia();
    }

    @PostMapping(WsConstants.WS_CBO_ANALISTAXAGENCIA)
    public ComboDTO cboAnalista(@RequestBody BaseRequest request){
        return comboService.cboAnalista(request);
    }
}
