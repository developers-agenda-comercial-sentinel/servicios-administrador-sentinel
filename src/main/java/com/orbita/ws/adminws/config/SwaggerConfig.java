package com.orbita.ws.adminws.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.orbita.ws.adminws"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .tags(new Tag("Login", "Controlador de acceso"),
                        new Tag("Combo", "Controlador de los combos"),
                        new Tag("Home", "Controlador del Home"),
                        new Tag("Perfil", "Controlador de los perfiles"),
                        new Tag("Cartera", "Controlador del módulo cartera"),
                        new Tag("Analista", "Controlador del módulo analista"),
                        new Tag("Agenda", "Controlador del módulo agenda"),
                        new Tag("Common", "Controlador de servicios comunes"),
                        new Tag("Oportunidad", "Controlador del oportunidad"),
                        new Tag("Reporte", "Controlador de reportes"),
                        new Tag("Parametria", "Controlador del módulo parametría"),
                        new Tag("Seguridad", "Controlador del módulo seguridad"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("ADMIN AGENDA SENTINEL")
                .description("Servicios web para el administrador de la Agenda Sentinel")
                .version("API 1.0.0")
                .contact(new Contact("Jorge León", "", "jcleonromero@gmail.com"))
                .build();
    }

}
