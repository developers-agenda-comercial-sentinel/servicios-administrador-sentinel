package com.orbita.ws.adminws.model.dto.request;

public class BaseSaveRequest extends InsRequest {
	
    private String base;
    private int codActividad;
    private int estado;
    
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public int getCodActividad() {
		return codActividad;
	}
	public void setCodActividad(int codActividad) {
		this.codActividad = codActividad;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}

}
