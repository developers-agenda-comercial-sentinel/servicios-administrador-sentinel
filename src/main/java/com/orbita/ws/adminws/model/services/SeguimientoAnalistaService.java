package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;

public interface SeguimientoAnalistaService {

    ListAnalistaUbicacionDTO obtenerUbicacionAnalista(AnalistaSentinelRequest request);
}
