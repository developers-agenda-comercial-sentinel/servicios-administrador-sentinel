package com.orbita.ws.adminws.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orbita.ws.adminws.util.SpConstants;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class ArrayActividadRequest implements SQLData {

    private int codActividad;

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    @JsonIgnore
    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_ACTIVIDAD;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        codActividad = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(codActividad);
    }
}
