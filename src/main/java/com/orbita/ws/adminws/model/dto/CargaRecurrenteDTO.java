package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaRecurrenteReq;

public class CargaRecurrenteDTO extends BaseResponse implements Serializable {

	private List<ArrayCargaRecurrenteReq> cargaRecurrente;

	public List<ArrayCargaRecurrenteReq> getCargaRecurrente() {
		return cargaRecurrente;
	}

	public void setCargaRecurrente(List<ArrayCargaRecurrenteReq> cargaRecurrente) {
		this.cargaRecurrente = cargaRecurrente;
	}
	
}
