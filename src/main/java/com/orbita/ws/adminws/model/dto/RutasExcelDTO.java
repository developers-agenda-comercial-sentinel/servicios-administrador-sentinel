package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class RutasExcelDTO extends BaseResponse implements Serializable{
	
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
