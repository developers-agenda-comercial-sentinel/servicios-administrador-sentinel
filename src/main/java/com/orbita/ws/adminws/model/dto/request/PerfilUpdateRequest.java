package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("REQUEST - Actualizar Perfil")
public class PerfilUpdateRequest extends UpdRequest{

    @ApiModelProperty(value = "Código Perfil", required = true)
    private int codPerfil;
    @ApiModelProperty(value = "Descripción Perfil", required = true)
    private String descripcion;
    @ApiModelProperty(value = "Estado Perfil", required = true)
    private int estado;
    @ApiModelProperty(value = "Listado de Opciones", required = true)
    private List<OpcionRequest> opciones;
    @ApiModelProperty(value = "Listado de Opciones a eliminar", required = true)
    private List<OpcionRequest> opcionesEliminar;

    public int getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(int codPerfil) {
        this.codPerfil = codPerfil;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public List<OpcionRequest> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionRequest> opciones) {
        this.opciones = opciones;
    }

	public List<OpcionRequest> getOpcionesEliminar() {
		return opcionesEliminar;
	}

	public void setOpcionesEliminar(List<OpcionRequest> opcionesEliminar) {
		this.opcionesEliminar = opcionesEliminar;
	}
    
}
