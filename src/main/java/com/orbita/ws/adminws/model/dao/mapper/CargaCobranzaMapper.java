package com.orbita.ws.adminws.model.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaCobranzaReq;

public class CargaCobranzaMapper implements RowMapper<ArrayCargaCobranzaReq>{

	@Override
	public ArrayCargaCobranzaReq mapRow(ResultSet rs, int rowNum) throws SQLException {
		ArrayCargaCobranzaReq bean = new ArrayCargaCobranzaReq();
		bean.setNomCliente(rs.getString("NOMBRE_CLIENTE"));
		bean.setApePaternoCliente(rs.getString("APEPATERNO_CLIENTE"));
		bean.setApeMaternoCliente(rs.getString("APEMATERNO_CLIENTE"));
		bean.setTipoDocumento(rs.getString("TIPO_DOCUMENTO"));
		bean.setNroDocCliente(rs.getString("NRO_DOCUMENTO"));
		bean.setNroTelefono1(rs.getString("NRO_TELEF_1"));
		bean.setNroTelefono2(rs.getString("NRO_TELEF_2"));
		bean.setCodBase(rs.getString("COD_BASE"));
		bean.setDirecDomicilio(rs.getString("DIRECCION_DOMICILIO"));
		bean.setRefDomicilio(rs.getString("REF_DOMICILIO"));
		bean.setUbigeoDomicilio(rs.getString("UBIGEO_DOMICILIO"));
		bean.setLatDomicilio(rs.getString("LATITUD_DOMICILIO"));
		bean.setLonDomicilio(rs.getString("LONGITUD_DOMICILIO"));
		bean.setDirecNegocio(rs.getString("DIRECCION_NEGOCIO"));
		bean.setRefNegocio(rs.getString("REF_NEGOCIO"));
		bean.setUbigeoNegocio(rs.getString("UBIGEO_NEGOCIO"));
		bean.setLatNegocio(rs.getString("LATITUD_NEGOCIO"));
		bean.setLonNegocio(rs.getString("LONGITUD_NEGOCIO"));
		bean.setNroDocAnalista(rs.getString("DNI_ANALISTA"));
		bean.setCorreo(rs.getString("CORREO"));
		bean.setUsuarioRegistro(rs.getString("USUARIO_REGISTRO"));
		bean.setFechaCita(rs.getString("FECHA_CITA"));
		bean.setEdad(rs.getString("EDAD"));
		bean.setDiasAtraso(rs.getString("DIAS_ATRASO"));
		bean.setMontoVencido(rs.getString("MONTO_VENCIDO"));
		bean.setNroInstancia(rs.getString("NRO_INSTANCIA"));
		bean.setNroTelefono3(rs.getString("NRO_TELEF_3"));
		bean.setFecRegistro(rs.getString("FEC_REGISTRO"));
		bean.setInfoAdicional(rs.getString("INFO_ADICIONAL"));
		
		return bean;
	}

}
