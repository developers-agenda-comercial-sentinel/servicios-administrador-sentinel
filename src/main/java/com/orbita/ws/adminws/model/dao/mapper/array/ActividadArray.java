package com.orbita.ws.adminws.model.dao.mapper.array;

import com.orbita.ws.adminws.model.dto.request.ArrayActividadRequest;
import com.orbita.ws.adminws.model.dto.request.ArrayBaseRequest;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import org.springframework.jdbc.core.support.AbstractSqlTypeValue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ActividadArray extends AbstractSqlTypeValue {

    private List<ArrayActividadRequest> values;

    public ActividadArray(List<ArrayActividadRequest> values) {
        this.values = values;
    }

    @Override
    protected Object createTypeValue(Connection connection, int i, String s) throws SQLException {
        oracle.jdbc.OracleConnection wrapperConnection = connection.unwrap(oracle.jdbc.OracleConnection.class);
        connection = wrapperConnection;
        ArrayDescriptor desc = new ArrayDescriptor(s, connection);

        return new ARRAY(desc, connection, values.toArray(new ArrayActividadRequest[values.size()]));
    }
}
