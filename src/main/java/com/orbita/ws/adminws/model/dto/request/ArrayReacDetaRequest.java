package com.orbita.ws.adminws.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orbita.ws.adminws.util.SpConstants;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class ArrayReacDetaRequest implements SQLData {

    private int codReaccion;
    private String reaccion;

    public int getCodReaccion() {
        return codReaccion;
    }

    public void setCodReaccion(int codReaccion) {
        this.codReaccion = codReaccion;
    }

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    @JsonIgnore
    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_REACCION;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        codReaccion = stream.readInt();
        reaccion = stream.readString();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(codReaccion);
        stream.writeString(reaccion);
    }
}
