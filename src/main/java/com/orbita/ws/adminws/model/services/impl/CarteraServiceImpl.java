package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.CarteraDAO;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.AsignarCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.AsignarOportunidadRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraRequest;
import com.orbita.ws.adminws.model.dto.request.TransferirClienteRequest;
import com.orbita.ws.adminws.model.services.CarteraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarteraServiceImpl implements CarteraService {

    @Autowired
    private CarteraDAO carteraDAO;

    @Override
    public CarteraDTO listarCartera(CarteraRequest request) {
        return carteraDAO.listarCartera(request);
    }

    @Override
    public BaseResponse transferirCliente(TransferirClienteRequest request) {
        return carteraDAO.transferirCliente(request);
    }

    @Override
    public BaseResponse asignarOportunidad(AsignarOportunidadRequest request) {
        return carteraDAO.asignarOportunidad(request);
    }

    @Override
    public BaseResponse asignarCartera(AsignarCarteraRequest request) {
        return carteraDAO.asignarCartera(request);
    }
}
