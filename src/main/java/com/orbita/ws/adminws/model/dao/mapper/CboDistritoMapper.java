package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.UbigeoBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CboDistritoMapper implements RowMapper<UbigeoBean> {

    @Override
    public UbigeoBean mapRow(ResultSet rs, int i) throws SQLException {
        UbigeoBean bean = new UbigeoBean();
        bean.setUbigeo(rs.getString("UBIGEO"));
        bean.setNombre(rs.getString("NOMBRE"));

        return bean;
    }

}
