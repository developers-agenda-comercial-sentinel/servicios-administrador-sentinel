package com.orbita.ws.adminws.model.dao.sp.configuracion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dao.mapper.BaseMapper;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.ListConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.BaseSaveRequest;
import com.orbita.ws.adminws.model.dto.request.BaseUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpBase {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall callSel;
	private SimpleJdbcCall callGet;
	private SimpleJdbcCall callIns;
	private SimpleJdbcCall callUpd;
	private SimpleJdbcCall callDel;
	
	// Inputs - Outputs
    SqlParameterSource in;
    Map<String, Object> out;
    
    @PostConstruct
	void init() {
		callSel = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_SEL_CONFIG_BASE)
				.declareParameters(
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR))
				.returningResultSet(SpConstants.O_CUR_BASE, new BaseMapper());
		
		callGet = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_GET_CONFIG_BASE)
				.declareParameters(
						new SqlParameter(SpConstants.I_COD_BASE, OracleTypes.VARCHAR),
						new SqlOutParameter(SpConstants.O_BASE, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_ACTIVO, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR));
		
		callIns = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_INS_CONFIG_BASE)
				.declareParameters(
                        new SqlParameter(SpConstants.I_DESCRIPCION, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_CODIGO_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ACTIVO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_REGISTRO, OracleTypes.VARCHAR),
						new SqlOutParameter(SpConstants.O_COD_MSG, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR));
		
		callUpd = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_UPD_CONFIG_BASE)
				.declareParameters(
                        new SqlParameter(SpConstants.I_CODIGO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_DESCRIPCION, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_CODIGO_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ACTIVO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
						new SqlOutParameter(SpConstants.O_COD_MSG, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR));
		
		callDel = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_DEL_CONFIG_BASE)
				.declareParameters(
                        new SqlParameter(SpConstants.I_CODIGO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
						new SqlOutParameter(SpConstants.O_COD_MSG, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR));
	}

	public ListConfigBaseDTO findAllBase() {
		ListConfigBaseDTO dto = new ListConfigBaseDTO();
		
		try {
			out = callSel.execute();
            dto.setLista((List)out.get(SpConstants.O_CUR_BASE));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
		} catch (Exception e) {
            System.err.println(e.getMessage());
		}
		
		return dto;
	}
	
	public ConfigBaseDTO findByIdBase(BaseRequest request) {
		ConfigBaseDTO response = new ConfigBaseDTO();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_COD_BASE, request.getCodigo());

        try {
            out = callGet.execute(in);
            response.setBase((String) out.get(SpConstants.O_BASE));
            response.setCodActividad((((BigDecimal) out.get(SpConstants.O_COD_ACTIVIDAD)).intValue()));
            response.setEstado((((BigDecimal) out.get(SpConstants.O_ACTIVO)).intValue()));
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
	
	public BaseResponse saveBase(BaseSaveRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_DESCRIPCION, request.getBase())
            .addValue(SpConstants.I_CODIGO_ACTIVIDAD, request.getCodActividad())
            .addValue(SpConstants.I_ACTIVO, request.getEstado())
            .addValue(SpConstants.I_USUARIO_REGISTRO, request.getUsuarioRegistro());

        try {
            out = callIns.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_MSG));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
	
	public BaseResponse updateBase(BaseUpdateRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_CODIGO, request.getCodBase())
            .addValue(SpConstants.I_DESCRIPCION, request.getBase())
            .addValue(SpConstants.I_CODIGO_ACTIVIDAD, request.getCodActividad())
            .addValue(SpConstants.I_ACTIVO, request.getEstado())
            .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callUpd.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_MSG));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
	
	public BaseResponse deleteBase(DeleteRequest request) {
		BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_CODIGO, request.getCodigo())
            .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callDel.execute(in);
            
            response.setCodVal((String) out.get(SpConstants.O_COD_MSG));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
}
