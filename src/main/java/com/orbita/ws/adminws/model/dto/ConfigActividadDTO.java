package com.orbita.ws.adminws.model.dto;

public class ConfigActividadDTO extends BaseResponse {
	
    private String actividad;
    private int codColor;
    private String Hexadecimalcolor;
    private int estado;
    
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public int getCodColor() {
		return codColor;
	}
	public void setCodColor(int codColor) {
		this.codColor = codColor;
	}
	public String getHexadecimalcolor() {
		return Hexadecimalcolor;
	}
	public void setHexadecimalcolor(String hexadecimalcolor) {
		Hexadecimalcolor = hexadecimalcolor;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
    
}
