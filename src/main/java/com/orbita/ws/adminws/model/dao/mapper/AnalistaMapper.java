package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.AnalistaBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnalistaMapper implements RowMapper<AnalistaBean> {
    @Override
    public AnalistaBean mapRow(ResultSet rs, int i) throws SQLException {
        AnalistaBean analistaBean = new AnalistaBean();
        analistaBean.setCodUsuario(rs.getLong("COD_USUARIO"));
        analistaBean.setNombre(rs.getString("NOMBRES"));
        analistaBean.setApePaterno(rs.getString("APE_PATERNO"));
        analistaBean.setApeMaterno(rs.getString("APE_MATERNO"));
        analistaBean.setNroDocumento(rs.getString("NRO_DOCUMENTO"));
        analistaBean.setCodAgencia(rs.getString("COD_AGENCIA"));
        analistaBean.setNomAgencia(rs.getString("AGENCIA"));
        return analistaBean;
    }
}
