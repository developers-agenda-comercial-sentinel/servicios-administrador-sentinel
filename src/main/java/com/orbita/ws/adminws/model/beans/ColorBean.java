package com.orbita.ws.adminws.model.beans;

public class ColorBean {

	private int codColor;
	private String hexadecimal;
	
	public int getCodColor() {
		return codColor;
	}
	public void setCodColor(int codColor) {
		this.codColor = codColor;
	}
	public String getHexadecimal() {
		return hexadecimal;
	}
	public void setHexadecimal(String hexadecimal) {
		this.hexadecimal = hexadecimal;
	}
	
}
