package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.LoginDAO;
import com.orbita.ws.adminws.model.dao.sp.seguridad.SpLogin;
import com.orbita.ws.adminws.model.dto.LoginDTO;
import com.orbita.ws.adminws.model.dto.request.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginDAOImpl implements LoginDAO {

    @Autowired
    private SpLogin spLogin;

    @Override
    public LoginDTO getUsuario(LoginRequest usuario) {
        return spLogin.execute(usuario);
    }
}
