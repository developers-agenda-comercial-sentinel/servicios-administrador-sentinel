package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.UsuarioBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper implements RowMapper<UsuarioBean> {

    @Override
    public UsuarioBean mapRow(ResultSet rs, int i) throws SQLException {
        UsuarioBean bean = new UsuarioBean();

        bean.setCodUsuario(rs.getInt("COD_USUARIO"));
        bean.setApePaterno(rs.getString("APE_PATERNO"));
        bean.setApeMaterno(rs.getString("APE_MATERNO"));
        bean.setNombre(rs.getString("NOMBRES"));
        bean.setTipoDocumento(rs.getInt("TIPO_DOCUMENTO"));
        bean.setNroDocumento(rs.getString("NRO_DOCUMENTO"));
        bean.setEmail(rs.getString("EMAIL"));
        bean.setTelefono(rs.getString("TELEFONO"));
        bean.setCodPerfil(rs.getInt("COD_PERFIL"));
        bean.setPerfil(rs.getString("PERFIL"));
        bean.setEstado(rs.getInt("ESTADO"));
        
        return bean;
    }
}
