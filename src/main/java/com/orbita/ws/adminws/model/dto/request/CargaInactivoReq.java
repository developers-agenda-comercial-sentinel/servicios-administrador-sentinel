package com.orbita.ws.adminws.model.dto.request;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaInactivoReq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Carga Inactivo")
public class CargaInactivoReq implements Serializable{
	
	@ApiModelProperty(value = "Nombre del Archivo")
	private String nomArchivo;
	
	@ApiModelProperty(value = "Lista de Inactivo")
	List<ArrayCargaInactivoReq> cargaInactivo;

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public List<ArrayCargaInactivoReq> getCargaInactivo() {
		return cargaInactivo;
	}

	public void setCargaInactivo(List<ArrayCargaInactivoReq> cargaInactivo) {
		this.cargaInactivo = cargaInactivo;
	}

}
