package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.OpcionBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OpcionXUsuarioMapper implements RowMapper<OpcionBean> {

    @Override
    public OpcionBean mapRow(ResultSet rs, int i) throws SQLException {
        OpcionBean bean = new OpcionBean();
        bean.setCodOpcion(rs.getInt("COD_OPCION"));
        bean.setCodPadre(rs.getInt("COD_PADRE"));
        bean.setOrden(rs.getInt("ORDEN"));
        bean.setDescripcion(rs.getString("DESCRIPCION"));

        return bean;
    }
}
