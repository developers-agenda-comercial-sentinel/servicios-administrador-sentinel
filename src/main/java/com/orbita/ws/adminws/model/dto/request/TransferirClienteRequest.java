package com.orbita.ws.adminws.model.dto.request;

import com.orbita.ws.adminws.model.beans.GestionBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("REQUEST - Transferir Cliente")
public class TransferirClienteRequest extends UpdRequest {

    @ApiModelProperty(value = "Código de analista", required = true)
    private int codAnalista;
    @ApiModelProperty(value = "Lista de gestiones", required = true)
    private List<GestionBean> gestiones;

    public int getCodAnalista() {
        return codAnalista;
    }

    public void setCodAnalista(int codAnalista) {
        this.codAnalista = codAnalista;
    }

    public List<GestionBean> getGestiones() {
        return gestiones;
    }

    public void setGestiones(List<GestionBean> gestiones) {
        this.gestiones = gestiones;
    }
}
