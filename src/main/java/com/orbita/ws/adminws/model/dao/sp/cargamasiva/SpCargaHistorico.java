package com.orbita.ws.adminws.model.dao.sp.cargamasiva;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dao.mapper.CargaHistoricaMapper;
import com.orbita.ws.adminws.model.dto.ListCargaHistoricoDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpCargaHistorico {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall jdbcCallRecCob;
	private SimpleJdbcCall jdbcCallInacRef;
	
	@PostConstruct
	void init() {
		jdbcCallRecCob = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_SEL_CARGA_HISTORICA)
				.declareParameters(
						new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_CARGA, new CargaHistoricaMapper(true));
		
		jdbcCallInacRef = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_SEL_CARGA_HISTORICA)
				.declareParameters(
						new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_CARGA, new CargaHistoricaMapper(false));
	}
	
	public ListCargaHistoricoDTO execute(BaseRequest request) {
		ListCargaHistoricoDTO dto = new ListCargaHistoricoDTO();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodigo());
        Map<String, Object> out;
        try {
        	if (request.getCodigo() == 1 || request.getCodigo() == 2) {
        		out = jdbcCallRecCob.execute(in);
        	} else {
        		out = jdbcCallInacRef.execute(in);
        	}
            dto.setListaHistorico((List) out.get(SpConstants.O_CUR_CARGA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
	
}
