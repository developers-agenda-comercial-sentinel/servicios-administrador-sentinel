package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.ComboBean;
import com.orbita.ws.adminws.model.beans.UbigeoBean;

import java.io.Serializable;
import java.util.List;

public class ListRegAgendaCboDTO extends BaseResponse implements Serializable {

    List<UbigeoBean> distritos;
    List<ComboBean> actividades;

    public List<UbigeoBean> getDistritos() {
        return distritos;
    }

    public void setDistritos(List<UbigeoBean> distritos) {
        this.distritos = distritos;
    }

    public List<ComboBean> getActividades() {
        return actividades;
    }

    public void setActividades(List<ComboBean> actividades) {
        this.actividades = actividades;
    }
}
