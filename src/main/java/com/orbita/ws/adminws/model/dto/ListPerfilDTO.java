package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.PerfilBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Lista Perfil")
public class ListPerfilDTO extends BaseResponse implements Serializable {
    private List<PerfilBean> perfiles;

    public List<PerfilBean> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<PerfilBean> perfiles) {
        this.perfiles = perfiles;
    }
}
