package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.InsertDTO;
import com.orbita.ws.adminws.model.dto.ListPerfilDTO;
import com.orbita.ws.adminws.model.dto.PerfilDTO;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilSaveRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilUpdateRequest;

public interface PerfilService {

    ListPerfilDTO findAll();
    PerfilDTO findById(PerfilRequest request);
    InsertDTO save(PerfilSaveRequest request);
    BaseResponse update(PerfilUpdateRequest perfilBean);
    BaseResponse delete(DeleteRequest perfilBean);
}
