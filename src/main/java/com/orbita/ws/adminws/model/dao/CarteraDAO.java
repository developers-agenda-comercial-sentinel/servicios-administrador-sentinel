package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.AsignarCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.AsignarOportunidadRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraRequest;
import com.orbita.ws.adminws.model.dto.request.TransferirClienteRequest;

public interface CarteraDAO {

    CarteraDTO listarCartera(CarteraRequest request);
    BaseResponse transferirCliente(TransferirClienteRequest request);
    BaseResponse asignarOportunidad(AsignarOportunidadRequest request);
    BaseResponse asignarCartera(AsignarCarteraRequest request);
}
