package com.orbita.ws.adminws.model.dto.request;

public class ActividadUpdateRequest extends UpdRequest{

    private int codActividad;
	private String actividad;
    private int codColor;
    private int estado;
    
	public int getCodActividad() {
		return codActividad;
	}
	public void setCodActividad(int codActividad) {
		this.codActividad = codActividad;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public int getCodColor() {
		return codColor;
	}
	public void setCodColor(int codColor) {
		this.codColor = codColor;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
    
}
