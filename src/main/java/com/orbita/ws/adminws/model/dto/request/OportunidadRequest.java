package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("REQUEST - Oportunidad")
public class OportunidadRequest {

    @ApiModelProperty(value = "Valor a buscar", allowableValues="Nombres, Apellidos" ,required = true)
    private String busqueda;
    @ApiModelProperty(value = "Código de Ubigeo", required = true)
    private String ubigeo;
    @ApiModelProperty(value = "Lista de Bases", required = true)
    private List<ArrayBaseRequest> bases;
    @ApiModelProperty(value = "Lista de Actividades", required = true)
    private List<ArrayActividadRequest> actividades;
    @ApiModelProperty(value = "Número de página", required = true)
    private int pageNumber;
    @ApiModelProperty(value = "Cantidad de Registros por página", required = true)
    private int pageSize;


    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }
    
    public String getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}

	public List<ArrayBaseRequest> getBases() {
        return bases;
    }

    public void setBases(List<ArrayBaseRequest> bases) {
        this.bases = bases;
    }

    public List<ArrayActividadRequest> getActividades() {
        return actividades;
    }

    public void setActividades(List<ArrayActividadRequest> actividades) {
        this.actividades = actividades;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
