package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaReferidoReq;

public class CargaReferidoDTO extends BaseResponse implements Serializable {
	
	private List<ArrayCargaReferidoReq> cargaReferido;

	public List<ArrayCargaReferidoReq> getCargaReferido() {
		return cargaReferido;
	}

	public void setCargaReferido(List<ArrayCargaReferidoReq> cargaReferido) {
		this.cargaReferido = cargaReferido;
	}

}
