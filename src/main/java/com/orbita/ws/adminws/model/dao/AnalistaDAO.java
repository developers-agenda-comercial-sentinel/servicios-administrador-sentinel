package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.AnalistaDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaRequest;

public interface AnalistaDAO {

    AnalistaDTO listarAnalistas(AnalistaRequest analistaRequest);
}
