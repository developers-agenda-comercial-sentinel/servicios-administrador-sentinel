package com.orbita.ws.adminws.model.dao.sp.cartera;

import com.orbita.ws.adminws.model.dao.mapper.array.CarteraArray;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.request.AsignarCarteraRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;

@Repository
public class SpAsignarCartera {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_INS_ASIGNAR_CARTERA)
                .declareParameters(
                        new SqlParameter(SpConstants.I_ARR_CARTERA, OracleTypes.ARRAY, SpConstants.TYPE_CARTERA_TAB),
                        new SqlParameter(SpConstants.I_COD_USUARIO_REGISTRO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );
    }

    public BaseResponse execute(AsignarCarteraRequest request) {
        BaseResponse dto = new BaseResponse();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_ARR_CARTERA, new CarteraArray(request.getCartera()))
                .addValue(SpConstants.I_COD_USUARIO_REGISTRO, request.getUsuarioRegistro());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
}
