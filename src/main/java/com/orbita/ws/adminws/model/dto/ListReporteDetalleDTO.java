package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListReporteDetalleDTO extends BaseResponse implements Serializable {

    List<ReporteDetalleDTO> reportes;

    public List<ReporteDetalleDTO> getReportes() {
        return reportes;
    }

    public void setReportes(List<ReporteDetalleDTO> reportes) {
        this.reportes = reportes;
    }
}
