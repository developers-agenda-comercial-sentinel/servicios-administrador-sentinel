package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.UsuarioBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel("DTO - Usuario")
public class UsuarioDTO extends BaseResponse implements Serializable {

    private UsuarioBean usuario;

    public UsuarioBean getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioBean usuario) {
        this.usuario = usuario;
    }
}
