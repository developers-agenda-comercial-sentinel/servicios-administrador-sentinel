package com.orbita.ws.adminws.model.dto.request.arrayRequest;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orbita.ws.adminws.util.SpConstants;

public class ArrayCargaRecurrenteReq implements SQLData{

	private String nroDocCliente;
	private String tipoDocumento;
	private String nombreCliente;
	private String apePaternoCliente;
	private String apeMaternoCliente;
	private String codBase;
	private String nroTelefono1;
	private String direcDomicilio;
	private String refDomicilio;
	private String ubigeoDomicilio;
	private String latDomicilio;
	private String lonDomicilio;
	private String direcNegocio;
	private String refNegocio;
	private String ubigeoNegocio;
	private String latNegocio;
	private String lonNegocio;
	private String nroDocAnalista;
	private String usuarioRegistro;
	private String edad;
	private String segmentacion;
	private String nroEntSfTot;
	private String nroCuotasPendientes;
	private String fechaCita;
	private String correo;
	private String nroTelefono2;
	private String nroTelefono3;
	private String montoSfTot;
	private String fecRegistro;
	private String precalifica;
	private String infoAdicional;
	
	public String getNroDocCliente() {
		return nroDocCliente;
	}

	public void setNroDocCliente(String nroDocCliente) {
		this.nroDocCliente = nroDocCliente;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getApePaternoCliente() {
		return apePaternoCliente;
	}

	public void setApePaternoCliente(String apePaternoCliente) {
		this.apePaternoCliente = apePaternoCliente;
	}

	public String getApeMaternoCliente() {
		return apeMaternoCliente;
	}

	public void setApeMaternoCliente(String apeMaternoCliente) {
		this.apeMaternoCliente = apeMaternoCliente;
	}

	public String getCodBase() {
		return codBase;
	}

	public void setCodBase(String codBase) {
		this.codBase = codBase;
	}

	public String getNroTelefono1() {
		return nroTelefono1;
	}

	public void setNroTelefono1(String nroTelefono1) {
		this.nroTelefono1 = nroTelefono1;
	}

	public String getDirecDomicilio() {
		return direcDomicilio;
	}

	public void setDirecDomicilio(String direcDomicilio) {
		this.direcDomicilio = direcDomicilio;
	}

	public String getRefDomicilio() {
		return refDomicilio;
	}

	public void setRefDomicilio(String refDomicilio) {
		this.refDomicilio = refDomicilio;
	}

	public String getUbigeoDomicilio() {
		return ubigeoDomicilio;
	}

	public void setUbigeoDomicilio(String ubigeoDomicilio) {
		this.ubigeoDomicilio = ubigeoDomicilio;
	}

	public String getLatDomicilio() {
		return latDomicilio;
	}

	public void setLatDomicilio(String latDomicilio) {
		this.latDomicilio = latDomicilio;
	}

	public String getLonDomicilio() {
		return lonDomicilio;
	}

	public void setLonDomicilio(String lonDomicilio) {
		this.lonDomicilio = lonDomicilio;
	}

	public String getDirecNegocio() {
		return direcNegocio;
	}

	public void setDirecNegocio(String direcNegocio) {
		this.direcNegocio = direcNegocio;
	}

	public String getRefNegocio() {
		return refNegocio;
	}

	public void setRefNegocio(String refNegocio) {
		this.refNegocio = refNegocio;
	}

	public String getUbigeoNegocio() {
		return ubigeoNegocio;
	}

	public void setUbigeoNegocio(String ubigeoNegocio) {
		this.ubigeoNegocio = ubigeoNegocio;
	}

	public String getLatNegocio() {
		return latNegocio;
	}

	public void setLatNegocio(String latNegocio) {
		this.latNegocio = latNegocio;
	}

	public String getLonNegocio() {
		return lonNegocio;
	}

	public void setLonNegocio(String lonNegocio) {
		this.lonNegocio = lonNegocio;
	}

	public String getNroDocAnalista() {
		return nroDocAnalista;
	}

	public void setNroDocAnalista(String nroDocAnalista) {
		this.nroDocAnalista = nroDocAnalista;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getSegmentacion() {
		return segmentacion;
	}

	public void setSegmentacion(String segmentacion) {
		this.segmentacion = segmentacion;
	}

	public String getNroEntSfTot() {
		return nroEntSfTot;
	}

	public void setNroEntSfTot(String nroEntSfTot) {
		this.nroEntSfTot = nroEntSfTot;
	}

	public String getNroCuotasPendientes() {
		return nroCuotasPendientes;
	}

	public void setNroCuotasPendientes(String nroCuotasPendientes) {
		this.nroCuotasPendientes = nroCuotasPendientes;
	}

	public String getFechaCita() {
		return fechaCita;
	}

	public void setFechaCita(String fechaCita) {
		this.fechaCita = fechaCita;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNroTelefono2() {
		return nroTelefono2;
	}

	public void setNroTelefono2(String nroTelefono2) {
		this.nroTelefono2 = nroTelefono2;
	}

	public String getNroTelefono3() {
		return nroTelefono3;
	}

	public void setNroTelefono3(String nroTelefono3) {
		this.nroTelefono3 = nroTelefono3;
	}

	public String getMontoSfTot() {
		return montoSfTot;
	}

	public void setMontoSfTot(String montoSfTot) {
		this.montoSfTot = montoSfTot;
	}

	public String getFecRegistro() {
		return fecRegistro;
	}

	public void setFecRegistro(String fecRegistro) {
		this.fecRegistro = fecRegistro;
	}

	public String getPrecalifica() {
		return precalifica;
	}

	public void setPrecalifica(String precalifica) {
		this.precalifica = precalifica;
	}

	public String getInfoAdicional() {
		return infoAdicional;
	}

	public void setInfoAdicional(String infoAdicional) {
		this.infoAdicional = infoAdicional;
	}

	@JsonIgnore
	@Override
	public String getSQLTypeName() throws SQLException {
		return SpConstants.TYPE_LOAD_RECURRENTE;
	}

	@Override
	public void readSQL(SQLInput stream, String typeName) throws SQLException {
		nroDocCliente = stream.readString();
		tipoDocumento = stream.readString();
		nombreCliente = stream.readString();
		apePaternoCliente = stream.readString();
		apeMaternoCliente = stream.readString();
		codBase = stream.readString();
		nroTelefono1 = stream.readString();
		direcDomicilio = stream.readString();
		refDomicilio = stream.readString();
		ubigeoDomicilio = stream.readString();
		latDomicilio = stream.readString();
		lonDomicilio = stream.readString();
		direcNegocio = stream.readString();
		refNegocio = stream.readString();
		ubigeoNegocio = stream.readString();
		latNegocio = stream.readString();
		lonNegocio = stream.readString();
		nroDocAnalista = stream.readString();
		usuarioRegistro = stream.readString();
		edad = stream.readString();
		segmentacion = stream.readString();
		nroEntSfTot = stream.readString();
		nroCuotasPendientes = stream.readString();
		fechaCita = stream.readString();
		correo = stream.readString();
		nroTelefono2 = stream.readString();
		nroTelefono3 = stream.readString();
		montoSfTot = stream.readString();
		fecRegistro = stream.readString();
		precalifica = stream.readString();
		infoAdicional = stream.readString();
	}

	@Override
	public void writeSQL(SQLOutput stream) throws SQLException {
		stream.writeString(nroDocCliente);
		stream.writeString(tipoDocumento);
		stream.writeString(nombreCliente);
		stream.writeString(apePaternoCliente);
		stream.writeString(apeMaternoCliente);
		stream.writeString(codBase);
		stream.writeString(nroTelefono1);
		stream.writeString(direcDomicilio);
		stream.writeString(refDomicilio);
		stream.writeString(ubigeoDomicilio);
		stream.writeString(latDomicilio);
		stream.writeString(lonDomicilio);
		stream.writeString(direcNegocio);
		stream.writeString(refNegocio);
		stream.writeString(ubigeoNegocio);
		stream.writeString(latNegocio);
		stream.writeString(lonNegocio);
		stream.writeString(nroDocAnalista);
		stream.writeString(usuarioRegistro);
		stream.writeString(edad);
		stream.writeString(segmentacion);
		stream.writeString(nroEntSfTot);
		stream.writeString(nroCuotasPendientes);
		stream.writeString(fechaCita);
		stream.writeString(correo);
		stream.writeString(nroTelefono2);
		stream.writeString(nroTelefono3);
		stream.writeString(montoSfTot);
		stream.writeString(fecRegistro);
		stream.writeString(precalifica);
		stream.writeString(infoAdicional);
	}

}
