package com.orbita.ws.adminws.model.dto.request;

public class UpdRequest {
    private String usuarioModificacion;

    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }
}
