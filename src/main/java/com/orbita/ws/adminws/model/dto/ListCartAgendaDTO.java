package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListCartAgendaDTO extends BaseResponse implements Serializable {

    List<CarteraAgendaDTO> lista;

    public List<CarteraAgendaDTO> getLista() {
        return lista;
    }

    public void setLista(List<CarteraAgendaDTO> lista) {
        this.lista = lista;
    }
}
