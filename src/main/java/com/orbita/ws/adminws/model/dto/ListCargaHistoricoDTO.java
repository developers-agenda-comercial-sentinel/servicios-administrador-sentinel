package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListCargaHistoricoDTO extends BaseResponse implements Serializable{

	private List<CargaHistoricoDTO> listaHistorico;

	public List<CargaHistoricoDTO> getListaHistorico() {
		return listaHistorico;
	}

	public void setListaHistorico(List<CargaHistoricoDTO> listaHistorico) {
		this.listaHistorico = listaHistorico;
	}
	
}
