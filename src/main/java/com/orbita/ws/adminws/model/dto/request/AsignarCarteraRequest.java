package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel("REQUEST - Asignar Cartera")
public class AsignarCarteraRequest extends InsRequest implements Serializable {

    @ApiModelProperty(value = "Lista de clientes")
    List<ArrayCarteraRequest> cartera;

    public List<ArrayCarteraRequest> getCartera() {
        return cartera;
    }

    public void setCartera(List<ArrayCarteraRequest> cartera) {
        this.cartera = cartera;
    }
}
