package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.beans.BaseBean;

public class ListConfigBaseDTO extends BaseResponse implements Serializable {

	private List<BaseBean> lista;

    public List<BaseBean> getLista() {
        return lista;
    }

    public void setLista(List<BaseBean> lista) {
        this.lista = lista;
    }
}
