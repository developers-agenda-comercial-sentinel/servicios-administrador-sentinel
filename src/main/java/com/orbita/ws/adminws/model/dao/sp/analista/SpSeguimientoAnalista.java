package com.orbita.ws.adminws.model.dao.sp.analista;

import com.orbita.ws.adminws.model.dao.mapper.SeguimientoAnalistaMapper;
import com.orbita.ws.adminws.model.dto.AnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository
public class SpSeguimientoAnalista {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_GET_UBICACION_ANALISTA)
                .declareParameters(
                		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_SEGUIMIENTO_ANALISTA, new SeguimientoAnalistaMapper());
    }

    public ListAnalistaUbicacionDTO execute(AnalistaSentinelRequest request) {
        ListAnalistaUbicacionDTO dto = new ListAnalistaUbicacionDTO();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_CODUSUARIO, request.getCodigo());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setUbicaciones((List) out.get(SpConstants.O_CUR_SEGUIMIENTO_ANALISTA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
}
