package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.DashTopEfectivaDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashTopEfectivaMapper implements RowMapper<DashTopEfectivaDTO> {

    @Override
    public DashTopEfectivaDTO mapRow(ResultSet rs, int i) throws SQLException {
        DashTopEfectivaDTO dto = new DashTopEfectivaDTO();
        dto.setAnalista(rs.getString("ANALISTA"));
        dto.setEfectivas(rs.getInt("EFECTIVAS"));
        return dto;
    }
}
