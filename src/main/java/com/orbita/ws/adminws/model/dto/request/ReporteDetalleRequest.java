package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Listar Reportes Detalle")
public class ReporteDetalleRequest {

    @ApiModelProperty(value = "Código Analista", required = true)
    private int codAnalista;
    @ApiModelProperty(value = "Nombre del Reporte", required = true)
    private String nomReporte;

	public int getCodAnalista() {
		return codAnalista;
	}

	public void setCodAnalista(int codAnalista) {
		this.codAnalista = codAnalista;
	}

	public String getNomReporte() {
        return nomReporte;
    }

    public void setNomReporte(String nomReporte) {
        this.nomReporte = nomReporte;
    }
}
