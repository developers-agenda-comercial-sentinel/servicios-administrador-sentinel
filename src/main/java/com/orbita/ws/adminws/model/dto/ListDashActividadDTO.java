package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListDashActividadDTO extends BaseResponse implements Serializable {

    private List<DashActividadDTO> lista;

    public List<DashActividadDTO> getLista() {
        return lista;
    }

    public void setLista(List<DashActividadDTO> lista) {
        this.lista = lista;
    }
}
