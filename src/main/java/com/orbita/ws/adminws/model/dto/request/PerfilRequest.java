package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Perfil")
public class PerfilRequest {

    @ApiModelProperty(value = "Código de perfil", required = true)
    private int codPerfil;

    public int getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(int codPerfil) {
        this.codPerfil = codPerfil;
    }
}
