package com.orbita.ws.adminws.model.dao.sp.parametria;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dao.mapper.ColorMapper;
import com.orbita.ws.adminws.model.dto.ColorDTO;
import com.orbita.ws.adminws.model.dto.ListCargaHistoricoDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpListarColor {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall jdbcCall;
	
	@PostConstruct
	void init() {
		jdbcCall= new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_SEL_COLOR)
				.declareParameters(
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_COLOR, new ColorMapper());
		
	}
	
	public ColorDTO execute() {
		ColorDTO dto = new ColorDTO();
        
        try {
        	Map<String, Object>	out = jdbcCall.execute();
            dto.setColores((List) out.get(SpConstants.O_CUR_COLOR));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
	
}
