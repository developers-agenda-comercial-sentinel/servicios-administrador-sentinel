package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel("REQUEST - Actualizar Reaccion No Contactado")
public class ReaccionNCUpdateRequest extends UpdRequest {

    private int codReaccion;
    private String reaccion;
    private int activo;

    public int getCodReaccion() {
        return codReaccion;
    }

    public void setCodReaccion(int codReaccion) {
        this.codReaccion = codReaccion;
    }

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
