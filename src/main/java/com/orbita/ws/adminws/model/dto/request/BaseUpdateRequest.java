package com.orbita.ws.adminws.model.dto.request;

public class BaseUpdateRequest extends UpdRequest{
	
	private int codBase;
    private String base;
    private int codActividad;
    private int estado;
    
	public int getCodBase() {
		return codBase;
	}
	public void setCodBase(int codBase) {
		this.codBase = codBase;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public int getCodActividad() {
		return codActividad;
	}
	public void setCodActividad(int codActividad) {
		this.codActividad = codActividad;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
    
}
