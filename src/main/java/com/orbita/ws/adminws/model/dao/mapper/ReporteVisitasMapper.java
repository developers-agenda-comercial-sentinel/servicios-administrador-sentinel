package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.ReporteVisitasDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReporteVisitasMapper implements RowMapper<ReporteVisitasDTO> {

    @Override
    public ReporteVisitasDTO mapRow(ResultSet rs, int i) throws SQLException {
        ReporteVisitasDTO dto = new ReporteVisitasDTO();
        dto.setTipoResultado(rs.getString("TIPO"));
        dto.setEnero(rs.getInt("ENERO"));
        dto.setFebrero(rs.getInt("FEBRERO"));
        dto.setMarzo(rs.getInt("MARZO"));
        dto.setAbril(rs.getInt("ABRIL"));
        dto.setMayo(rs.getInt("MAYO"));
        dto.setJunio(rs.getInt("JUNIO"));
        dto.setJulio(rs.getInt("JULIO"));
        dto.setAgosto(rs.getInt("AGOSTO"));
        dto.setSeptiembre(rs.getInt("SEPTIEMBRE"));
        dto.setOctubre(rs.getInt("OCTUBRE"));
        dto.setNoviembre(rs.getInt("NOVIEMBRE"));
        dto.setDiciembre(rs.getInt("DICIEMBRE"));
        return dto;
    }
}
