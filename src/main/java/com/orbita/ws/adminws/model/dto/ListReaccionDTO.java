package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.ReaccionBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Lista Reacción")
public class ListReaccionDTO extends BaseResponse implements Serializable {

    private List<ReaccionBean> reacciones;

    public List<ReaccionBean> getReacciones() {
        return reacciones;
    }

    public void setReacciones(List<ReaccionBean> reacciones) {
        this.reacciones = reacciones;
    }
}
