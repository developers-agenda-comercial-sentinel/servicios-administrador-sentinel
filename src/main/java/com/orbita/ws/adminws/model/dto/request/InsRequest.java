package com.orbita.ws.adminws.model.dto.request;

public class InsRequest {
    private String usuarioRegistro;

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }
}
