package com.orbita.ws.adminws.model.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.orbita.ws.adminws.model.dto.CargaHistoricoDTO;

public class CargaHistoricaMapper implements RowMapper<CargaHistoricoDTO>{

	private Boolean determine;
	
	public CargaHistoricaMapper(Boolean determine) {
		this.determine = determine;
	}
	
	@Override
	public CargaHistoricoDTO mapRow(ResultSet rs, int i) throws SQLException {
		CargaHistoricoDTO dto = new CargaHistoricoDTO();
		dto.setCodigo(rs.getInt("COD_CARGA"));
		dto.setNomArchivo(rs.getString("NOM_ARCHIVO"));
		dto.setNroClientes(rs.getInt("TOTAL_CLIENTES"));
		if (determine == true) {
			dto.setNroAnalistas(rs.getInt("TOTAL_ANALISTAS"));			
		}
		dto.setFecRegistro(rs.getString("FEC_REGISTRO"));
		dto.setNroValidos(rs.getInt("VALIDOS"));
		dto.setNroObservados(rs.getInt("OBSERVADOS"));
		
		return dto;
	}

}
