package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.CalendarioDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CalendarioMapper implements RowMapper<CalendarioDTO> {
    @Override
    public CalendarioDTO mapRow(ResultSet rs, int i) throws SQLException {
        CalendarioDTO dto = new CalendarioDTO();
        dto.setFechaCita(rs.getString("FECHA_CITA"));
        dto.setCantidad(rs.getInt("CANTIDAD_CITA"));

        return dto;
    }
}
