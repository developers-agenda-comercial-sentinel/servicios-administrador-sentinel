package com.orbita.ws.adminws.model.dao.sp.cartera;

import com.orbita.ws.adminws.model.beans.ActividadBean;
import com.orbita.ws.adminws.model.beans.BaseBean;
import com.orbita.ws.adminws.model.dao.mapper.ActividadMapper;
import com.orbita.ws.adminws.model.dao.mapper.BaseMapper;
import com.orbita.ws.adminws.model.dto.ActividadDTO;
import com.orbita.ws.adminws.model.dto.ListActividadDTO;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class SpActividad {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callActividad;
    private SimpleJdbcCall callBase;

    @PostConstruct
    void init() {
        callActividad = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_ACTIVIDAD)
                .declareParameters(
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_ACTIVIDAD, new ActividadMapper());

        callBase = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_BASE)
                .declareParameters(
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_BASE, new BaseMapper());
    }

    public ListActividadDTO execute() {
        ListActividadDTO dto = new ListActividadDTO();
        List<ActividadDTO> listaDTO = new ArrayList<>();
        List<ActividadBean> listaA;
        List<BaseBean> listaB;
        ActividadDTO aDTO;

        try {

            Map<String, Object> outActividad = callActividad.execute();
            listaA = (List)outActividad.get(SpConstants.O_CUR_ACTIVIDAD);

            for (ActividadBean a : listaA) {

                Map<String, Object> outBase = callBase.execute();
                listaB = (List)outBase.get(SpConstants.O_CUR_BASE);

                listaB = listaB.stream().filter(b -> b.getActividad().equals(a.getActividad())).collect(Collectors.toList());
                
                aDTO = new ActividadDTO();
                aDTO.setCodActividad(a.getCodActividad());
                aDTO.setActividad(a.getActividad());
                aDTO.setCodColor(a.getCodColor());
                aDTO.setBases(listaB);

                listaDTO.add(aDTO);
            }

            dto.setActividades(listaDTO);
            dto.setCodVal((String) outActividad.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) outActividad.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;

    }
}
