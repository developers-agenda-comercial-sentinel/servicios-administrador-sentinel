package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.ReporteDAO;
import com.orbita.ws.adminws.model.dto.ListReporteDTO;
import com.orbita.ws.adminws.model.dto.ListReporteDetalleDTO;
import com.orbita.ws.adminws.model.dto.ListReporteVisitasDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.ReporteDetalleRequest;
import com.orbita.ws.adminws.model.services.ReporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReporteServiceImpl implements ReporteService {

    @Autowired
    private ReporteDAO reporteDAO;

    @Override
    public ListReporteDTO listarReportes(AnalistaSentinelRequest request) {
        return reporteDAO.listarReportes(request);
    }

    @Override
    public ListReporteDetalleDTO getReporte(ReporteDetalleRequest request) {
        return reporteDAO.getReporte(request);
    }

    @Override
    public ListReporteVisitasDTO listarRepVisitasRealizadas(AnalistaSentinelRequest request) {
        return reporteDAO.listarRepVisitasRealizadas(request);
    }
}
