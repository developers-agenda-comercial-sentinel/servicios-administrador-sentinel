package com.orbita.ws.adminws.model.dao.sp.agenda;

import com.orbita.ws.adminws.model.dao.mapper.CalendarioMapper;
import com.orbita.ws.adminws.model.dao.mapper.IndicadorCalendMapper;
import com.orbita.ws.adminws.model.dto.CalendarioDTO;
import com.orbita.ws.adminws.model.dto.IndicadorCalendDTO;
import com.orbita.ws.adminws.model.dto.ListCalendarioDTO;
import com.orbita.ws.adminws.model.dto.request.CalendarioRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository
public class SpAgenda {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callCalendario;
    private SimpleJdbcCall callIndicadores;

    SqlParameterSource in;

    @PostConstruct
    void init() {
        callCalendario = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_CITAS_CALENDARIO)
                        .declareParameters(
                        		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_MES, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_ANNIO, OracleTypes.NUMBER),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_CITAS_CALENDARIO, new CalendarioMapper());

        callIndicadores = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_IND_ACT_CALENDARIO)
                        .declareParameters(
                        		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_FECHA, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_INDICADOR_CALENDARIO, new IndicadorCalendMapper());
    }

    public ListCalendarioDTO execCalendario(CalendarioRequest request) {
        ListCalendarioDTO dto = new ListCalendarioDTO();
        List<CalendarioDTO> calendario;
        List<IndicadorCalendDTO> indicadores;

        in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_CODUSUARIO, request.getCodAnalista())
                .addValue(SpConstants.I_MES, request.getMes())
                .addValue(SpConstants.I_ANNIO, request.getAnio());

        try {

            Map<String, Object> out = callCalendario.execute(in);
            calendario = (List) out.get(SpConstants.O_CUR_CITAS_CALENDARIO);

            for (CalendarioDTO c : calendario) {
                 in = new MapSqlParameterSource()
                		.addValue(SpConstants.I_CODUSUARIO, request.getCodAnalista())
                        .addValue(SpConstants.I_FECHA, c.getFechaCita());

                Map<String, Object> outBase = callIndicadores.execute(in);
                indicadores = (List)outBase.get(SpConstants.O_CUR_INDICADOR_CALENDARIO);
                c.setIndicadores(indicadores);
            }

            dto.setCalendario(calendario);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

}
