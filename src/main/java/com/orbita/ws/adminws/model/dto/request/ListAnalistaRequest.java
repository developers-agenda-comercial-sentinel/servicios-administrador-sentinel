package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel("REQUEST - Listar Analista")
public class ListAnalistaRequest implements Serializable {

    @ApiModelProperty(value = "Código de Agencia", required = true)
    private String codAgencia;
    @ApiModelProperty(value = "Código de Perfil")
    private int codPerfil;

    public String getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(String codAgencia) {
        this.codAgencia = codAgencia;
    }

    public int getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(int codPerfil) {
        this.codPerfil = codPerfil;
    }
}
