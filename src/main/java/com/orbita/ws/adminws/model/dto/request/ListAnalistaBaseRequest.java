package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Lista Analista x Base")
public class ListAnalistaBaseRequest {

    @ApiModelProperty(value = "Código de base", required = true)
    private int codBase;
    @ApiModelProperty(value = "Número de página", required = true)
    private int pageNumber;
    @ApiModelProperty(value = "Cantidad de Registros por página", required = true)
    private int pageSize;

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
