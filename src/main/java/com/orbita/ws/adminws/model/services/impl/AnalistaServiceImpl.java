package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.AnalistaDAO;
import com.orbita.ws.adminws.model.dto.AnalistaDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaRequest;
import com.orbita.ws.adminws.model.services.AnalistaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnalistaServiceImpl implements AnalistaService {

    @Autowired
    private AnalistaDAO analistaDAO;

    @Override
    public AnalistaDTO listarAnalista(AnalistaRequest analistaRequest) {
        return analistaDAO.listarAnalistas(analistaRequest);
    }
}
