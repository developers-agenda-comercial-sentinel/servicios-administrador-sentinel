package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.OpcionBean;
import com.orbita.ws.adminws.model.beans.PerfilBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Perfil")
public class PerfilDTO extends BaseResponse implements Serializable {

    private PerfilBean perfil;
    private List<OpcionBean> opciones;

    public PerfilBean getPerfil() {
        return perfil;
    }

    public void setPerfil(PerfilBean perfil) {
        this.perfil = perfil;
    }

    public List<OpcionBean> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionBean> opciones) {
        this.opciones = opciones;
    }
}
