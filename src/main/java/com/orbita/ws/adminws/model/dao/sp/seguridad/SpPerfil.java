package com.orbita.ws.adminws.model.dao.sp.seguridad;

import com.orbita.ws.adminws.model.beans.PerfilBean;
import com.orbita.ws.adminws.model.dao.mapper.OpcionMapper;
import com.orbita.ws.adminws.model.dao.mapper.PerfilMapper;
import com.orbita.ws.adminws.model.dao.mapper.array.OpcionArray;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.InsertDTO;
import com.orbita.ws.adminws.model.dto.ListPerfilDTO;
import com.orbita.ws.adminws.model.dto.PerfilDTO;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilSaveRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilUpdateRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SpPerfil {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callSel;
    private SimpleJdbcCall callGet;
    private SimpleJdbcCall callIns;
    private SimpleJdbcCall callUpd;
    private SimpleJdbcCall callDel;

    // Inputs - Outputs
    SqlParameterSource in;
    Map<String, Object> out;

    @PostConstruct
    void init() {
        callSel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_PERFIL)
                .declareParameters(
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_PERFIL, new PerfilMapper());

        callGet = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_GET_PERFIL)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_PERFIL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_ESTADO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
        .returningResultSet(SpConstants.O_CUR_OPCION, new OpcionMapper());

        callIns = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_INS_PERFIL)
                .declareParameters(
                        new SqlParameter(SpConstants.I_ARR_OPCION, OracleTypes.ARRAY, SpConstants.TYPE_OPCION_TAB),
                        new SqlParameter(SpConstants.I_DESC_PERFIL, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_EST_PERFIL, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_REGISTRO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callUpd = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_UPD_PERFIL)
                .declareParameters(
                        new SqlParameter(SpConstants.I_ARR_OPCION, OracleTypes.ARRAY, SpConstants.TYPE_OPCION_TAB),
                        new SqlParameter(SpConstants.I_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_DESC_PERFIL, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_EST_PERFIL, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callDel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_DEL_PERFIL)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );
    }

    public ListPerfilDTO findAll(){
        ListPerfilDTO dto = new ListPerfilDTO();

        try {
            out = callSel.execute();
            dto.setPerfiles((List)out.get(SpConstants.O_CUR_PERFIL));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public PerfilDTO findById(PerfilRequest request){
        PerfilDTO dto = new PerfilDTO();
        PerfilBean bean = new PerfilBean();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_PERFIL, request.getCodPerfil());

        try {
            out = callGet.execute(in);
            bean.setDescripcion((String)out.get(SpConstants.O_PERFIL));
            bean.setEstado(((BigDecimal) out.get(SpConstants.O_ESTADO)).intValue());

            dto.setPerfil(bean);
            dto.setOpciones((List) out.get(SpConstants.O_CUR_OPCION));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));


        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public InsertDTO save(PerfilSaveRequest request) {
        InsertDTO dto = new InsertDTO();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_ARR_OPCION, new OpcionArray(request.getOpciones()))
                .addValue(SpConstants.I_DESC_PERFIL, request.getDescripcion())
                .addValue(SpConstants.I_EST_PERFIL, request.getEstado())
                .addValue(SpConstants.I_USUARIO_REGISTRO, request.getUsuarioRegistro());

        try {
            out = callIns.execute(in);
            dto.setCodigo(((BigDecimal) out.get(SpConstants.O_COD_PERFIL)).intValue());
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return  dto;
    }

    public BaseResponse update(PerfilUpdateRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_ARR_OPCION, new OpcionArray(request.getOpciones()))
                .addValue(SpConstants.I_ARR_OPCION_ELIMINAR, new OpcionArray(request.getOpcionesEliminar()))
                .addValue(SpConstants.I_COD_PERFIL, request.getCodPerfil())
                .addValue(SpConstants.I_DESC_PERFIL, request.getDescripcion())
                .addValue(SpConstants.I_EST_PERFIL, request.getEstado())
                .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getEstado());

        try {
            out = callUpd.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }

    public BaseResponse delete(DeleteRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_PERFIL, request.getCodigo())
                .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callDel.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }

}
