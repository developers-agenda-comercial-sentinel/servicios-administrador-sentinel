package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.CargaCobranzaDTO;
import com.orbita.ws.adminws.model.dto.CargaInactivoDTO;
import com.orbita.ws.adminws.model.dto.CargaRecurrenteDTO;
import com.orbita.ws.adminws.model.dto.CargaReferidoDTO;
import com.orbita.ws.adminws.model.dto.ListCargaHistoricoDTO;
import com.orbita.ws.adminws.model.dto.RutasExcelDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.CargaCobranzaReq;
import com.orbita.ws.adminws.model.dto.request.CargaErradaRequest;
import com.orbita.ws.adminws.model.dto.request.CargaInactivoReq;
import com.orbita.ws.adminws.model.dto.request.CargaRecurrenteReq;
import com.orbita.ws.adminws.model.dto.request.CargaReferidoReq;

public interface CargaMasivaDAO {

	RutasExcelDTO listarRutasExcel(BaseRequest request);
	BaseResponse cargaRecurrente(CargaRecurrenteReq request);
	BaseResponse cargaCobranza(CargaCobranzaReq request);
	BaseResponse cargaInactivo(CargaInactivoReq request);
	BaseResponse cargaReferido(CargaReferidoReq request);

	ListCargaHistoricoDTO listarHistorico(BaseRequest request);
	CargaRecurrenteDTO cargaErradaRecurrente(CargaErradaRequest request);
	CargaCobranzaDTO cargaErradaCobranza(CargaErradaRequest request);
	CargaInactivoDTO cargaErradaInactivo(CargaErradaRequest request);
	CargaReferidoDTO cargaErradaReferido(CargaErradaRequest request);
}
