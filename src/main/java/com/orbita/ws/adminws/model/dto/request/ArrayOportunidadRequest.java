package com.orbita.ws.adminws.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orbita.ws.adminws.util.SpConstants;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class ArrayOportunidadRequest implements SQLData {

    private String nroDocumento;
    private int codOportunidad;
    private int codCliente;
    private int codBase;

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public int getCodOportunidad() {
        return codOportunidad;
    }

    public void setCodOportunidad(int codOportunidad) {
        this.codOportunidad = codOportunidad;
    }

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    @JsonIgnore
    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_OPORTUNIDAD;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        nroDocumento = stream.readString();
        codOportunidad = stream.readInt();
        codCliente = stream.readInt();
        codBase = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(nroDocumento);
        stream.writeInt(codOportunidad);
        stream.writeInt(codCliente);
        stream.writeInt(codBase);
    }
}
