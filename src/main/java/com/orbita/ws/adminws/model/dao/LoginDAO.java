package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.LoginDTO;
import com.orbita.ws.adminws.model.dto.request.LoginRequest;

public interface LoginDAO {

    LoginDTO getUsuario(LoginRequest usuario);
}
