package com.orbita.ws.adminws.model.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel("DTO - Response Global")
public class BaseResponse implements Serializable {
    private String codVal;
    private String mensaje;

    public String getCodVal() {
        return codVal;
    }

    public void setCodVal(String codVal) {
        this.codVal = codVal;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
