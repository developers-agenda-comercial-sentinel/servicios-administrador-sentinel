package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.ListReporteDTO;
import com.orbita.ws.adminws.model.dto.ListReporteDetalleDTO;
import com.orbita.ws.adminws.model.dto.ListReporteVisitasDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ReporteDetalleRequest;

public interface ReporteDAO {

    ListReporteDTO listarReportes(AnalistaSentinelRequest request);
    ListReporteDetalleDTO getReporte(ReporteDetalleRequest request);

    ListReporteVisitasDTO listarRepVisitasRealizadas(AnalistaSentinelRequest request);
}
