package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.BaseBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BaseMapper implements RowMapper<BaseBean> {

    @Override
    public BaseBean mapRow(ResultSet rs, int i) throws SQLException {
        BaseBean bean = new BaseBean();
        bean.setCodBase(rs.getInt("COD_BASE"));
        bean.setBase(rs.getString("BASE"));
        bean.setActividad(rs.getString("ACTIVIDAD"));
        bean.setCodColor(rs.getString("COD_COLOR"));
        bean.setEstado(rs.getInt("ESTADO"));

        return bean;
    }
}
