package com.orbita.ws.adminws.model.dao.sp.cargamasiva;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dao.mapper.array.CargaCobranzaArray;
import com.orbita.ws.adminws.model.dao.mapper.array.CargaInactivoArray;
import com.orbita.ws.adminws.model.dao.mapper.array.CargaRecurrenteArray;
import com.orbita.ws.adminws.model.dao.mapper.array.CargaReferidoArray;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.request.CargaCobranzaReq;
import com.orbita.ws.adminws.model.dto.request.CargaInactivoReq;
import com.orbita.ws.adminws.model.dto.request.CargaRecurrenteReq;
import com.orbita.ws.adminws.model.dto.request.CargaReferidoReq;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpCargaMasiva {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall recCall;
	private SimpleJdbcCall cobCall;
	private SimpleJdbcCall inacCall;
	private SimpleJdbcCall refCall;
	
	@PostConstruct
	void init() {
		recCall = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_LOAD_RECURRENTE)
				.declareParameters(
						new SqlParameter(SpConstants.I_NOM_ARCHIVO, OracleTypes.VARCHAR),
						new SqlParameter(SpConstants.I_ARR_LOAD_RECURRENTE, OracleTypes.ARRAY, SpConstants.TYPE_LOAD_RECURRENTE_TAB),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				);
		
		cobCall = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_LOAD_COBRANZA)
				.declareParameters(
						new SqlParameter(SpConstants.I_NOM_ARCHIVO, OracleTypes.VARCHAR),
						new SqlParameter(SpConstants.I_ARR_LOAD_COBRANZA, OracleTypes.ARRAY, SpConstants.TYPE_LOAD_COBRANZA_TAB),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				);
		
		inacCall = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_LOAD_INACTIVO)
				.declareParameters(
						new SqlParameter(SpConstants.I_NOM_ARCHIVO, OracleTypes.VARCHAR),
						new SqlParameter(SpConstants.I_ARR_LOAD_INACTIVO, OracleTypes.ARRAY, SpConstants.TYPE_LOAD_INACTIVO_TAB),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				);
		
		refCall = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_LOAD_REFERIDO)
				.declareParameters(
						new SqlParameter(SpConstants.I_NOM_ARCHIVO, OracleTypes.VARCHAR),
						new SqlParameter(SpConstants.I_ARR_LOAD_REFERIDO, OracleTypes.ARRAY, SpConstants.TYPE_LOAD_REFERIDO_TAB),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				);
	}
	
	public BaseResponse cargaRecurrente(CargaRecurrenteReq request) {
        BaseResponse dto = new BaseResponse();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_NOM_ARCHIVO, request.getNomArchivo())
                .addValue(SpConstants.I_ARR_LOAD_RECURRENTE, new CargaRecurrenteArray(request.getCargaRecurrente()));

        try {
            Map<String, Object> out = recCall.execute(in);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
	}
	
	public BaseResponse cargaCobranza(CargaCobranzaReq request) {
        BaseResponse dto = new BaseResponse();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_NOM_ARCHIVO, request.getNomArchivo())
                .addValue(SpConstants.I_ARR_LOAD_COBRANZA, new CargaCobranzaArray(request.getCargaCobranza()));

        try {
            Map<String, Object> out = cobCall.execute(in);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
	}
	
	public BaseResponse cargaInactivo(CargaInactivoReq request) {
        BaseResponse dto = new BaseResponse();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_NOM_ARCHIVO, request.getNomArchivo())
                .addValue(SpConstants.I_ARR_LOAD_INACTIVO, new CargaInactivoArray(request.getCargaInactivo()));

        try {
            Map<String, Object> out = inacCall.execute(in);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
	}
	
	public BaseResponse cargaReferido(CargaReferidoReq request) {
        BaseResponse dto = new BaseResponse();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_NOM_ARCHIVO, request.getNomArchivo())
                .addValue(SpConstants.I_ARR_LOAD_REFERIDO, new CargaReferidoArray(request.getCargaReferido()));

        try {
            Map<String, Object> out = refCall.execute(in);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
	}
	
}
