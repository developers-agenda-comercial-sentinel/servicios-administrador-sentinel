package com.orbita.ws.adminws.model.beans;

import com.google.gson.annotations.SerializedName;

public class LoginBean {

	@SerializedName("CodigoWS")
    private String codigoWS;
    
	public String getCodigoWS() {
		return codigoWS;
	}
	public void setCodigoWS(String codigoWS) {
		this.codigoWS = codigoWS;
	}
    
}
