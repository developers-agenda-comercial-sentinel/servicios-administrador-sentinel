package com.orbita.ws.adminws.model.dao.sp.cartera;

import com.orbita.ws.adminws.model.dao.mapper.OportunidadMapper;
import com.orbita.ws.adminws.model.dao.mapper.array.ActividadArray;
import com.orbita.ws.adminws.model.dao.mapper.array.BaseArray;
import com.orbita.ws.adminws.model.dto.ListOportunidadDTO;
import com.orbita.ws.adminws.model.dto.request.OportunidadRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@RestController
public class SpOportunidad {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_OPORTUNIDADES)
                .declareParameters(
                        new SqlParameter(SpConstants.I_ARR_BASE, OracleTypes.ARRAY, SpConstants.TYPE_BASE_TAB),
                        new SqlParameter(SpConstants.I_ARR_ACTIVIDAD, OracleTypes.ARRAY, SpConstants.TYPE_ACTIVIDAD_TAB),
                        new SqlParameter(SpConstants.I_BUSQUEDA, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_UBIGEO, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_PAGE_NUMBER, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_PAGE_SIZE, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_OPORTUNIDAD, new OportunidadMapper());
    }

    public ListOportunidadDTO execute(OportunidadRequest request) {
        ListOportunidadDTO dto = new ListOportunidadDTO();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_BUSQUEDA, request.getBusqueda())
                .addValue(SpConstants.I_COD_UBIGEO, request.getUbigeo())
                .addValue(SpConstants.I_ARR_BASE, new BaseArray(request.getBases()))
                .addValue(SpConstants.I_ARR_ACTIVIDAD, new ActividadArray(request.getActividades()))
                .addValue(SpConstants.I_PAGE_NUMBER, request.getPageNumber())
                .addValue(SpConstants.I_PAGE_SIZE, request.getPageSize());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setOportunidades((List) out.get(SpConstants.O_CUR_OPORTUNIDAD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;

    }
}
