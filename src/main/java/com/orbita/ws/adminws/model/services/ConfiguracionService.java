package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.ListConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ListConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.request.ActividadUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.BaseSaveRequest;
import com.orbita.ws.adminws.model.dto.request.BaseUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;

public interface ConfiguracionService {

	ListConfigActividadDTO findAllActivity();
	ConfigActividadDTO findByIdActivity(BaseRequest request);
	BaseResponse updateActivity(ActividadUpdateRequest request);
	
	ListConfigBaseDTO findAllBase();
	ConfigBaseDTO findByIdBase(BaseRequest request);
	BaseResponse saveBase(BaseSaveRequest request);
	BaseResponse updateBase(BaseUpdateRequest request);
	BaseResponse deleteBase(DeleteRequest request);
	
}
