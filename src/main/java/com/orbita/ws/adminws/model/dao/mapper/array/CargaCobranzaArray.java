package com.orbita.ws.adminws.model.dao.mapper.array;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.support.AbstractSqlTypeValue;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaCobranzaReq;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class CargaCobranzaArray extends AbstractSqlTypeValue{

    private List<ArrayCargaCobranzaReq> values;

    public CargaCobranzaArray(List<ArrayCargaCobranzaReq> values) {
        this.values = values;
    }
	
	@Override
	protected Object createTypeValue(Connection connection, int sqlType, String s) throws SQLException {
		oracle.jdbc.OracleConnection wrapperConnection = connection.unwrap(oracle.jdbc.OracleConnection.class);
        connection = wrapperConnection;
        ArrayDescriptor desc = new ArrayDescriptor(s, connection);

        return new ARRAY(desc, connection, values.toArray(new ArrayCargaCobranzaReq[values.size()]));
	}

}
