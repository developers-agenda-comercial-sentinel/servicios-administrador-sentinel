package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.ColorDTO;
import com.orbita.ws.adminws.model.dto.ListActividadDTO;

public interface ActividadDAO {

    ListActividadDTO listarActividades();
    ColorDTO listarColores();
}
