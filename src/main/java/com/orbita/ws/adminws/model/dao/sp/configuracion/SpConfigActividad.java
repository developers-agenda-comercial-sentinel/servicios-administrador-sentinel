package com.orbita.ws.adminws.model.dao.sp.configuracion;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dao.mapper.ActividadMapper;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ListConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.request.ActividadUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpConfigActividad {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall callSel;
	private SimpleJdbcCall callGet;
	private SimpleJdbcCall callUpd;
	
	// Inputs - Outputs
    SqlParameterSource in;
    Map<String, Object> out;
	
	@PostConstruct
	void init() {
		callSel = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_SEL_CONFIG_ACTIVIDAD)
				.declareParameters(
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR))
				.returningResultSet(SpConstants.O_CUR_ACTIVIDAD, new ActividadMapper());
		
		callGet = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_GET_CONFIG_ACTIVIDAD)
				.declareParameters(
                        new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_ACTIVIDAD, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COLOR, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_HEXADECIMAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_ESTADO, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR));
		
		callUpd = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB)
				.withProcedureName(SpConstants.SP_UPD_CONFIG_ACTIVIDAD)
				.declareParameters(
                        new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ACTIVIDAD, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_COLOR, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ESTADO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR));
	}

	public ListConfigActividadDTO findAllActivity() {
		ListConfigActividadDTO dto = new ListConfigActividadDTO();
		
		try {
			out = callSel.execute();
            dto.setLista((List)out.get(SpConstants.O_CUR_ACTIVIDAD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
		} catch (Exception e) {
            System.err.println(e.getMessage());
		}
		
		return dto;
	}
	
	public ConfigActividadDTO findByIdActivity(BaseRequest request) {
		ConfigActividadDTO response = new ConfigActividadDTO();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodigo());

        try {
            out = callGet.execute(in);
            response.setActividad((String) out.get(SpConstants.O_ACTIVIDAD));
            response.setCodColor(((BigDecimal) out.get(SpConstants.O_COLOR)).intValue());
            response.setHexadecimalcolor((String) out.get(SpConstants.O_HEXADECIMAL));
            response.setEstado(((BigDecimal) out.get(SpConstants.O_ESTADO)).intValue());
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
	
	public BaseResponse updateActivity(ActividadUpdateRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad())
            .addValue(SpConstants.I_ACTIVIDAD, request.getActividad())
            .addValue(SpConstants.I_COLOR, request.getCodColor())
            .addValue(SpConstants.I_ESTADO, request.getEstado())
            .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callUpd.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
}
