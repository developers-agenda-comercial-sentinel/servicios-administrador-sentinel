package com.orbita.ws.adminws.model.dto.request;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaCobranzaReq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Carga Cobranza")
public class CargaCobranzaReq implements Serializable{
	
	@ApiModelProperty(value = "Nombre del Archivo")
	private String nomArchivo;
	
	@ApiModelProperty(value = "Lista de Cobranza")
	List<ArrayCargaCobranzaReq> cargaCobranza;

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public List<ArrayCargaCobranzaReq> getCargaCobranza() {
		return cargaCobranza;
	}

	public void setCargaCobranza(List<ArrayCargaCobranzaReq> cargaCobranza) {
		this.cargaCobranza = cargaCobranza;
	}

}
