package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.OportunidadDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OportunidadMapper implements RowMapper<OportunidadDTO> {
    @Override
    public OportunidadDTO mapRow(ResultSet rs, int i) throws SQLException {
        OportunidadDTO dto = new OportunidadDTO();
        dto.setCodCliente(rs.getInt("COD_CLIENTE"));
        dto.setCodOportunidad(rs.getInt("COD_OPORTUNIDAD"));
        dto.setNombres(rs.getString("NOMBRES"));
        dto.setApePaterno(rs.getString("APE_PATERNO"));
        dto.setApeMaterno(rs.getString("APE_MATERNO"));
        dto.setNroDocumento(rs.getString("NRO_DOCUMENTO"));
        dto.setDistrito(rs.getString("DISTRITO"));
        dto.setCodBase(rs.getInt("COD_BASE"));
        dto.setCodColor(rs.getInt("COLOR"));

        return dto;
    }
}
