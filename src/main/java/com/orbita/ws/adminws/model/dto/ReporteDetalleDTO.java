package com.orbita.ws.adminws.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class ReporteDetalleDTO {

    private String tipo;
    private String descripcion;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String grupo;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<ReporteDetalleDTO> detalle;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public List<ReporteDetalleDTO> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<ReporteDetalleDTO> detalle) {
        this.detalle = detalle;
    }
}
