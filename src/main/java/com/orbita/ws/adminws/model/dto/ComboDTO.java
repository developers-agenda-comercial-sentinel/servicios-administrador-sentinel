package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.ComboBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Combo")
public class ComboDTO extends BaseResponse implements Serializable {

    private List<ComboBean> lista;

    public List<ComboBean> getLista() {
        return lista;
    }

    public void setLista(List<ComboBean> lista) {
        this.lista = lista;
    }
}
