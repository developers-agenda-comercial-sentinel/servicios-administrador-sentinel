package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class DashTopVisitaDTO implements Serializable {

    private String analista;
    private int citas;

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public int getCitas() {
        return citas;
    }

    public void setCitas(int citas) {
        this.citas = citas;
    }
}
