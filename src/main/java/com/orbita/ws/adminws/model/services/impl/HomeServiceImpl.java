package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.HomeDAO;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaBaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListDashCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.ListHomeAnalista;
import com.orbita.ws.adminws.model.services.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private HomeDAO homeDAO;

    @Override
    public ListDashActividadDTO execActividad() {
        return homeDAO.execActividad();
    }

    @Override
    public ListBaseDTO execBase(BaseRequest request) {
        return homeDAO.execBase(request);
    }

    @Override
    public ListDashAnalistaBaseDTO execAnalistaBase(ListAnalistaBaseRequest request) {
        return homeDAO.execAnalistaBase(request);
    }

    @Override
    public ListDashCarteraDTO execCartera(ListDashCarteraRequest request) {
        return homeDAO.execCartera(request);
    }

    @Override
    public ListDashAnalistaDTO execAnalista(ListHomeAnalista request) {
        return homeDAO.execAnalista(request);
    }

    @Override
    public ListDashTopVisitaDTO execTopVisita() {
        return homeDAO.execTopVisita();
    }

    @Override
    public ListDashTopEfectivaDTO execTopEfectiva() {
        return homeDAO.execTopEfectiva();
    }

    @Override
    public ListAnalistaUbicacionDTO execUbiAnalista() {
        return homeDAO.execUbiAnalista();
    }


}
