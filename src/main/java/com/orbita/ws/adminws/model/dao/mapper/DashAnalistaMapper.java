package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.DashAnalistaDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashAnalistaMapper implements RowMapper<DashAnalistaDTO> {
    @Override
    public DashAnalistaDTO mapRow(ResultSet rs, int i) throws SQLException {

        DashAnalistaDTO dto = new DashAnalistaDTO();
        dto.setAnalista(rs.getString("ANALISTA"));
        dto.setEmail(rs.getString("EMAIL"));

        return dto;
    }
}
