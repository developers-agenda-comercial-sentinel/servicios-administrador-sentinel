package com.orbita.ws.adminws.model.beans;

import io.swagger.annotations.ApiModel;

@ApiModel("BEAN - Combo")
public class ComboBean {
    private int codigo;
    private String descripcion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
