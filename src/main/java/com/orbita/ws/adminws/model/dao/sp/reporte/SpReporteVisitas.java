package com.orbita.ws.adminws.model.dao.sp.reporte;

import com.orbita.ws.adminws.model.dao.mapper.ReporteVisitasMapper;
import com.orbita.ws.adminws.model.dto.ListReporteVisitasDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SpReporteVisitas {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_REPORTE_VISITAS)
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_REPORTE, new ReporteVisitasMapper());
    }

    public ListReporteVisitasDTO execute(AnalistaSentinelRequest request) {
        ListReporteVisitasDTO dto = new ListReporteVisitasDTO();

        Map<String, Object> in = new HashMap<>();
        in.put(SpConstants.I_CODUSUARIO, request.getCodigo());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setRepVisitasRealizadas((List) out.get(SpConstants.O_CUR_REPORTE));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

}
