package com.orbita.ws.adminws.model.dto.request;

import com.orbita.ws.adminws.model.beans.CitaBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("REQUEST - Registrar Cita")
public class RegCitaRequest extends InsRequest{

    @ApiModelProperty(value = "Código de analista", required = true)
    private int codAnalista;
    @ApiModelProperty(value = "Fecha de cita", required = true)
    private String fecha;
    @ApiModelProperty(value = "Lista clientes y gestiones", required = true)
    private List<CitaBean> clientes;

	public int getCodAnalista() {
		return codAnalista;
	}

	public void setCodAnalista(int codAnalista) {
		this.codAnalista = codAnalista;
	}

	public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public List<CitaBean> getClientes() {
        return clientes;
    }

    public void setClientes(List<CitaBean> clientes) {
        this.clientes = clientes;
    }
}
