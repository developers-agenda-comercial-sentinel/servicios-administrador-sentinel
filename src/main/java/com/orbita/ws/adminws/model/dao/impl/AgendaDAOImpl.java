package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.AgendaDAO;
import com.orbita.ws.adminws.model.dao.sp.agenda.SpAgenda;
import com.orbita.ws.adminws.model.dao.sp.agenda.SpListarCarteraAgenda;
import com.orbita.ws.adminws.model.dao.sp.agenda.SpListarCboAgenda;
import com.orbita.ws.adminws.model.dao.sp.agenda.SpRegAgenda;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListCalendarioDTO;
import com.orbita.ws.adminws.model.dto.ListCartAgendaDTO;
import com.orbita.ws.adminws.model.dto.ListRegAgendaCboDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.CalendarioRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraAgendaRequest;
import com.orbita.ws.adminws.model.dto.request.RegCitaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgendaDAOImpl implements AgendaDAO {

    @Autowired
    private SpAgenda spAgenda;

    @Autowired
    private SpListarCboAgenda regAgendaCbo;

    @Autowired
    private SpListarCarteraAgenda listarCarteraAgenda;

    @Autowired
    private SpRegAgenda regAgenda;

    @Override
    public ListCalendarioDTO execCalendario(CalendarioRequest request) {
        return spAgenda.execCalendario(request);
    }

    @Override
    public ListRegAgendaCboDTO execute(AnalistaSentinelRequest request) {
        return regAgendaCbo.execute(request);
    }

    @Override
    public ListCartAgendaDTO listarCartera(CarteraAgendaRequest request) {
        return listarCarteraAgenda.listarCartera(request);
    }

    @Override
    public BaseResponse registrarAgenda(RegCitaRequest request) {
        return regAgenda.registrarAgenda(request);
    }
}
