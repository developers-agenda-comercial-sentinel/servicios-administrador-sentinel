package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.ReaccionBean;

import java.io.Serializable;
import java.util.List;

public class ReacContactadoDTO extends BaseResponse implements Serializable {

    private Reaccion reaccion;

    public class Reaccion {
        private String reaccion;
        private int codActividad;
        private String actividad;
        private int misti;
        private int activo;
        private List<ReaccionBean> reaccionDetalle;

        public String getReaccion() {
            return reaccion;
        }

        public void setReaccion(String reaccion) {
            this.reaccion = reaccion;
        }

        public int getCodActividad() {
            return codActividad;
        }

        public void setCodActividad(int codActividad) {
            this.codActividad = codActividad;
        }

        public String getActividad() {
            return actividad;
        }

        public void setActividad(String actividad) {
            this.actividad = actividad;
        }

        public int getMisti() {
            return misti;
        }

        public void setMisti(int misti) {
            this.misti = misti;
        }

        public int getActivo() {
            return activo;
        }

        public void setActivo(int activo) {
            this.activo = activo;
        }

        public List<ReaccionBean> getReaccionDetalle() {
            return reaccionDetalle;
        }

        public void setReaccionDetalle(List<ReaccionBean> reaccionDetalle) {
            this.reaccionDetalle = reaccionDetalle;
        }
    }

    public Reaccion getReaccion() {
        return reaccion;
    }

    public void setReaccion(Reaccion reaccion) {
        this.reaccion = reaccion;
    }
}
