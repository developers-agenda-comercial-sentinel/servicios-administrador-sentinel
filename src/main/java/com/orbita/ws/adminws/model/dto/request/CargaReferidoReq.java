package com.orbita.ws.adminws.model.dto.request;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaReferidoReq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Carga Referido")
public class CargaReferidoReq implements Serializable{
	
	@ApiModelProperty(value = "Nombre del Archivo")
	private String nomArchivo;
	
	@ApiModelProperty(value = "Lista de Referido")
	List<ArrayCargaReferidoReq> cargaReferido;

	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public List<ArrayCargaReferidoReq> getCargaReferido() {
		return cargaReferido;
	}

	public void setCargaReferido(List<ArrayCargaReferidoReq> cargaReferido) {
		this.cargaReferido = cargaReferido;
	}

}
