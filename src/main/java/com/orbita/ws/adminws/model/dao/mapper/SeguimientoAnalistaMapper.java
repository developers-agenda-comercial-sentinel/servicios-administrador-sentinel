package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.AnalistaUbicacionDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SeguimientoAnalistaMapper implements RowMapper<AnalistaUbicacionDTO> {

    @Override
    public AnalistaUbicacionDTO mapRow(ResultSet rs, int i) throws SQLException {

        AnalistaUbicacionDTO dto = new AnalistaUbicacionDTO();
        dto.setNombres(rs.getString("NOMBRES"));
        dto.setApePaterno(rs.getString("APE_PATERNO"));
        dto.setApeMaterno(rs.getString("APE_MATERNO"));
        dto.setAgencia(rs.getString("AGENCIA"));
        dto.setLatitud(rs.getString("LATITUD"));
        dto.setLongitud(rs.getString("LONGITUD"));
        dto.setFecha(rs.getString("FECHA_REGISTRO"));
        return dto;
    }
}
