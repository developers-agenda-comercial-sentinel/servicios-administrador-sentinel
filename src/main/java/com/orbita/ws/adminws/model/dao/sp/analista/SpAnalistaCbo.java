package com.orbita.ws.adminws.model.dao.sp.analista;

import com.orbita.ws.adminws.model.dao.mapper.ComboMapper;
import com.orbita.ws.adminws.model.dto.ComboDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SpAnalistaCbo {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_ANALISTAXAGENCIA)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_AGENCIA, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_ANALISTA, new ComboMapper());
    }

    public ComboDTO execute(BaseRequest request) {
        ComboDTO dto = new ComboDTO();

        Map<String, Object> in = new HashMap<>();
        in.put(SpConstants.I_COD_AGENCIA, request.getCodigo());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setLista((List) out.get(SpConstants.O_CUR_ANALISTA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;

    }
}
