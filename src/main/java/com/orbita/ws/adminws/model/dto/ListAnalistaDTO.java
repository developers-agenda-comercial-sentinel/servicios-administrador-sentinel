package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.UsuarioBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Lista Analista")
public class ListAnalistaDTO extends BaseResponse implements Serializable {

    List<UsuarioBean> usuarios;

    public List<UsuarioBean> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioBean> usuarios) {
        this.usuarios = usuarios;
    }
}
