package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.OportunidadDAO;
import com.orbita.ws.adminws.model.dto.ListOportunidadDTO;
import com.orbita.ws.adminws.model.dto.request.OportunidadRequest;
import com.orbita.ws.adminws.model.services.OportunidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OportunidadServiceImpl implements OportunidadService {

    @Autowired
    private OportunidadDAO oportunidadDAO;

    @Override
    public ListOportunidadDTO listarOportunidades(OportunidadRequest request) {
        return oportunidadDAO.listarOportunidades(request);
    }
}
