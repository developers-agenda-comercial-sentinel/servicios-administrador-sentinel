package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaBaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListDashCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.ListHomeAnalista;

public interface HomeDAO {

    ListDashActividadDTO execActividad();
    ListBaseDTO execBase(BaseRequest request);
    ListDashAnalistaBaseDTO execAnalistaBase(ListAnalistaBaseRequest request);
    ListDashCarteraDTO execCartera(ListDashCarteraRequest request);

    ListDashAnalistaDTO execAnalista(ListHomeAnalista request);

    ListDashTopVisitaDTO execTopVisita();
    ListDashTopEfectivaDTO execTopEfectiva();

    ListAnalistaUbicacionDTO execUbiAnalista();
}
