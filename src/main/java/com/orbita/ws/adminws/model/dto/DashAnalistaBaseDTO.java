package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

/**
 * @author Jorge Leon
 *
 */
public class DashAnalistaBaseDTO implements Serializable {

    private int codAnalista;
    private String analista;
    private int porcentaje;
    
	public int getCodAnalista() {
		return codAnalista;
	}

	public void setCodAnalista(int codAnalista) {
		this.codAnalista = codAnalista;
	}

	public String getAnalista() {
		return analista;
	}

	public void setAnalista(String analista) {
		this.analista = analista;
	}

	public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }
}
