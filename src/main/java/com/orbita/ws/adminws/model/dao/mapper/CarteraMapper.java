package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.CarteraBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CarteraMapper implements RowMapper<CarteraBean> {

    @Override
    public CarteraBean mapRow(ResultSet rs, int i) throws SQLException {
        CarteraBean bean = new CarteraBean();
        bean.setCodCliente(rs.getLong("COD_CLIENTE"));
        bean.setNombres(rs.getString("NOMBRES"));
        bean.setApePaterno(rs.getString("APE_PATERNO"));
        bean.setApeMaterno(rs.getString("APE_MATERNO"));
        bean.setCodGestion(rs.getLong("COD_GESTION"));
        bean.setResultado(rs.getString("RESULTADO"));
        bean.setCodActividad(rs.getInt("COD_ACTIVIDAD"));
        bean.setActividad(rs.getString("ACTIVIDAD"));
        bean.setCodColor(rs.getInt("COLOR"));
        bean.setDistrito(rs.getString("DISTRITO"));
        bean.setCodBase(rs.getInt("COD_BASE"));

        return bean;
    }
}
