package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.ReporteDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ListaReporteMapper implements RowMapper<ReporteDTO> {
    @Override
    public ReporteDTO mapRow(ResultSet rs, int i) throws SQLException {
        ReporteDTO dto = new ReporteDTO();
        dto.setReporte(rs.getString("REPORTE"));
        dto.setFecha(rs.getString("FECHA"));

        return dto;
    }
}
