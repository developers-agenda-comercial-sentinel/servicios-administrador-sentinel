package com.orbita.ws.adminws.model.dto;

public class CargaHistoricoDTO {

	private int codigo;
	private String nomArchivo;
	private int nroClientes;
	private int nroAnalistas;
	private String fecRegistro;
	private int nroValidos;
	private int nroObservados;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNomArchivo() {
		return nomArchivo;
	}
	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}
	public int getNroClientes() {
		return nroClientes;
	}
	public void setNroClientes(int nroClientes) {
		this.nroClientes = nroClientes;
	}
	public int getNroAnalistas() {
		return nroAnalistas;
	}
	public void setNroAnalistas(int nroAnalistas) {
		this.nroAnalistas = nroAnalistas;
	}
	public String getFecRegistro() {
		return fecRegistro;
	}
	public void setFecRegistro(String fecRegistro) {
		this.fecRegistro = fecRegistro;
	}
	public int getNroValidos() {
		return nroValidos;
	}
	public void setNroValidos(int nroValidos) {
		this.nroValidos = nroValidos;
	}
	public int getNroObservados() {
		return nroObservados;
	}
	public void setNroObservados(int nroObservados) {
		this.nroObservados = nroObservados;
	}
	
}
