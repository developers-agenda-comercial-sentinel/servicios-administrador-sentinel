package com.orbita.ws.adminws.model.dto;

public class ReporteDTO {

    private String reporte;
    private String fecha;

    public String getReporte() {
        return reporte;
    }

    public void setReporte(String reporte) {
        this.reporte = reporte;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
