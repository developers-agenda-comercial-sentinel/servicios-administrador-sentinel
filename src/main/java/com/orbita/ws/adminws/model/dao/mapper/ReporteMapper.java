package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.ReporteDetalleDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReporteMapper implements RowMapper<ReporteDetalleDTO> {

    @Override
    public ReporteDetalleDTO mapRow(ResultSet rs, int i) throws SQLException {
        ReporteDetalleDTO dto = new ReporteDetalleDTO();
        dto.setTipo(rs.getString("TIPO"));
        dto.setDescripcion(rs.getString("DESCRIPCION"));
        dto.setGrupo(rs.getString("GROUP_INFO"));

        return dto;
    }
}
