package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListDashAnalistaDTO extends BaseResponse implements Serializable {

    private List<DashAnalistaDTO> lista;

    public List<DashAnalistaDTO> getLista() {
        return lista;
    }

    public void setLista(List<DashAnalistaDTO> lista) {
        this.lista = lista;
    }
}
