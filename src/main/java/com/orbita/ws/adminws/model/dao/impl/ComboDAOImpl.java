package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.ComboDAO;
import com.orbita.ws.adminws.model.dao.sp.analista.SpAnalistaCbo;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpAgencia;
import com.orbita.ws.adminws.model.dto.ComboDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComboDAOImpl implements ComboDAO {

    @Autowired
    private SpAgencia spAgencia;

    @Autowired
    private SpAnalistaCbo spAnalistaCbo;

    @Override
    public ComboDTO cboAgencia() {
        return spAgencia.execute();
    }

    @Override
    public ComboDTO cboAnalista(BaseRequest request) {
        return spAnalistaCbo.execute(request);
    }
}
