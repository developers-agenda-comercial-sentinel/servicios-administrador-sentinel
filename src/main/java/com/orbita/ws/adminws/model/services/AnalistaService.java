package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.AnalistaDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaRequest;

public interface AnalistaService {

    AnalistaDTO listarAnalista(AnalistaRequest analistaRequest);
}
