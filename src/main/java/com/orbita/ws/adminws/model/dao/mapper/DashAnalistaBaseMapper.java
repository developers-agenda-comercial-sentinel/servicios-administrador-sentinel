package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.DashAnalistaBaseDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashAnalistaBaseMapper implements RowMapper<DashAnalistaBaseDTO> {
    @Override
    public DashAnalistaBaseDTO mapRow(ResultSet rs, int i) throws SQLException {

        DashAnalistaBaseDTO dto = new DashAnalistaBaseDTO();
        dto.setCodAnalista(rs.getInt("COD_ANALISTA"));
        dto.setAnalista(rs.getString("ANALISTA"));
        dto.setPorcentaje(rs.getInt("PORCENTAJE"));

        return dto;
    }
}
