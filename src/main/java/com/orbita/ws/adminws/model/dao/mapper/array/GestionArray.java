package com.orbita.ws.adminws.model.dao.mapper.array;

import com.orbita.ws.adminws.model.beans.GestionBean;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import org.springframework.jdbc.core.support.AbstractSqlTypeValue;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class GestionArray extends AbstractSqlTypeValue {

    private List<GestionBean> values;

    public GestionArray(List<GestionBean> values) {
        this.values = values;
    }

    @Override
    protected Object createTypeValue(Connection connection, int i, String s) throws SQLException {
        oracle.jdbc.OracleConnection wrapperConnection = connection.unwrap(oracle.jdbc.OracleConnection.class);
        connection = wrapperConnection;
        ArrayDescriptor desc = new ArrayDescriptor(s, connection);

        return new ARRAY(desc, connection, values.toArray(new GestionBean[values.size()]));
    }
}
