package com.orbita.ws.adminws.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orbita.ws.adminws.model.dao.ConfiguracionDAO;
import com.orbita.ws.adminws.model.dao.sp.configuracion.SpConfigActividad;
import com.orbita.ws.adminws.model.dao.sp.configuracion.SpBase;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.ListConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ListConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.request.ActividadUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.BaseSaveRequest;
import com.orbita.ws.adminws.model.dto.request.BaseUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;

@Service
public class ConfiguracionDaoImpl implements ConfiguracionDAO{
	
	@Autowired
	private SpConfigActividad spActividad;

	@Autowired
	private SpBase spBase;

	@Override
	public ListConfigActividadDTO findAllActivity() {
		return spActividad.findAllActivity();
	}

	@Override
	public ConfigActividadDTO findByIdActivity(BaseRequest request) {
		return spActividad.findByIdActivity(request);
	}

	@Override
	public BaseResponse updateActivity(ActividadUpdateRequest request) {
		return spActividad.updateActivity(request);
	}

	@Override
	public ListConfigBaseDTO findAllBase() {
		return spBase.findAllBase();
	}

	@Override
	public ConfigBaseDTO findByIdBase(BaseRequest request) {
		return spBase.findByIdBase(request);
	}

	@Override
	public BaseResponse saveBase(BaseSaveRequest request) {
		return spBase.saveBase(request);
	}

	@Override
	public BaseResponse updateBase(BaseUpdateRequest request) {
		return spBase.updateBase(request);
	}

	@Override
	public BaseResponse deleteBase(DeleteRequest request) {
		return spBase.deleteBase(request);
	}

}
