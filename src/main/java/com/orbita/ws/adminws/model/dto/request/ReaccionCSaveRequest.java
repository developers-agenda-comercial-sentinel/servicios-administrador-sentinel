package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("REQUEST - Guardar Reaccion Contactado")
public class ReaccionCSaveRequest extends InsRequest implements Serializable {

    private String reaccion;
    private int codActividad;
    private int activo;
    private int misti;
    private List<ArrayReacDetaRequest> reaccionDetalle;

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getMisti() {
        return misti;
    }

    public void setMisti(int misti) {
        this.misti = misti;
    }

    public List<ArrayReacDetaRequest> getReaccionDetalle() {
        return reaccionDetalle;
    }

    public void setReaccionDetalle(List<ArrayReacDetaRequest> reaccionDetalle) {
        this.reaccionDetalle = reaccionDetalle;
    }
}
