package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.ActividadBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ActividadMapper implements RowMapper<ActividadBean> {

    @Override
    public ActividadBean mapRow(ResultSet rs, int i) throws SQLException {
        ActividadBean bean = new ActividadBean();
        bean.setCodActividad(rs.getInt("COD_ACTIVIDAD"));
        bean.setActividad(rs.getString("ACTIVIDAD"));
        bean.setHexadecimalColor(rs.getString("HEXADECIMAL_COLOR"));
        bean.setCodColor(rs.getInt("COD_COLOR"));
        bean.setColor(rs.getString("COLOR"));
        bean.setEstado(rs.getInt("ESTADO"));

        return bean;
    }
}
