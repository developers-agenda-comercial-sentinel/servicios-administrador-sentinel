package com.orbita.ws.adminws.model.beans;

import io.swagger.annotations.ApiModel;

@ApiModel("BEAN - Cartera")
public class CarteraBean {

    private Long codCliente;
    private String nombres;
    private String apePaterno;
    private String apeMaterno;
    private Long codGestion;
    private String resultado;
    private int codActividad;
    private String actividad;
    private int codColor;
    private String distrito;
    private int codBase;

    public Long getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(Long codCliente) {
        this.codCliente = codCliente;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApePaterno() {
        return apePaterno;
    }

    public void setApePaterno(String apePaterno) {
        this.apePaterno = apePaterno;
    }

    public String getApeMaterno() {
        return apeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {
        this.apeMaterno = apeMaterno;
    }

    public Long getCodGestion() {
        return codGestion;
    }

    public void setCodGestion(Long codGestion) {
        this.codGestion = codGestion;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public int getCodColor() {
        return codColor;
    }

    public void setCodColor(int codColor) {
        this.codColor = codColor;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }
}
