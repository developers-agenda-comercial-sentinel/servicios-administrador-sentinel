package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.ListOportunidadDTO;
import com.orbita.ws.adminws.model.dto.request.OportunidadRequest;

public interface OportunidadDAO {

    ListOportunidadDTO listarOportunidades(OportunidadRequest request);
}
