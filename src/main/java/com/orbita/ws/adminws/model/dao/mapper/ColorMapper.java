package com.orbita.ws.adminws.model.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.orbita.ws.adminws.model.beans.ColorBean;

public class ColorMapper implements RowMapper<ColorBean> {

	@Override
	public ColorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ColorBean bean = new ColorBean();
		bean.setCodColor(rs.getInt("COD_COLOR"));
		bean.setHexadecimal(rs.getString("HEXADECIMAL"));
		
		return bean;
	}

}
