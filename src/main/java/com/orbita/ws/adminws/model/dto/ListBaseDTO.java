package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.BaseBean;

import java.io.Serializable;
import java.util.List;

public class ListBaseDTO extends BaseResponse implements Serializable {

    private List<BaseBean> bases;

    public List<BaseBean> getBases() {
        return bases;
    }

    public void setBases(List<BaseBean> bases) {
        this.bases = bases;
    }
}
