package com.orbita.ws.adminws.model.dao.sp.seguridad;

import com.orbita.ws.adminws.model.dto.LoginDTO;
import com.orbita.ws.adminws.model.dto.request.LoginRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Repository
public class SpLogin {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                            .withCatalogName(SpConstants.PK_WEB)
                            .withProcedureName(SpConstants.SP_LOGIN)
                            .withoutProcedureColumnMetaDataAccess()
                            .declareParameters(
                                    new SqlParameter(SpConstants.I_USUARIO, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_COD_USUARIO, OracleTypes.NUMBER),
                                    new SqlOutParameter(SpConstants.O_NOMBRES, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_APEPATERNO, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_APEMATERNO, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_NRODOCUMENTO, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                            );
    }

    public LoginDTO execute(LoginRequest request) {
        LoginDTO dto = new LoginDTO();

        Map<String, Object> in = new HashMap<>();
        in.put(SpConstants.I_USUARIO, request.getUsuario());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setCodUsuario(((BigDecimal) out.get(SpConstants.O_COD_USUARIO)).intValue());
            dto.setNombre((String) out.get(SpConstants.O_NOMBRES));
            dto.setApePaterno((String) out.get(SpConstants.O_APEPATERNO));
            dto.setApeMaterno((String) out.get(SpConstants.O_APEMATERNO));
            dto.setNroDocumento((String) out.get(SpConstants.O_NRODOCUMENTO));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

}
