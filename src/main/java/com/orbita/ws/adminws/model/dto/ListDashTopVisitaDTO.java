package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListDashTopVisitaDTO extends BaseResponse implements Serializable {

    private List<DashTopVisitaDTO> lista;

    public List<DashTopVisitaDTO> getLista() {
        return lista;
    }

    public void setLista(List<DashTopVisitaDTO> lista) {
        this.lista = lista;
    }
}
