package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class OportunidadDTO implements Serializable {

    private int codCliente;
    private int codOportunidad;
    private String nombres;
    private String apePaterno;
    private String apeMaterno;
    private String nroDocumento;
    private String distrito;
    private int codBase;
    private int codColor;

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }

    public int getCodOportunidad() {
        return codOportunidad;
    }

    public void setCodOportunidad(int codOportunidad) {
        this.codOportunidad = codOportunidad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApePaterno() {
        return apePaterno;
    }

    public void setApePaterno(String apePaterno) {
        this.apePaterno = apePaterno;
    }

    public String getApeMaterno() {
        return apeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {
        this.apeMaterno = apeMaterno;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    public int getCodColor() {
        return codColor;
    }

    public void setCodColor(int codColor) {
        this.codColor = codColor;
    }
}
