package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.ReaccionDAO;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.*;
import com.orbita.ws.adminws.model.services.ReaccionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReaccionServiceImpl implements ReaccionService {

    @Autowired
    private ReaccionDAO reaccionDAO;

    @Override
    public ListReaccionDTO findAllContact() {
        return reaccionDAO.findAllContact();
    }

    @Override
    public ReacContactadoDTO findByIdContact(BaseRequest request) {
        return reaccionDAO.findByIdContact(request);
    }

    @Override
    public InsertDTO saveContact(ReaccionCSaveRequest request) {
        return reaccionDAO.saveContact(request);
    }

    @Override
    public BaseResponse updateContact(ReaccionCUpdateRequest request) {
        return reaccionDAO.updateContact(request);
    }

    @Override
    public BaseResponse deleteContact(DeleteRequest request) {
        return reaccionDAO.deleteContact(request);
    }

    @Override
    public ListReaccionDTO findAllNoContact() {
        return reaccionDAO.findAllNoContact();
    }

    @Override
    public ReacNoContactadoDTO findByIdNoContact(BaseRequest request) {
        return reaccionDAO.findByIdNoContact(request);
    }

    @Override
    public InsertDTO saveNoContact(ReaccionNCSaveRequest request) {
        return reaccionDAO.saveNoContact(request);
    }

    @Override
    public BaseResponse updateNoContact(ReaccionNCUpdateRequest request) {
        return reaccionDAO.updateNoContact(request);
    }

    @Override
    public BaseResponse deleteNoContact(DeleteRequest request) {
        return reaccionDAO.deleteNoContact(request);
    }
}
