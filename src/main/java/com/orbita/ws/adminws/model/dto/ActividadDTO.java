package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.BaseBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Actividad")
public class ActividadDTO implements Serializable {
    private int codActividad;
    private String actividad;
    private int codColor;
    private List<BaseBean> bases;

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public int getCodColor() {
        return codColor;
    }

    public void setCodColor(int codColor) {
        this.codColor = codColor;
    }

    public List<BaseBean> getBases() {
        return bases;
    }

    public void setBases(List<BaseBean> bases) {
        this.bases = bases;
    }
}
