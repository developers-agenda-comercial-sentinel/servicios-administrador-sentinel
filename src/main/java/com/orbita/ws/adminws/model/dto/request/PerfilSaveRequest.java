package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("REQUEST - Guardar Perfil")
public class PerfilSaveRequest extends InsRequest{

    @ApiModelProperty(value = "Descripción Perfil", required = true)
    private String descripcion;
    @ApiModelProperty(value = "Estado Perfil", required = true)
    private int estado;
    @ApiModelProperty(value = "Listado de Opciones", required = true)
    private List<OpcionRequest> opciones;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public List<OpcionRequest> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionRequest> opciones) {
        this.opciones = opciones;
    }
}
