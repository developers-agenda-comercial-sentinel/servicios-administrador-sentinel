package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.DashCarteraDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashCarteraMapper implements RowMapper<DashCarteraDTO> {
    @Override
    public DashCarteraDTO mapRow(ResultSet rs, int i) throws SQLException {
        DashCarteraDTO dto = new DashCarteraDTO();
        dto.setCliente(rs.getString("CLIENTE"));
        dto.setEstado(rs.getString("ESTADO"));

        return dto;
    }
}
