package com.orbita.ws.adminws.model.beans;

import com.orbita.ws.adminws.util.SpConstants;
import io.swagger.annotations.ApiModel;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

@ApiModel("BEAN - Oportunidad")
public class OportunidadBean implements SQLData {

    private int codCliente;
    private int codOportunidad;
    private int codBase;
    private String nombres;
    private String apePaterno;
    private String apeMaterno;
    private String nroDocumento;

    public int getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(int codCliente) {
        this.codCliente = codCliente;
    }

    public int getCodOportunidad() {
        return codOportunidad;
    }

    public void setCodOportunidad(int codOportunidad) {
        this.codOportunidad = codOportunidad;
    }

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApePaterno() {
        return apePaterno;
    }

    public void setApePaterno(String apePaterno) {
        this.apePaterno = apePaterno;
    }

    public String getApeMaterno() {
        return apeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {
        this.apeMaterno = apeMaterno;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_OPORTUNIDAD;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        codOportunidad = stream.readInt();
        codCliente = stream.readInt();
        codBase = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(codOportunidad);
        stream.writeInt(codCliente);
        stream.writeInt(codBase);
    }
}
