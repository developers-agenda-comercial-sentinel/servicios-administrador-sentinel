package com.orbita.ws.adminws.model.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;

@ApiModel("BEAN - Opción")
public class OpcionBean {

    private int codOpcion;
    private String descripcion;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int codPadre;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int orden;

    public int getCodOpcion() {
        return codOpcion;
    }

    public void setCodOpcion(int codOpcion) {
        this.codOpcion = codOpcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodPadre() {
        return codPadre;
    }

    public void setCodPadre(int codPadre) {
        this.codPadre = codPadre;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

}
