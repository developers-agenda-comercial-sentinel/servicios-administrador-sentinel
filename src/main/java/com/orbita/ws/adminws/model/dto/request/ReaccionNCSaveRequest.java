package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel("REQUEST - Guardar Reaccion No Contactado")
public class ReaccionNCSaveRequest extends InsRequest {

    private String reaccion;
    private int activo;

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

}
