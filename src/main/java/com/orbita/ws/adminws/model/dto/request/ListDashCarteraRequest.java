package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Lista Cartera x Analista y Base")
public class ListDashCarteraRequest {

    @ApiModelProperty(value = "Código del analista", required = true)
    private int codAnalista;
    @ApiModelProperty(value = "Código del base", required = true)
    private int codBase;

	public int getCodAnalista() {
		return codAnalista;
	}

	public void setCodAnalista(int codAnalista) {
		this.codAnalista = codAnalista;
	}

	public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }
}
