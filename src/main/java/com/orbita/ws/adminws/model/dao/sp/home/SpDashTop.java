package com.orbita.ws.adminws.model.dao.sp.home;

import com.orbita.ws.adminws.model.dao.mapper.DashTopEfectivaMapper;
import com.orbita.ws.adminws.model.dao.mapper.DashTopVisitaMapper;
import com.orbita.ws.adminws.model.dto.ListDashTopEfectivaDTO;
import com.orbita.ws.adminws.model.dto.ListDashTopVisitaDTO;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository
public class SpDashTop {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callVisitas;
    private SimpleJdbcCall callEfectivas;

    @PostConstruct
    void init() {
        callVisitas = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_DASH_CITAS)
                        .declareParameters(
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_DASHBOARD, new DashTopVisitaMapper());

        callEfectivas = new SimpleJdbcCall(jdbcTemplate)
                           .withCatalogName(SpConstants.PK_WEB)
                           .withProcedureName(SpConstants.SP_SEL_DASH_EFECTIVAS)
                           .declareParameters(
                                    new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                    new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                           )
                           .returningResultSet(SpConstants.O_CUR_DASHBOARD, new DashTopEfectivaMapper());
    }

    public ListDashTopVisitaDTO execTopVisita() {
        ListDashTopVisitaDTO dto = new ListDashTopVisitaDTO();

        try {
            Map<String, Object> out = callVisitas.execute();
            dto.setLista((List) out.get(SpConstants.O_CUR_DASHBOARD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public ListDashTopEfectivaDTO execTopEfectiva() {
        ListDashTopEfectivaDTO dto = new ListDashTopEfectivaDTO();

        try {
            Map<String, Object> out = callEfectivas.execute();
            dto.setLista((List) out.get(SpConstants.O_CUR_DASHBOARD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
}
