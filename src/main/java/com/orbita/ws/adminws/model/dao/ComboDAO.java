package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.ComboDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;

public interface ComboDAO {

    ComboDTO cboAgencia();
    ComboDTO cboAnalista(BaseRequest request);
}
