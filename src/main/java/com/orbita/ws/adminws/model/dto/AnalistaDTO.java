package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.AnalistaBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Analista")
public class AnalistaDTO extends BaseResponse implements Serializable {

    private List<AnalistaBean> analistas;

    public List<AnalistaBean> getAnalistas() {
        return analistas;
    }

    public void setAnalistas(List<AnalistaBean> analistas) {
        this.analistas = analistas;
    }
}
