package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.ActividadDAO;
import com.orbita.ws.adminws.model.dto.ColorDTO;
import com.orbita.ws.adminws.model.dto.ListActividadDTO;
import com.orbita.ws.adminws.model.services.ActividadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActividadServiceImpl implements ActividadService {

    @Autowired
    private ActividadDAO actividadDAO;

    @Override
    public ListActividadDTO listarActividades() {
        return actividadDAO.listarActividades();
    }

	@Override
	public ColorDTO listarColores() {
		return actividadDAO.listarColores();
	}
}
