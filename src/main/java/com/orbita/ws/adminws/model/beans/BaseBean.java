package com.orbita.ws.adminws.model.beans;

import com.orbita.ws.adminws.util.SpConstants;
import io.swagger.annotations.ApiModel;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

@ApiModel("BEAN - Base")
public class BaseBean {// implements SQLData {

    private int codBase;
    private String Base;
    private String codColor;
    private String actividad;
    private int estado;

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    public String getBase() {
        return Base;
    }

    public void setBase(String base) {
        Base = base;
    }

    public String getCodColor() {
		return codColor;
	}

	public void setCodColor(String codColor) {
		this.codColor = codColor;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	/**
	@Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_BASE;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        codBase = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(codBase);
    }**/
}
