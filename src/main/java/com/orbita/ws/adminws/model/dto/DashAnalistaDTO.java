package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class DashAnalistaDTO implements Serializable {

    private String analista;
    private String email;

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
