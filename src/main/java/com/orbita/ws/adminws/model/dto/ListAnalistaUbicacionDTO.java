package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListAnalistaUbicacionDTO extends BaseResponse implements Serializable {

    List<AnalistaUbicacionDTO> ubicaciones;

    public List<AnalistaUbicacionDTO> getUbicaciones() {
        return ubicaciones;
    }

    public void setUbicaciones(List<AnalistaUbicacionDTO> ubicaciones) {
        this.ubicaciones = ubicaciones;
    }
}
