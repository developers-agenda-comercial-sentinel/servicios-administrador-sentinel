package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.DashTopVisitaDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashTopVisitaMapper implements RowMapper<DashTopVisitaDTO> {

    @Override
    public DashTopVisitaDTO mapRow(ResultSet rs, int i) throws SQLException {
        DashTopVisitaDTO dto = new DashTopVisitaDTO();
        dto.setAnalista(rs.getString("ANALISTA"));
        dto.setCitas(rs.getInt("CITAS"));
        return dto;
    }
}
