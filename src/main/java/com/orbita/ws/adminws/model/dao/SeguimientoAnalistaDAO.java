package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;

public interface SeguimientoAnalistaDAO {

    ListAnalistaUbicacionDTO obtenerUbicacionAnalista(AnalistaSentinelRequest request);
}
