package com.orbita.ws.adminws.model.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Lista Actividad")
public class ListActividadDTO extends BaseResponse implements Serializable {

    private List<ActividadDTO> actividades;

    public List<ActividadDTO> getActividades() {
        return actividades;
    }

    public void setActividades(List<ActividadDTO> actividades) {
        this.actividades = actividades;
    }
}
