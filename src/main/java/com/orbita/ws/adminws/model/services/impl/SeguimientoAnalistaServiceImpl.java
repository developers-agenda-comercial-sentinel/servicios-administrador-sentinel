package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.SeguimientoAnalistaDAO;
import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.services.SeguimientoAnalistaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeguimientoAnalistaServiceImpl implements SeguimientoAnalistaService {

    @Autowired
    private SeguimientoAnalistaDAO seguimientoAnalistaDAO;

    @Override
    public ListAnalistaUbicacionDTO obtenerUbicacionAnalista(AnalistaSentinelRequest request) {
        return seguimientoAnalistaDAO.obtenerUbicacionAnalista(request);
    }
}
