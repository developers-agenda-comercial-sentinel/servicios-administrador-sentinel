package com.orbita.ws.adminws.model.dao.sp.parametria;

import com.orbita.ws.adminws.model.beans.ReaccionBean;
import com.orbita.ws.adminws.model.dao.mapper.ReaccionMapper;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.*;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public class SpNoContactado {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callSel;
    private SimpleJdbcCall callGet;
    private SimpleJdbcCall callIns;
    private SimpleJdbcCall callUpd;
    private SimpleJdbcCall callDel;

    // Inputs - Outputs
    SqlParameterSource in;
    Map<String, Object> out;

    @PostConstruct
    void init() {
        callSel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_REACCION)
                .declareParameters(
                        new SqlParameter(SpConstants.I_TIPO_REACCION, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_REACCION, new ReaccionMapper());

        callGet = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_GET_REACCION)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_REACCION, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_REACCION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_ACTIVIDAD, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MISTI, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_ACTIVO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callIns = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_INS_REACCION_NOCONTACTADO)
                .declareParameters(
                        new SqlParameter(SpConstants.I_DESC_REACCION, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_ACTIVO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_REGISTRO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_REACCION, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callUpd = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_UPD_REACCION_NOCONTACTADO)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_REACCION, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_DESC_REACCION, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_ACTIVO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callDel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_DEL_REACCION)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_REACCION, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );
    }

    public ListReaccionDTO findAllNoContact(){
        ListReaccionDTO dto = new ListReaccionDTO();
        int request = 16;

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_TIPO_REACCION, request);

        try {
            out = callSel.execute(in);
            dto.setReacciones((List)out.get(SpConstants.O_CUR_REACCION));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public ReacNoContactadoDTO findByIdNoContact(BaseRequest request){
        ReacNoContactadoDTO dto = new ReacNoContactadoDTO();
        ReaccionBean bean = new ReaccionBean();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_REACCION, request.getCodigo());

        try {
            out = callGet.execute(in);
            bean.setReaccion((String) out.get(SpConstants.O_REACCION));
            bean.setActivo(((BigDecimal) out.get(SpConstants.O_ACTIVO)).intValue());

            dto.setReaccion(bean);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public InsertDTO saveNoContact(ReaccionNCSaveRequest request) {
        InsertDTO dto = new InsertDTO();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_DESC_REACCION, request.getReaccion())
                .addValue(SpConstants.I_ACTIVO, request.getActivo())
                .addValue(SpConstants.I_USUARIO_REGISTRO, request.getUsuarioRegistro());

        try {
            out = callIns.execute(in);
            dto.setCodigo(((BigDecimal) out.get(SpConstants.O_COD_REACCION)).intValue());
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return  dto;
    }

    public BaseResponse updateNoContact(ReaccionNCUpdateRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_REACCION, request.getCodReaccion())
                .addValue(SpConstants.I_DESC_REACCION, request.getReaccion())
                .addValue(SpConstants.I_ACTIVO, request.getActivo())
                .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callUpd.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }

    public BaseResponse deleteNoContact(DeleteRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_REACCION, request.getCodigo())
                .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callDel.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
}
