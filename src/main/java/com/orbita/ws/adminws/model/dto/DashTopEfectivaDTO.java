package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class DashTopEfectivaDTO implements Serializable {

    private String analista;
    private int efectivas;

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public int getEfectivas() {
        return efectivas;
    }

    public void setEfectivas(int efectivas) {
        this.efectivas = efectivas;
    }
}
