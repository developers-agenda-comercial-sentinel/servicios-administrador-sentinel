package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;

public class DashCarteraDTO implements Serializable {

    private String cliente;
    private String estado;

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
