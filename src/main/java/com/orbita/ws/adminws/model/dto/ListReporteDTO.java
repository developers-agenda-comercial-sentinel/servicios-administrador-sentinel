package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListReporteDTO extends BaseResponse implements Serializable {

    private List<ReporteDTO> reportes;

    public List<ReporteDTO> getReportes() {
        return reportes;
    }

    public void setReportes(List<ReporteDTO> reportes) {
        this.reportes = reportes;
    }
}
