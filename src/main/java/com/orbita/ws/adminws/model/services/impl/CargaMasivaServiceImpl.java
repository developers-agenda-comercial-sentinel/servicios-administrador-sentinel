package com.orbita.ws.adminws.model.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orbita.ws.adminws.model.dao.CargaMasivaDAO;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.CargaCobranzaDTO;
import com.orbita.ws.adminws.model.dto.CargaInactivoDTO;
import com.orbita.ws.adminws.model.dto.CargaRecurrenteDTO;
import com.orbita.ws.adminws.model.dto.CargaReferidoDTO;
import com.orbita.ws.adminws.model.dto.ListCargaHistoricoDTO;
import com.orbita.ws.adminws.model.dto.RutasExcelDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.CargaCobranzaReq;
import com.orbita.ws.adminws.model.dto.request.CargaErradaRequest;
import com.orbita.ws.adminws.model.dto.request.CargaInactivoReq;
import com.orbita.ws.adminws.model.dto.request.CargaRecurrenteReq;
import com.orbita.ws.adminws.model.dto.request.CargaReferidoReq;
import com.orbita.ws.adminws.model.services.CargaMasivaService;

@Service
public class CargaMasivaServiceImpl implements CargaMasivaService {

	@Autowired
	private CargaMasivaDAO cargaMasivaDAO;
	
	@Override
	public RutasExcelDTO listarRutasExcel(BaseRequest request) {
		return cargaMasivaDAO.listarRutasExcel(request);
	}

	@Override
	public BaseResponse cargaRecurrente(CargaRecurrenteReq request) {
		return cargaMasivaDAO.cargaRecurrente(request);
	}

	@Override
	public BaseResponse cargaCobranza(CargaCobranzaReq request) {
		return cargaMasivaDAO.cargaCobranza(request);
	}

	@Override
	public BaseResponse cargaInactivo(CargaInactivoReq request) {
		return cargaMasivaDAO.cargaInactivo(request);
	}

	@Override
	public BaseResponse cargaReferido(CargaReferidoReq request) {
		return cargaMasivaDAO.cargaReferido(request);
	}

	@Override
	public ListCargaHistoricoDTO listarHistorico(BaseRequest request) {
		return cargaMasivaDAO.listarHistorico(request);
	}

	@Override
	public CargaRecurrenteDTO cargaErradaRecurrente(CargaErradaRequest request) {
		return cargaMasivaDAO.cargaErradaRecurrente(request);
	}

	@Override
	public CargaCobranzaDTO cargaErradaCobranza(CargaErradaRequest request) {
		return cargaMasivaDAO.cargaErradaCobranza(request);
	}

	@Override
	public CargaInactivoDTO cargaErradaInactivo(CargaErradaRequest request) {
		return cargaMasivaDAO.cargaErradaInactivo(request);
	}

	@Override
	public CargaReferidoDTO cargaErradaReferido(CargaErradaRequest request) {
		return cargaMasivaDAO.cargaErradaReferido(request);
	}

}
