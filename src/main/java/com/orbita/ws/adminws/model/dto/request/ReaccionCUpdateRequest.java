package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel("REQUEST - Actualizar Reaccion Contactado")
public class ReaccionCUpdateRequest extends UpdRequest {

    private int codReaccion;
    private String reaccion;
    private int codActividad;
    private int activo;
    private int misti;
    private List<ArrayReacDetaRequest> reaccionDetalle;
    private List<ArrayReacDetaRequest> reaccionDetaEliminar;

    public int getCodReaccion() {
        return codReaccion;
    }

    public void setCodReaccion(int codReaccion) {
        this.codReaccion = codReaccion;
    }

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getMisti() {
        return misti;
    }

    public void setMisti(int misti) {
        this.misti = misti;
    }

    public List<ArrayReacDetaRequest> getReaccionDetalle() {
        return reaccionDetalle;
    }

    public void setReaccionDetalle(List<ArrayReacDetaRequest> reaccionDetalle) {
        this.reaccionDetalle = reaccionDetalle;
    }

    public List<ArrayReacDetaRequest> getReaccionDetaEliminar() {
        return reaccionDetaEliminar;
    }

    public void setReaccionDetaEliminar(List<ArrayReacDetaRequest> reaccionDetaEliminar) {
        this.reaccionDetaEliminar = reaccionDetaEliminar;
    }
}
