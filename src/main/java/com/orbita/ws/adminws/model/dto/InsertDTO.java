package com.orbita.ws.adminws.model.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel("DTO - Response Insert")
public class InsertDTO extends BaseResponse implements Serializable {

    private int codigo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
