package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.ReaccionBean;

import java.io.Serializable;

public class ReacNoContactadoDTO extends BaseResponse implements Serializable {

    private ReaccionBean reaccion;

    public ReaccionBean getReaccion() {
        return reaccion;
    }

    public void setReaccion(ReaccionBean reaccion) {
        this.reaccion = reaccion;
    }
}
