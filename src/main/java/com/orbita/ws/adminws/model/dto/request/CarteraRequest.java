package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel("REQUEST - Cartera")
public class CarteraRequest {

    @ApiModelProperty(value = "Código de Analista", required = true)
    private int codUsuario;
    @ApiModelProperty(value = "Lista de Bases", required = true)
    private List<ArrayBaseRequest> bases;
    @ApiModelProperty(value = "Lista de Actividades", required = true)
    private List<ArrayActividadRequest> actividades;
    @ApiModelProperty(value = "Número de página", required = true)
    private int pageNumber;
    @ApiModelProperty(value = "Cantidad de Registros por página", required = true)
    private int pageSize;

	public int getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(int codUsuario) {
		this.codUsuario = codUsuario;
	}

	public List<ArrayBaseRequest> getBases() {
        return bases;
    }

    public void setBases(List<ArrayBaseRequest> bases) {
        this.bases = bases;
    }

    public List<ArrayActividadRequest> getActividades() {
        return actividades;
    }

    public void setActividades(List<ArrayActividadRequest> actividades) {
        this.actividades = actividades;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
