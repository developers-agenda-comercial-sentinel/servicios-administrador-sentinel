package com.orbita.ws.adminws.model.dto.request;

public class CargaErradaRequest {

	private int codCarga;
	private int codActividad;
	
	public int getCodCarga() {
		return codCarga;
	}
	public void setCodCarga(int codCarga) {
		this.codCarga = codCarga;
	}
	public int getCodActividad() {
		return codActividad;
	}
	public void setCodActividad(int codActividad) {
		this.codActividad = codActividad;
	}
	
}
