package com.orbita.ws.adminws.model.dao.sp.agenda;

import com.orbita.ws.adminws.model.dao.mapper.CboDistritoMapper;
import com.orbita.ws.adminws.model.dao.mapper.ComboMapper;
import com.orbita.ws.adminws.model.dto.ListRegAgendaCboDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository
public class SpListarCboAgenda {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callDistritos;
    private SimpleJdbcCall callActividades;

    @PostConstruct
    void init() {
        callDistritos = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_DISTRITOS_CARTERA)
                .declareParameters(
                		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_CARTERA_DISTRITO, new CboDistritoMapper());

        callActividades = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_ACTIVIDAD_CARTERA)
                .declareParameters(
                		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_CARTERA_ACTIVIDAD, new ComboMapper());
    }

    public ListRegAgendaCboDTO execute(AnalistaSentinelRequest request) {
        ListRegAgendaCboDTO dto = new ListRegAgendaCboDTO();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_CODUSUARIO, request.getCodigo());

        try {
            Map<String, Object> outD = callDistritos.execute(in);
            dto.setDistritos((List) outD.get(SpConstants.O_CUR_CARTERA_DISTRITO));

            Map<String, Object> outA = callActividades.execute(in);
            dto.setActividades((List) outA.get(SpConstants.O_CUR_CARTERA_ACTIVIDAD));


            dto.setCodVal((String) outD.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) outD.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
}
