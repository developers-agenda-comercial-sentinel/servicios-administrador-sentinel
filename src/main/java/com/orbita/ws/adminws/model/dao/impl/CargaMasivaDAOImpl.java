package com.orbita.ws.adminws.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orbita.ws.adminws.model.dao.CargaMasivaDAO;
import com.orbita.ws.adminws.model.dao.sp.cargamasiva.SpCargaErrada;
import com.orbita.ws.adminws.model.dao.sp.cargamasiva.SpCargaHistorico;
import com.orbita.ws.adminws.model.dao.sp.cargamasiva.SpCargaMasiva;
import com.orbita.ws.adminws.model.dao.sp.cargamasiva.SpRutasExcel;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.CargaCobranzaDTO;
import com.orbita.ws.adminws.model.dto.CargaInactivoDTO;
import com.orbita.ws.adminws.model.dto.CargaRecurrenteDTO;
import com.orbita.ws.adminws.model.dto.CargaReferidoDTO;
import com.orbita.ws.adminws.model.dto.ListCargaHistoricoDTO;
import com.orbita.ws.adminws.model.dto.RutasExcelDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.CargaCobranzaReq;
import com.orbita.ws.adminws.model.dto.request.CargaErradaRequest;
import com.orbita.ws.adminws.model.dto.request.CargaInactivoReq;
import com.orbita.ws.adminws.model.dto.request.CargaRecurrenteReq;
import com.orbita.ws.adminws.model.dto.request.CargaReferidoReq;

@Service
public class CargaMasivaDAOImpl implements CargaMasivaDAO{

	@Autowired
	private SpRutasExcel spRutasExcel; 
	
	@Autowired
	private SpCargaMasiva spCargaMasiva;
	
	@Autowired
	private SpCargaHistorico spCargaHistorico;
	
	@Autowired
	private SpCargaErrada SpCargaErrada;
	
	@Override
	public RutasExcelDTO listarRutasExcel(BaseRequest request) {
		return spRutasExcel.execute(request);
	}

	@Override
	public BaseResponse cargaRecurrente(CargaRecurrenteReq request) {
		return spCargaMasiva.cargaRecurrente(request);
	}

	@Override
	public BaseResponse cargaCobranza(CargaCobranzaReq request) {
		return spCargaMasiva.cargaCobranza(request);
	}

	@Override
	public BaseResponse cargaInactivo(CargaInactivoReq request) {
		return spCargaMasiva.cargaInactivo(request);
	}

	@Override
	public BaseResponse cargaReferido(CargaReferidoReq request) {
		return spCargaMasiva.cargaReferido(request);
	}

	@Override
	public ListCargaHistoricoDTO listarHistorico(BaseRequest request) {
		return spCargaHistorico.execute(request);
	}

	@Override
	public CargaRecurrenteDTO cargaErradaRecurrente(CargaErradaRequest request) {
		return SpCargaErrada.CargaErradaRecurrente(request);
	}

	@Override
	public CargaCobranzaDTO cargaErradaCobranza(CargaErradaRequest request) {
		return SpCargaErrada.CargaErradacobranza(request);
	}

	@Override
	public CargaInactivoDTO cargaErradaInactivo(CargaErradaRequest request) {
		return SpCargaErrada.CargaErradaInactivo(request);
	}

	@Override
	public CargaReferidoDTO cargaErradaReferido(CargaErradaRequest request) {
		return SpCargaErrada.CargaErradaReferido(request);
	}

}
