package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.beans.ColorBean;

public class ColorDTO extends BaseResponse implements Serializable{

	private List<ColorBean> colores;

	public List<ColorBean> getColores() {
		return colores;
	}

	public void setColores(List<ColorBean> colores) {
		this.colores = colores;
	}
	
}
