package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.AnalistaDAO;
import com.orbita.ws.adminws.model.dao.sp.analista.SpAnalista;
import com.orbita.ws.adminws.model.dto.AnalistaDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnalistaDAOImpl implements AnalistaDAO {

    @Autowired
    private SpAnalista spAnalista;

    @Override
    public AnalistaDTO listarAnalistas(AnalistaRequest analistaRequest) {
        return spAnalista.execute(analistaRequest);
    }
}
