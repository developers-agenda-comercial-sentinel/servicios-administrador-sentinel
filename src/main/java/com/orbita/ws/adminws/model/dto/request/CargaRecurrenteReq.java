package com.orbita.ws.adminws.model.dto.request;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaRecurrenteReq;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Carga Recurrente")
public class CargaRecurrenteReq implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "Nombre del Archivo")
	private String nomArchivo;
	
	@ApiModelProperty(value = "Lista de Recurrentes")
	private List<ArrayCargaRecurrenteReq> cargaRecurrente;
	
	public String getNomArchivo() {
		return nomArchivo;
	}

	public void setNomArchivo(String nomArchivo) {
		this.nomArchivo = nomArchivo;
	}

	public List<ArrayCargaRecurrenteReq> getCargaRecurrente() {
		return cargaRecurrente;
	}

	public void setCargaRecurrente(List<ArrayCargaRecurrenteReq> cargaRecurrente) {
		this.cargaRecurrente = cargaRecurrente;
	}

}
