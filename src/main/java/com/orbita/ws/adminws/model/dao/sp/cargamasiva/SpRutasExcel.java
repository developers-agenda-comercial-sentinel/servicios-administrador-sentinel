package com.orbita.ws.adminws.model.dao.sp.cargamasiva;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dto.RutasExcelDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpRutasExcel {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall jdbcCall;
	
	@PostConstruct
	void init() {
		jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_RUTAS_RECURSOS)
                .declareParameters(
                		new SqlParameter(SpConstants.I_TIPO, OracleTypes.NUMBER),
                		new SqlOutParameter(SpConstants.O_REC_PATH, OracleTypes.VARCHAR),
                		new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );
	}
	
	public RutasExcelDTO execute(BaseRequest request) {
		RutasExcelDTO dto = new RutasExcelDTO();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_TIPO, request.getCodigo());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setPath((String) out.get(SpConstants.O_REC_PATH));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
	}
}
