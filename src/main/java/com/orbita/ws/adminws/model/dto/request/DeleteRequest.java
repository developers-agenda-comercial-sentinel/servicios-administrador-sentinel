package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Eliminar Registro")
public class DeleteRequest extends UpdRequest{

    @ApiModelProperty(value = "Código a eliminar", required = true)
    private int codigo;
    @ApiModelProperty(value = "Usuario que elimina", required = true)
    private String usuarioModificacion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getUsuarioModificacion() {
        return usuarioModificacion;
    }

    @Override
    public void setUsuarioModificacion(String usuarioModificacion) {
        this.usuarioModificacion = usuarioModificacion;
    }
}
