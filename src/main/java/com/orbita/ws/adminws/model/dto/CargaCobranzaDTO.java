package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaCobranzaReq;

public class CargaCobranzaDTO extends BaseResponse implements Serializable {
	
	private List<ArrayCargaCobranzaReq> cargaCobranza;

	public List<ArrayCargaCobranzaReq> getCargaCobranza() {
		return cargaCobranza;
	}

	public void setCargaCobranza(List<ArrayCargaCobranzaReq> cargaCobranza) {
		this.cargaCobranza = cargaCobranza;
	}

}
