package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.ReaccionDAO;
import com.orbita.ws.adminws.model.dao.sp.parametria.SpContactado;
import com.orbita.ws.adminws.model.dao.sp.parametria.SpNoContactado;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReaccionDAOImpl implements ReaccionDAO {

    @Autowired
    private SpContactado spContactado;

    @Autowired
    private SpNoContactado spNoContactado;

    @Override
    public ListReaccionDTO findAllContact() {
        return spContactado.findAllContact();
    }

    @Override
    public ReacContactadoDTO findByIdContact(BaseRequest request) {
        return spContactado.findByIdContact(request);
    }

    @Override
    public InsertDTO saveContact(ReaccionCSaveRequest request) {
        return spContactado.saveContact(request);
    }

    @Override
    public BaseResponse updateContact(ReaccionCUpdateRequest request) {
        return spContactado.updateContact(request);
    }

    @Override
    public BaseResponse deleteContact(DeleteRequest request) {
        return spContactado.deleteContact(request);
    }

    @Override
    public ListReaccionDTO findAllNoContact() {
        return spNoContactado.findAllNoContact();
    }

    @Override
    public ReacNoContactadoDTO findByIdNoContact(BaseRequest request) {
        return spNoContactado.findByIdNoContact(request);
    }

    @Override
    public InsertDTO saveNoContact(ReaccionNCSaveRequest request) {
        return spNoContactado.saveNoContact(request);
    }

    @Override
    public BaseResponse updateNoContact(ReaccionNCUpdateRequest request) {
        return spNoContactado.updateNoContact(request);
    }

    @Override
    public BaseResponse deleteNoContact(DeleteRequest request) {
        return spNoContactado.deleteNoContact(request);
    }
}
