package com.orbita.ws.adminws.model.dao.sp.cargamasiva;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.orbita.ws.adminws.model.dao.mapper.CargaCobranzaMapper;
import com.orbita.ws.adminws.model.dao.mapper.CargaInactivoMapper;
import com.orbita.ws.adminws.model.dao.mapper.CargaRecurrenteMapper;
import com.orbita.ws.adminws.model.dao.mapper.CargaReferidoMapper;
import com.orbita.ws.adminws.model.dto.CargaCobranzaDTO;
import com.orbita.ws.adminws.model.dto.CargaInactivoDTO;
import com.orbita.ws.adminws.model.dto.CargaRecurrenteDTO;
import com.orbita.ws.adminws.model.dto.CargaReferidoDTO;
import com.orbita.ws.adminws.model.dto.request.CargaErradaRequest;
import com.orbita.ws.adminws.util.SpConstants;

import oracle.jdbc.OracleTypes;

@Repository
public class SpCargaErrada {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall jdbcCallRec;
	private SimpleJdbcCall jdbcCallCob;
	private SimpleJdbcCall jdbcCallInac;
	private SimpleJdbcCall jdbcCallRef;
	
	SqlParameterSource in;
	
	@PostConstruct
	void init() {
		jdbcCallRec = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_SEL_CARGA_ERRADA)
				.declareParameters(
						new SqlParameter(SpConstants.I_CODIGO_CARGA, OracleTypes.NUMBER),
						new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_CARGA, new CargaRecurrenteMapper());
		
		jdbcCallCob = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_SEL_CARGA_ERRADA)
				.declareParameters(
						new SqlParameter(SpConstants.I_CODIGO_CARGA, OracleTypes.NUMBER),
						new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_CARGA, new CargaCobranzaMapper());
		
		jdbcCallInac = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_SEL_CARGA_ERRADA)
				.declareParameters(
						new SqlParameter(SpConstants.I_CODIGO_CARGA, OracleTypes.NUMBER),
						new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_CARGA, new CargaInactivoMapper());
		
		jdbcCallRef = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(SpConstants.PK_WEB_LOAD)
				.withProcedureName(SpConstants.SP_SEL_CARGA_ERRADA)
				.declareParameters(
						new SqlParameter(SpConstants.I_CODIGO_CARGA, OracleTypes.NUMBER),
						new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
						new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
				)
				.returningResultSet(SpConstants.O_CUR_CARGA, new CargaReferidoMapper());
	}
	
	public CargaRecurrenteDTO CargaErradaRecurrente(CargaErradaRequest request) {
		CargaRecurrenteDTO dto = new CargaRecurrenteDTO();
		
		in = new MapSqlParameterSource()
				.addValue(SpConstants.I_CODIGO_CARGA, request.getCodCarga())
				.addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad());
		
		try {
			Map<String, Object> out = jdbcCallRec.execute(in);
			dto.setCargaRecurrente((List) out.get(SpConstants.O_CUR_CARGA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
            
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return dto;
	}
	
	public CargaCobranzaDTO CargaErradacobranza(CargaErradaRequest request) {
		CargaCobranzaDTO dto = new CargaCobranzaDTO();
		
		in = new MapSqlParameterSource()
				.addValue(SpConstants.I_CODIGO_CARGA, request.getCodCarga())
				.addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad());
		
		try {
			Map<String, Object> out = jdbcCallCob.execute(in);
			dto.setCargaCobranza((List) out.get(SpConstants.O_CUR_CARGA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
            
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return dto;
	}
	
	public CargaInactivoDTO CargaErradaInactivo(CargaErradaRequest request) {
		CargaInactivoDTO dto = new CargaInactivoDTO();
		
		in = new MapSqlParameterSource()
				.addValue(SpConstants.I_CODIGO_CARGA, request.getCodCarga())
				.addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad());
		
		try {
			Map<String, Object> out = jdbcCallInac.execute(in);
			dto.setCargaInactivo((List) out.get(SpConstants.O_CUR_CARGA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
            
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return dto;
	}
	
	public CargaReferidoDTO CargaErradaReferido(CargaErradaRequest request) {
		CargaReferidoDTO dto = new CargaReferidoDTO();
		
		in = new MapSqlParameterSource()
				.addValue(SpConstants.I_CODIGO_CARGA, request.getCodCarga())
				.addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad());
		
		try {
			Map<String, Object> out = jdbcCallRef.execute(in);
			dto.setCargaReferido((List) out.get(SpConstants.O_CUR_CARGA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
            
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return dto;
	}
	
}
