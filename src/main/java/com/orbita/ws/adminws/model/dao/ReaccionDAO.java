package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.*;

public interface ReaccionDAO {

    ListReaccionDTO findAllContact();
    ReacContactadoDTO findByIdContact(BaseRequest request);
    InsertDTO saveContact(ReaccionCSaveRequest request);
    BaseResponse updateContact(ReaccionCUpdateRequest request);
    BaseResponse deleteContact(DeleteRequest request);

    ListReaccionDTO findAllNoContact();
    ReacNoContactadoDTO findByIdNoContact(BaseRequest request);
    InsertDTO saveNoContact(ReaccionNCSaveRequest request);
    BaseResponse updateNoContact(ReaccionNCUpdateRequest request);
    BaseResponse deleteNoContact(DeleteRequest request);
}
