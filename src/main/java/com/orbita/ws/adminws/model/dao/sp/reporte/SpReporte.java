package com.orbita.ws.adminws.model.dao.sp.reporte;

import com.orbita.ws.adminws.model.dao.mapper.ListaReporteMapper;
import com.orbita.ws.adminws.model.dao.mapper.ReporteMapper;
import com.orbita.ws.adminws.model.dto.ListReporteDTO;
import com.orbita.ws.adminws.model.dto.ListReporteDetalleDTO;
import com.orbita.ws.adminws.model.dto.ReporteDetalleDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ReporteDetalleRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository
public class SpReporte {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callListReport;
    private SimpleJdbcCall callReportes;
    private SimpleJdbcCall callDetaReport;

    SqlParameterSource in;

    @PostConstruct
    void init() {
        callListReport = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_REPORTE)
                        .declareParameters(
                                new SqlParameter(SpConstants.I_TIPO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_NOM_REPORTE, OracleTypes.VARCHAR),
                                new SqlParameter(SpConstants.I_ID_GROUP, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_REPORTE, new ListaReporteMapper());

        callReportes = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_REPORTE)
                        .declareParameters(
                                new SqlParameter(SpConstants.I_TIPO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_NOM_REPORTE, OracleTypes.VARCHAR),
                                new SqlParameter(SpConstants.I_ID_GROUP, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_REPORTE, new ReporteMapper());

        callDetaReport = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_REPORTE)
                        .declareParameters(
                                new SqlParameter(SpConstants.I_TIPO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_NOM_REPORTE, OracleTypes.VARCHAR),
                                new SqlParameter(SpConstants.I_ID_GROUP, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_REPORTE, new ReporteMapper());
    }

    public ListReporteDTO listarReportes(AnalistaSentinelRequest request) {
        ListReporteDTO dto = new ListReporteDTO();

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_TIPO, 1)
                .addValue(SpConstants.I_CODUSUARIO, request.getCodigo())
                .addValue(SpConstants.I_NOM_REPORTE, null)
                .addValue(SpConstants.I_ID_GROUP, null);

        try {
            Map<String, Object> out = callListReport.execute(in);
            dto.setReportes((List) out.get(SpConstants.O_CUR_REPORTE));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public ListReporteDetalleDTO getReporte(ReporteDetalleRequest request) {
        ListReporteDetalleDTO dto = new ListReporteDetalleDTO();
        List<ReporteDetalleDTO> cabeceras;
        List<ReporteDetalleDTO> detalle;

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_TIPO, 2)
                .addValue(SpConstants.I_CODUSUARIO, request.getCodAnalista())
                .addValue(SpConstants.I_NOM_REPORTE, request.getNomReporte())
                .addValue(SpConstants.I_ID_GROUP, null);

        try {
            Map<String, Object> out = callReportes.execute(in);
            cabeceras = (List) out.get(SpConstants.O_CUR_REPORTE);

            for (ReporteDetalleDTO d : cabeceras) {
                in = new MapSqlParameterSource()
                        .addValue(SpConstants.I_TIPO, 3)
                        .addValue(SpConstants.I_CODUSUARIO, request.getCodAnalista())
                        .addValue(SpConstants.I_NOM_REPORTE, request.getNomReporte())
                        .addValue(SpConstants.I_ID_GROUP, d.getGrupo());

                Map<String, Object> outDetalle = callDetaReport.execute(in);
                detalle = (List) outDetalle.get(SpConstants.O_CUR_REPORTE);
                d.setDetalle(detalle);
            }

            dto.setReportes(cabeceras);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
}
