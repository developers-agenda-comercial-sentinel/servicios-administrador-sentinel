package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.beans.ActividadBean;

public class ListConfigActividadDTO extends BaseResponse implements Serializable {

	private List<ActividadBean> lista;

    public List<ActividadBean> getLista() {
        return lista;
    }

    public void setLista(List<ActividadBean> lista) {
        this.lista = lista;
    }
}
