package com.orbita.ws.adminws.model.dto;

import com.orbita.ws.adminws.model.beans.OpcionBean;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("Listado de Opciones")
public class OpcionDTO extends BaseResponse implements Serializable {

    private List<OpcionBean> opciones;

    public List<OpcionBean> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<OpcionBean> opciones) {
        this.opciones = opciones;
    }
}
