package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListCalendarioDTO;
import com.orbita.ws.adminws.model.dto.ListCartAgendaDTO;
import com.orbita.ws.adminws.model.dto.ListRegAgendaCboDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.CalendarioRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraAgendaRequest;
import com.orbita.ws.adminws.model.dto.request.RegCitaRequest;

public interface AgendaService {

    ListCalendarioDTO execCalendario(CalendarioRequest request);
    ListRegAgendaCboDTO execute(AnalistaSentinelRequest request);
    ListCartAgendaDTO listarCartera(CarteraAgendaRequest request);
    BaseResponse registrarAgenda(RegCitaRequest request);

}
