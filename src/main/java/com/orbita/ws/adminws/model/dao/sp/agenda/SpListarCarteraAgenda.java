package com.orbita.ws.adminws.model.dao.sp.agenda;

import com.orbita.ws.adminws.model.dao.mapper.CartAgendaMapper;
import com.orbita.ws.adminws.model.dto.ListCartAgendaDTO;
import com.orbita.ws.adminws.model.dto.request.CarteraAgendaRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Repository
public class SpListarCarteraAgenda {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall jdbcCall;

    @PostConstruct
    void init() {
        jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_CARTERA_AGENDA)
                .declareParameters(
                		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_COD_BASE, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_COD_UBIGEO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_CARTERA, new CartAgendaMapper());
    }

    public ListCartAgendaDTO listarCartera(CarteraAgendaRequest request) {
        ListCartAgendaDTO dto = new ListCartAgendaDTO();

        SqlParameterSource in = new MapSqlParameterSource()
        		.addValue(SpConstants.I_CODUSUARIO, request.getCodAnalista())
                .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad())
                .addValue(SpConstants.I_COD_BASE, request.getCodBase())
                .addValue(SpConstants.I_COD_UBIGEO, request.getUbigeo());

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            dto.setLista((List) out.get(SpConstants.O_CUR_CARTERA));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }
}
