package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.ComboDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;

public interface ComboService {

    ComboDTO cboAgencia();
    ComboDTO cboAnalista(BaseRequest request);

}
