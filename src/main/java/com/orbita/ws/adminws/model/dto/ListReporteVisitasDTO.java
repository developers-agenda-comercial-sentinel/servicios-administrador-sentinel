package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListReporteVisitasDTO extends BaseResponse implements Serializable {

    private List<ReporteVisitasDTO> repVisitasRealizadas;

    public List<ReporteVisitasDTO> getRepVisitasRealizadas() {
        return repVisitasRealizadas;
    }

    public void setRepVisitasRealizadas(List<ReporteVisitasDTO> repVisitasRealizadas) {
        this.repVisitasRealizadas = repVisitasRealizadas;
    }
}
