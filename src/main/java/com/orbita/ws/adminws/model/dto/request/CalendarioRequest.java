package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Calendario")
public class CalendarioRequest {

    @ApiModelProperty(value = "Código de analista", required = true)
    private int codAnalista;
    @ApiModelProperty(value = "Mes a consultar", required = true)
    private int mes;
    @ApiModelProperty(value = "Año a consultar", required = true)
    private int anio;

    public int getCodAnalista() {
		return codAnalista;
	}

	public void setCodAnalista(int codAnalista) {
		this.codAnalista = codAnalista;
	}

	public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }
}
