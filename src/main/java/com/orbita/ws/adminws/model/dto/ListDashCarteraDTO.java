package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListDashCarteraDTO extends BaseResponse implements Serializable {

    private List<DashCarteraDTO> lista;

    public List<DashCarteraDTO> getLista() {
        return lista;
    }

    public void setLista(List<DashCarteraDTO> lista) {
        this.lista = lista;
    }
}
