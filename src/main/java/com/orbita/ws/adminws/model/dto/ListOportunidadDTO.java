package com.orbita.ws.adminws.model.dto;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("DTO - Oportunidad")
public class ListOportunidadDTO extends BaseResponse implements Serializable {

    private List<OportunidadDTO> oportunidades;

    public List<OportunidadDTO> getOportunidades() {
        return oportunidades;
    }

    public void setOportunidades(List<OportunidadDTO> oportunidades) {
        this.oportunidades = oportunidades;
    }
}
