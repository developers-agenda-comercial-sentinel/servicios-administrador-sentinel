package com.orbita.ws.adminws.model.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;

@ApiModel("BEAN - Reacción")
public class ReaccionBean {

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int codReaccion;
    private String reaccion;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int codActividad;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String actividad;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int misti;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private int activo;

    public int getCodReaccion() {
        return codReaccion;
    }

    public void setCodReaccion(int codReaccion) {
        this.codReaccion = codReaccion;
    }

    public String getReaccion() {
        return reaccion;
    }

    public void setReaccion(String reaccion) {
        this.reaccion = reaccion;
    }

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public int getMisti() {
        return misti;
    }

    public void setMisti(int misti) {
        this.misti = misti;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
