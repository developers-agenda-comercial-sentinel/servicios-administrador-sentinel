package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.CarteraDAO;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpAsignarCartera;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpAsignarOportunidad;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpCartera;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpTransferirCliente;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.AsignarCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.AsignarOportunidadRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraRequest;
import com.orbita.ws.adminws.model.dto.request.TransferirClienteRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarteraDAOImpl implements CarteraDAO {

    @Autowired
    private SpCartera spCartera;

    @Autowired
    private SpTransferirCliente transferirCliente;

    @Autowired
    private SpAsignarOportunidad asignarOportunidad;

    @Autowired
    private SpAsignarCartera asignarCartera;

    @Override
    public CarteraDTO listarCartera(CarteraRequest request) {
        return spCartera.execute(request);
    }

    @Override
    public BaseResponse transferirCliente(TransferirClienteRequest request) {
        return transferirCliente.execute(request);
    }

    @Override
    public BaseResponse asignarOportunidad(AsignarOportunidadRequest request) {
        return asignarOportunidad.execute(request);
    }

    @Override
    public BaseResponse asignarCartera(AsignarCarteraRequest request) {
        return asignarCartera.execute(request);
    }
}
