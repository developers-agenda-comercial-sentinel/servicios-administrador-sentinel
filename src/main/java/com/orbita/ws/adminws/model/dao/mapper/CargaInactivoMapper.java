package com.orbita.ws.adminws.model.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaInactivoReq;

public class CargaInactivoMapper implements RowMapper<ArrayCargaInactivoReq>{

	@Override
	public ArrayCargaInactivoReq mapRow(ResultSet rs, int rowNum) throws SQLException {
		ArrayCargaInactivoReq bean = new ArrayCargaInactivoReq();
		bean.setNroDocCliente(rs.getString("NRO_DOCUMENTO"));
		bean.setTipoDocumento(rs.getString("TIPO_DOCUMENTO"));
		bean.setNomCliente(rs.getString("NOMBRE_CLIENTE"));
		bean.setApePaternoCliente(rs.getString("APEPATERNO_CLIENTE"));
		bean.setApeMaternoCliente(rs.getString("APEMATERNO_CLIENTE"));
		bean.setCodBase(rs.getString("COD_BASE"));
		bean.setNroTelefono1(rs.getString("NRO_TELEF_1"));
		bean.setDirecDomicilio(rs.getString("DIRECCION_DOMICILIO"));
		bean.setRefDomicilio(rs.getString("REF_DOMICILIO"));
		bean.setUbigeoDomicilio(rs.getString("UBIGEO_DOMICILIO"));
		bean.setLatDomicilio(rs.getString("LATITUD_DOMICILIO"));
		bean.setLonDomicilio(rs.getString("LONGITUD_DOMICILIO"));
		bean.setDirecNegocio(rs.getString("DIRECCION_NEGOCIO"));
		bean.setRefNegocio(rs.getString("REF_NEGOCIO"));
		bean.setUbigeoNegocio(rs.getString("UBIGEO_NEGOCIO"));
		bean.setLatNegocio(rs.getString("LATITUD_NEGOCIO"));
		bean.setLonNegocio(rs.getString("LONGITUD_NEGOCIO"));
		bean.setUsuarioRegistro(rs.getString("USUARIO_REGISTRO"));
		bean.setEdad(rs.getString("EDAD"));
		bean.setSegmentacion(rs.getString("SEGMENTACION"));
		bean.setNroEntSFTot(rs.getString("NRO_ENT_SF_TOT"));
		bean.setNroDocAnalista(rs.getString("DNI_ANALISTA"));
		bean.setFechaCita(rs.getString("FECHA_CITA"));
		bean.setCorreo(rs.getString("CORREO"));
		bean.setNroTelefono2(rs.getString("NRO_TELEF_2"));
		bean.setNroTelefono3(rs.getString("NRO_TELEF_3"));
		bean.setFecUltDesembolso(rs.getString("FEC_ULT_DESEMBOLSO"));
		bean.setUltMontoCred(rs.getString("ULT_MONTO_CRED"));
		bean.setFecRegistro(rs.getString("FEC_REGISTRO"));
		bean.setEntidadCompartida(rs.getString("ENTIDAD_COMPARTIDA"));
		bean.setInfoAdicional(rs.getString("INFO_ADICIONAL"));
		
		return bean;
	}

}
