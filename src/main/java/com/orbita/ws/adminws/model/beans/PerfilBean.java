package com.orbita.ws.adminws.model.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;

@ApiModel("BEAN - Perfil")
public class PerfilBean {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long codPerfil;
    private String descripcion;
    private int estado;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String usuarioRegistro;

    public Long getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(Long codPerfil) {
        this.codPerfil = codPerfil;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }
}
