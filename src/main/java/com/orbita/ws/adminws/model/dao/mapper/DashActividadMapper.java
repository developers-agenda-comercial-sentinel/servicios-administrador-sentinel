package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.DashActividadDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashActividadMapper implements RowMapper<DashActividadDTO> {

    @Override
    public DashActividadDTO mapRow(ResultSet rs, int i) throws SQLException {
        DashActividadDTO dto = new DashActividadDTO();
        dto.setCodActividad(rs.getInt("COD_ACTIVIDAD"));
        dto.setActividad(rs.getString("ACTIVIDAD"));
        dto.setPorcentaje(rs.getInt("PORCENTAJE"));

        return dto;
    }
}
