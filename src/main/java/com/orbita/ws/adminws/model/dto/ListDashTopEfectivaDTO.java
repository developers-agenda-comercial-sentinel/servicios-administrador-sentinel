package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListDashTopEfectivaDTO extends BaseResponse implements Serializable {

    private List<DashTopEfectivaDTO> lista;

    public List<DashTopEfectivaDTO> getLista() {
        return lista;
    }

    public void setLista(List<DashTopEfectivaDTO> lista) {
        this.lista = lista;
    }
}
