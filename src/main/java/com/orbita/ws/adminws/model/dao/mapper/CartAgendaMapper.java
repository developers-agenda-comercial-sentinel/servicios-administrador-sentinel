package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.dto.CarteraAgendaDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CartAgendaMapper implements RowMapper<CarteraAgendaDTO> {

    @Override
    public CarteraAgendaDTO mapRow(ResultSet rs, int i) throws SQLException {
        CarteraAgendaDTO dto = new CarteraAgendaDTO();
        dto.setCodCliente(rs.getInt("COD_CLIENTE"));
        dto.setNombres(rs.getString("NOMBRES"));
        dto.setApePaterno(rs.getString("APE_PATERNO"));
        dto.setApeMaterno(rs.getString("APE_MATERNO"));
        dto.setCodGestion(rs.getInt("COD_GESTION"));
        return dto;
    }
}
