package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModelProperty;

public class AnalistaSentinelRequest {

    @ApiModelProperty(value = "Código del Analista", required = true)
    private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	
	
}
