package com.orbita.ws.adminws.model.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orbita.ws.adminws.util.SpConstants;
import io.swagger.annotations.ApiModel;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

@ApiModel("BEAN - Gestión")
public class GestionBean implements SQLData {

    private int codGestion;

    public int getCodGestion() {
        return codGestion;
    }

    public void setCodGestion(int codGestion) {
        this.codGestion = codGestion;
    }

    @JsonIgnore
    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_GESTION;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        codGestion = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(codGestion);
    }
}
