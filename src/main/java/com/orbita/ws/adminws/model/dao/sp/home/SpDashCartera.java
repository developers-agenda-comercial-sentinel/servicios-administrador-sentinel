package com.orbita.ws.adminws.model.dao.sp.home;

import com.orbita.ws.adminws.model.beans.BaseBean;
import com.orbita.ws.adminws.model.dao.mapper.BaseMapper;
import com.orbita.ws.adminws.model.dao.mapper.DashActividadMapper;
import com.orbita.ws.adminws.model.dao.mapper.DashAnalistaBaseMapper;
import com.orbita.ws.adminws.model.dao.mapper.DashCarteraMapper;
import com.orbita.ws.adminws.model.dto.DashActividadDTO;
import com.orbita.ws.adminws.model.dto.ListBaseDTO;
import com.orbita.ws.adminws.model.dto.ListDashActividadDTO;
import com.orbita.ws.adminws.model.dto.ListDashAnalistaBaseDTO;
import com.orbita.ws.adminws.model.dto.ListDashCarteraDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaBaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListDashCarteraRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class SpDashCartera {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callActividad;
    private SimpleJdbcCall callBase;
    private SimpleJdbcCall callAnalista;
    private SimpleJdbcCall callCartera;

    SqlParameterSource in;

    @PostConstruct
    void init() {
        callActividad = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_DASH_ACTIVIDAD)
                        .declareParameters(
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_DASHBOARD, new DashActividadMapper());

        callBase = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_BASE)
                        .declareParameters(
                                new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_BASE, new BaseMapper());

        callAnalista = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_DASH_ANALISTAXBASE)
                        .declareParameters(
                                new SqlParameter(SpConstants.I_COD_BASE, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_PAGE_NUMBER, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_PAGE_SIZE, OracleTypes.NUMBER),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_DASHBOARD, new DashAnalistaBaseMapper());

        callCartera = new SimpleJdbcCall(jdbcTemplate)
                        .withCatalogName(SpConstants.PK_WEB)
                        .withProcedureName(SpConstants.SP_SEL_DASH_CARTXANALISTA)
                        .declareParameters(
                        		new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                                new SqlParameter(SpConstants.I_COD_BASE, OracleTypes.NUMBER),
                                new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                                new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                        )
                        .returningResultSet(SpConstants.O_CUR_DASHBOARD, new DashCarteraMapper());
    }

    public ListDashActividadDTO execActividad() {
        ListDashActividadDTO dto = new ListDashActividadDTO();

        try {
            Map<String, Object> out = callActividad.execute();
            dto.setLista((List) out.get(SpConstants.O_CUR_DASHBOARD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return dto;
    }

    public ListBaseDTO execBase(BaseRequest request) {
    	List<DashActividadDTO> listActividad;
    	List<BaseBean> listBase;
    	Optional<DashActividadDTO> dtoA;
        ListBaseDTO dto = new ListBaseDTO();
        
        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodigo());

        try {
        	
        	listActividad = execActividad().getLista();
        	dtoA = listActividad.stream().filter(a -> a.getCodActividad() == request.getCodigo()).findFirst();

            Map<String, Object> out = callBase.execute(in);
            listBase = (List) out.get(SpConstants.O_CUR_BASE);
            listBase = listBase.stream().filter(b -> b.getActividad().equals(dtoA.get().getActividad())).collect(Collectors.toList());
            
            dto.setBases(listBase);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return dto;
    }

    public ListDashAnalistaBaseDTO execAnalistaBase(ListAnalistaBaseRequest request) {
        ListDashAnalistaBaseDTO dto = new ListDashAnalistaBaseDTO();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_COD_BASE, request.getCodBase())
            .addValue(SpConstants.I_PAGE_NUMBER, request.getPageNumber())
            .addValue(SpConstants.I_PAGE_SIZE, request.getPageSize());

        try {
            Map<String, Object> out = callAnalista.execute(in);
            dto.setLista((List) out.get(SpConstants.O_CUR_DASHBOARD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return dto;
    }

    public ListDashCarteraDTO execCartera(ListDashCarteraRequest request) {
        ListDashCarteraDTO dto = new ListDashCarteraDTO();

        in = new MapSqlParameterSource()
        	.addValue(SpConstants.I_CODUSUARIO, request.getCodAnalista())
            .addValue(SpConstants.I_COD_BASE, request.getCodBase());

        try {
            Map<String, Object> out = callCartera.execute(in);
            dto.setLista((List) out.get(SpConstants.O_CUR_DASHBOARD));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return dto;
    }
}
