package com.orbita.ws.adminws.model.dao;

import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListAnalistaDTO;
import com.orbita.ws.adminws.model.dto.UsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.OpcionXUsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioUpdateRequest;

public interface SeguridadDAO {

    ListAnalistaDTO findAll(ListAnalistaRequest request);
    UsuarioDTO findById(UsuarioRequest request);
    BaseResponse update(UsuarioUpdateRequest request);
    OpcionXUsuarioDTO listarPrivilegios(BaseRequest request);
}
