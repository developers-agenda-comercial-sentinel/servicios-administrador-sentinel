package com.orbita.ws.adminws.model.dao.sp.parametria;

import com.orbita.ws.adminws.model.beans.ReaccionBean;
import com.orbita.ws.adminws.model.dao.mapper.ReaccionMapper;
import com.orbita.ws.adminws.model.dao.mapper.array.ReaccionArray;
import com.orbita.ws.adminws.model.dto.ReacContactadoDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.InsertDTO;
import com.orbita.ws.adminws.model.dto.ListReaccionDTO;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.dto.request.ReaccionCSaveRequest;
import com.orbita.ws.adminws.model.dto.request.ReaccionCUpdateRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
public class SpContactado {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callSel;
    private SimpleJdbcCall callGet;
    private SimpleJdbcCall callIns;
    private SimpleJdbcCall callUpd;
    private SimpleJdbcCall callDel;
    private SimpleJdbcCall callSelDeta;

    // Inputs - Outputs
    SqlParameterSource in;
    Map<String, Object> out;

    @PostConstruct
    void init() {
        callSel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_REACCION)
                .declareParameters(
                        new SqlParameter(SpConstants.I_TIPO_REACCION, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_REACCION, new ReaccionMapper());

        callSelDeta = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_REACCION_DETALLE)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_REACCION_PADRE, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_REACCION, new ReaccionMapper());

        callGet = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_GET_REACCION)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_REACCION, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_REACCION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_ACTIVIDAD, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MISTI, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_ACTIVO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callIns = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_INS_REACCION_CONTACTADO)
                .declareParameters(
                        new SqlParameter(SpConstants.I_ARR_REACCION, OracleTypes.ARRAY, SpConstants.TYPE_REACCION_TAB),
                        new SqlParameter(SpConstants.I_DESC_REACCION, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ACTIVO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_MISTI, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_REGISTRO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_REACCION, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callUpd = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_UPD_REACCION_CONTACTADO)
                .declareParameters(
                        new SqlParameter(SpConstants.I_ARR_REACCION, OracleTypes.ARRAY, SpConstants.TYPE_REACCION_TAB),
                        new SqlParameter(SpConstants.I_ARR_REACCION_ELIMINAR, OracleTypes.ARRAY, SpConstants.TYPE_REACCION_TAB),
                        new SqlParameter(SpConstants.I_COD_REACCION, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_DESC_REACCION, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_ACTIVIDAD, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ACTIVO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_MISTI, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callDel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_DEL_REACCION)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_REACCION, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );
    }

    public ListReaccionDTO findAllContact(){
        ListReaccionDTO dto = new ListReaccionDTO();
        int request = 17;

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_TIPO_REACCION, request);

        try {
            out = callSel.execute(in);
            dto.setReacciones((List)out.get(SpConstants.O_CUR_REACCION));
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public ReacContactadoDTO findByIdContact(BaseRequest request){
        ReacContactadoDTO dto = new ReacContactadoDTO();
        ReacContactadoDTO.Reaccion reaccion = dto.new Reaccion();
        List<ReaccionBean> detalle;

        in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_REACCION, request.getCodigo());

        try {
            out = callGet.execute(in);
            reaccion.setReaccion((String) out.get(SpConstants.O_REACCION));
            reaccion.setCodActividad(((BigDecimal) out.get(SpConstants.O_COD_ACTIVIDAD)).intValue());
            reaccion.setActividad((String) out.get(SpConstants.O_ACTIVIDAD));
            reaccion.setMisti(((BigDecimal) out.get(SpConstants.O_MISTI)).intValue());
            reaccion.setActivo(((BigDecimal) out.get(SpConstants.O_ACTIVO)).intValue());

            in = new MapSqlParameterSource()
                    .addValue(SpConstants.I_COD_REACCION_PADRE, request.getCodigo());
            out = callSelDeta.execute(in);
            detalle = (List) out.get(SpConstants.O_CUR_REACCION);
            reaccion.setReaccionDetalle(detalle);

            dto.setReaccion(reaccion);
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return dto;
    }

    public InsertDTO saveContact(ReaccionCSaveRequest request) {
        InsertDTO dto = new InsertDTO();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_ARR_REACCION, new ReaccionArray(request.getReaccionDetalle()))
            .addValue(SpConstants.I_DESC_REACCION, request.getReaccion())
            .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad())
            .addValue(SpConstants.I_ACTIVO, request.getActivo())
            .addValue(SpConstants.I_MISTI, request.getMisti())
            .addValue(SpConstants.I_USUARIO_REGISTRO, request.getUsuarioRegistro());

        try {
            out = callIns.execute(in);
            dto.setCodigo(((BigDecimal) out.get(SpConstants.O_COD_REACCION)).intValue());
            dto.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            dto.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return  dto;
    }

    public BaseResponse updateContact(ReaccionCUpdateRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_ARR_REACCION, new ReaccionArray(request.getReaccionDetalle()))
            .addValue(SpConstants.I_ARR_REACCION_ELIMINAR, new ReaccionArray(request.getReaccionDetaEliminar()))
            .addValue(SpConstants.I_COD_REACCION, request.getCodReaccion())
            .addValue(SpConstants.I_DESC_REACCION, request.getReaccion())
            .addValue(SpConstants.I_COD_ACTIVIDAD, request.getCodActividad())
            .addValue(SpConstants.I_ACTIVO, request.getActivo())
            .addValue(SpConstants.I_MISTI, request.getMisti())
            .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callUpd.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }

    public BaseResponse deleteContact(DeleteRequest request) {
        BaseResponse response = new BaseResponse();

        in = new MapSqlParameterSource()
            .addValue(SpConstants.I_COD_REACCION, request.getCodigo())
            .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            out = callDel.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return response;
    }
}
