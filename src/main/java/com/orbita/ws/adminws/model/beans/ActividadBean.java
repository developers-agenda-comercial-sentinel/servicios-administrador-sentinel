package com.orbita.ws.adminws.model.beans;

import io.swagger.annotations.ApiModel;


@ApiModel("BEAN - Actividad")
public class ActividadBean {

    private int codActividad;
    private String actividad;
    private int codColor;
    private String color;
    private String hexadecimalColor;
    private int estado;

    public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }
    
	public int getCodColor() {
		return codColor;
	}

	public void setCodColor(int codColor) {
		this.codColor = codColor;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getHexadecimalColor() {
		return hexadecimalColor;
	}

	public void setHexadecimalColor(String hexadecimalColor) {
		this.hexadecimalColor = hexadecimalColor;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

}
