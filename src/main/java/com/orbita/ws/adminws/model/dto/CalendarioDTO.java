package com.orbita.ws.adminws.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class CalendarioDTO {

    private String fechaCita;
    private int cantidad;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    List<IndicadorCalendDTO> indicadores;

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public List<IndicadorCalendDTO> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(List<IndicadorCalendDTO> indicadores) {
        this.indicadores = indicadores;
    }
}
