package com.orbita.ws.adminws.model.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orbita.ws.adminws.model.dao.ConfiguracionDAO;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.ListConfigActividadDTO;
import com.orbita.ws.adminws.model.dto.ListConfigBaseDTO;
import com.orbita.ws.adminws.model.dto.request.ActividadUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.BaseSaveRequest;
import com.orbita.ws.adminws.model.dto.request.BaseUpdateRequest;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.services.ConfiguracionService;

@Service
public class ConfiguracionServiceImpl implements ConfiguracionService{
	
	@Autowired
	private ConfiguracionDAO configuracionDao;

	@Override
	public ListConfigActividadDTO findAllActivity() {
		return configuracionDao.findAllActivity();
	}

	@Override
	public ConfigActividadDTO findByIdActivity(BaseRequest request) {
		return configuracionDao.findByIdActivity(request);
	}

	@Override
	public BaseResponse updateActivity(ActividadUpdateRequest request) {
		return configuracionDao.updateActivity(request);
	}

	@Override
	public ListConfigBaseDTO findAllBase() {
		return configuracionDao.findAllBase();
	}

	@Override
	public ConfigBaseDTO findByIdBase(BaseRequest request) {
		return configuracionDao.findByIdBase(request);
	}

	@Override
	public BaseResponse saveBase(BaseSaveRequest request) {
		return configuracionDao.saveBase(request);
	}

	@Override
	public BaseResponse updateBase(BaseUpdateRequest request) {
		return configuracionDao.updateBase(request);
	}

	@Override
	public BaseResponse deleteBase(DeleteRequest request) {
		return configuracionDao.deleteBase(request);
	}

}
