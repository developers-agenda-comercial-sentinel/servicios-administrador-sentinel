package com.orbita.ws.adminws.model.dao.sp.seguridad;

import com.orbita.ws.adminws.model.beans.UsuarioBean;
import com.orbita.ws.adminws.model.dao.mapper.UsuarioMapper;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListAnalistaDTO;
import com.orbita.ws.adminws.model.dto.UsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioUpdateRequest;
import com.orbita.ws.adminws.util.SpConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SpUsuario {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall callSel;
    private SimpleJdbcCall callGet;
    private SimpleJdbcCall callUpd;

    @PostConstruct
    void init() {
        callSel = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_SEL_USUARIO)
                .declareParameters(
                        new SqlParameter(SpConstants.I_COD_AGENCIA, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                )
                .returningResultSet(SpConstants.O_CUR_USUARIO, new UsuarioMapper());

        callGet = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_GET_USUARIO)
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                        new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_USUARIO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_NOMBRES, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_APEPATERNO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_APEMATERNO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_TIPO_DOCUMENTO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_NRODOCUMENTO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_EMAIL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_TELEFONO, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_PERFIL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_ESTADO, OracleTypes.NUMBER),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );

        callUpd = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName(SpConstants.PK_WEB)
                .withProcedureName(SpConstants.SP_UPD_USUARIO)
                .withoutProcedureColumnMetaDataAccess()
                .declareParameters(
                        new SqlParameter(SpConstants.I_CODUSUARIO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_NOMBRES, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_APEPATERNO, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_APEMATERNO, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_TIPO_DOCUMENTO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_NRO_DOCUMENTO, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_EMAIL, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_TELEFONO, OracleTypes.VARCHAR),
                        new SqlParameter(SpConstants.I_COD_PERFIL, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_ESTADO, OracleTypes.NUMBER),
                        new SqlParameter(SpConstants.I_USUARIO_MODIFICACION, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_COD_VAL, OracleTypes.VARCHAR),
                        new SqlOutParameter(SpConstants.O_MSG, OracleTypes.VARCHAR)
                );
    }

    public ListAnalistaDTO findAll(ListAnalistaRequest request) {
        ListAnalistaDTO analistaDTO = new ListAnalistaDTO();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_COD_AGENCIA, request.getCodAgencia())
                .addValue(SpConstants.I_COD_PERFIL, request.getCodPerfil());

        try {
            Map<String, Object> out = callSel.execute(in);
            analistaDTO.setUsuarios((List)out.get(SpConstants.O_CUR_USUARIO));
            analistaDTO.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            analistaDTO.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return analistaDTO;
    }

    public UsuarioDTO findById(UsuarioRequest request) {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        UsuarioBean usuarioBean = new UsuarioBean();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_CODUSUARIO, request.getCodUsuario());

        try {
            Map<String, Object> out = callGet.execute(in);
            usuarioBean.setCodUsuario(((BigDecimal) out.get(SpConstants.O_COD_USUARIO)).intValue());
            usuarioBean.setNombre((String) out.get(SpConstants.O_NOMBRES));
            usuarioBean.setApePaterno((String) out.get(SpConstants.O_APEPATERNO));
            usuarioBean.setApeMaterno((String) out.get(SpConstants.O_APEMATERNO));
            usuarioBean.setTipoDocumento(((BigDecimal) out.get(SpConstants.O_COD_TIPO_DOCUMENTO)).intValue());
            usuarioBean.setNroDocumento((String) out.get(SpConstants.O_NRODOCUMENTO));
            usuarioBean.setEmail((String) out.get(SpConstants.O_EMAIL));
            usuarioBean.setTelefono((String) out.get(SpConstants.O_TELEFONO));
            usuarioBean.setCodPerfil(((BigDecimal) out.get(SpConstants.O_COD_PERFIL)).intValue());
            usuarioBean.setPerfil((String) out.get(SpConstants.O_PERFIL));
            usuarioBean.setEstado(((BigDecimal) out.get(SpConstants.O_ESTADO)).intValue());

            usuarioDTO.setUsuario(usuarioBean);
            usuarioDTO.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            usuarioDTO.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return usuarioDTO;
    }

    public BaseResponse update(UsuarioUpdateRequest request) {
        BaseResponse response = new BaseResponse();

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue(SpConstants.I_CODUSUARIO, request.getCodUsuario())
                .addValue(SpConstants.I_NOMBRES, request.getNombres())
                .addValue(SpConstants.I_APEPATERNO, request.getApePaterno())
                .addValue(SpConstants.I_APEMATERNO, request.getApeMaterno())
                .addValue(SpConstants.I_COD_TIPO_DOCUMENTO, request.getTipoDocumento())
                .addValue(SpConstants.I_NRO_DOCUMENTO, request.getNroDocumento())
                .addValue(SpConstants.I_EMAIL, request.getEmail())
                .addValue(SpConstants.I_TELEFONO, request.getTelefono())
                .addValue(SpConstants.I_COD_PERFIL, request.getCodPerfil())
                .addValue(SpConstants.I_ESTADO, request.getEstado())
                .addValue(SpConstants.I_USUARIO_MODIFICACION, request.getUsuarioModificacion());

        try {
            Map<String, Object> out = callUpd.execute(in);
            response.setCodVal((String) out.get(SpConstants.O_COD_VAL));
            response.setMensaje((String) out.get(SpConstants.O_MSG));

        } catch (Exception e){
            System.err.println(e.getMessage());
        }

        return response;
    }

}
