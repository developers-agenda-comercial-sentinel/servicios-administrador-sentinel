package com.orbita.ws.adminws.model.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.orbita.ws.adminws.util.SpConstants;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class OpcionRequest implements SQLData {

    private int codOpcion;
    private int estado;

    public int getCodOpcion() {
        return codOpcion;
    }

    public void setCodOpcion(int codOpcion) {
        this.codOpcion = codOpcion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @JsonIgnore
    @Override
    public String getSQLTypeName() throws SQLException {
        return SpConstants.TYPE_OPCION;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        codOpcion = stream.readInt();
        estado = stream.readInt();
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(codOpcion);
        stream.writeInt(estado);
    }
}
