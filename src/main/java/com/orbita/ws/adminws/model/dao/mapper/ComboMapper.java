package com.orbita.ws.adminws.model.dao.mapper;

import com.orbita.ws.adminws.model.beans.ComboBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ComboMapper implements RowMapper<ComboBean> {
    @Override
    public ComboBean mapRow(ResultSet rs, int i) throws SQLException {
        ComboBean bean = new ComboBean();
        bean.setCodigo(rs.getInt("CODIGO"));
        bean.setDescripcion(rs.getString("DESCRIPCION"));

        return bean;
    }
}
