package com.orbita.ws.adminws.model.beans;

import com.google.gson.annotations.SerializedName;

public class ErrorBean {

	@SerializedName("ErrorDes")
	private String errorDes;

	public String getErrorDes() {
		return errorDes;
	}

	public void setErrorDes(String errorDes) {
		this.errorDes = errorDes;
	}

}
