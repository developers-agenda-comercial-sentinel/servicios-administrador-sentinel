package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Actualizar Usuario")
public class UsuarioUpdateRequest extends UpdRequest {

    @ApiModelProperty(value = "Código del usuario", required = true)
    private int codUsuario;
    @ApiModelProperty(value = "Nombres", required = true)
    private String nombres;
    @ApiModelProperty(value = "Apellido Paterno", required = true)
    private String apePaterno;
    @ApiModelProperty(value = "Apellido materno", required = true)
    private String apeMaterno;
    @ApiModelProperty(value = "Tipo de documento", required = true)
    private int tipoDocumento;
    @ApiModelProperty(value = "Número de documento", required = true)
    private String nroDocumento;
    @ApiModelProperty(value = "Email", required = true)
    private String email;
    @ApiModelProperty(value = "Teléfono", required = true)
    private String telefono;
    @ApiModelProperty(value = "Código de perfil", required = true)
    private int codPerfil;
    @ApiModelProperty(value = "Estado", required = true)
    private int estado;

    public int getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApePaterno() {
        return apePaterno;
    }

    public void setApePaterno(String apePaterno) {
        this.apePaterno = apePaterno;
    }

    public String getApeMaterno() {
        return apeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {
        this.apeMaterno = apeMaterno;
    }

    public int getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(int tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getCodPerfil() {
        return codPerfil;
    }

    public void setCodPerfil(int codPerfil) {
        this.codPerfil = codPerfil;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
