package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.SeguridadDAO;
import com.orbita.ws.adminws.model.dao.sp.seguridad.SpPrivilegios;
import com.orbita.ws.adminws.model.dao.sp.seguridad.SpUsuario;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListAnalistaDTO;
import com.orbita.ws.adminws.model.dto.UsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.OpcionXUsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeguridadDAOImpl implements SeguridadDAO {

    @Autowired
    private SpUsuario spUsuario;

    @Autowired
    private SpPrivilegios spPrivilegios;

    @Override
    public ListAnalistaDTO findAll(ListAnalistaRequest request) {
        return spUsuario.findAll(request);
    }

    @Override
    public UsuarioDTO findById(UsuarioRequest request) {
        return spUsuario.findById(request);
    }

    @Override
    public BaseResponse update(UsuarioUpdateRequest request) {
        return spUsuario.update(request);
    }

    @Override
    public OpcionXUsuarioDTO listarPrivilegios(BaseRequest request) {
        return spPrivilegios.execute(request);
    }
}
