package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

import com.orbita.ws.adminws.model.dto.request.arrayRequest.ArrayCargaInactivoReq;

public class CargaInactivoDTO extends BaseResponse implements Serializable {
	
	private List<ArrayCargaInactivoReq> cargaInactivo;

	public List<ArrayCargaInactivoReq> getCargaInactivo() {
		return cargaInactivo;
	}

	public void setCargaInactivo(List<ArrayCargaInactivoReq> cargaInactivo) {
		this.cargaInactivo = cargaInactivo;
	}

}
