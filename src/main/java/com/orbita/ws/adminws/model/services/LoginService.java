package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.LoginDTO;
import com.orbita.ws.adminws.model.dto.request.LoginRequest;

public interface LoginService {

    LoginDTO getUsuario(LoginRequest usuario);
}
