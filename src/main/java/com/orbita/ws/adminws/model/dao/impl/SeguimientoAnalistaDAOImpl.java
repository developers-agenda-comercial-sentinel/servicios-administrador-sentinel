package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.SeguimientoAnalistaDAO;
import com.orbita.ws.adminws.model.dao.sp.analista.SpSeguimientoAnalista;
import com.orbita.ws.adminws.model.dto.ListAnalistaUbicacionDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeguimientoAnalistaDAOImpl implements SeguimientoAnalistaDAO {

    @Autowired
    private SpSeguimientoAnalista spSeguimientoAnalista;

    @Override
    public ListAnalistaUbicacionDTO obtenerUbicacionAnalista(AnalistaSentinelRequest request) {
        return spSeguimientoAnalista.execute(request);
    }
}
