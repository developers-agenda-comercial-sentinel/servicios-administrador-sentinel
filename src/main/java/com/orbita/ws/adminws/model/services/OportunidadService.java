package com.orbita.ws.adminws.model.services;

import com.orbita.ws.adminws.model.dto.ListOportunidadDTO;
import com.orbita.ws.adminws.model.dto.request.OportunidadRequest;

public interface OportunidadService {

    ListOportunidadDTO listarOportunidades(OportunidadRequest request);
}
