package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Listar Cartera Agendar")
public class CarteraAgendaRequest {

    @ApiModelProperty(value = "Código de analista", required = true)
    private int codAnalista;
    @ApiModelProperty(value = "Código de actividad", required = true)
    private int codActividad;
    @ApiModelProperty(value = "Código de base", required = true)
    private int codBase;
    @ApiModelProperty(value = "Ubigeo", required = true)
    private String ubigeo;


	public int getCodAnalista() {
		return codAnalista;
	}

	public void setCodAnalista(int codAnalista) {
		this.codAnalista = codAnalista;
	}

	public int getCodActividad() {
        return codActividad;
    }

    public void setCodActividad(int codActividad) {
        this.codActividad = codActividad;
    }

    public int getCodBase() {
        return codBase;
    }

    public void setCodBase(int codBase) {
        this.codBase = codBase;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }
}
