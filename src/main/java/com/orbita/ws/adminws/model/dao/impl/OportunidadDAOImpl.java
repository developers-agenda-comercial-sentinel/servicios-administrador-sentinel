package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.OportunidadDAO;
import com.orbita.ws.adminws.model.dao.sp.cartera.SpOportunidad;
import com.orbita.ws.adminws.model.dto.ListOportunidadDTO;
import com.orbita.ws.adminws.model.dto.request.OportunidadRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OportunidadDAOImpl implements OportunidadDAO {

    @Autowired
    private SpOportunidad spOportunidad;

    @Override
    public ListOportunidadDTO listarOportunidades(OportunidadRequest request) {
        return spOportunidad.execute(request);
    }
}
