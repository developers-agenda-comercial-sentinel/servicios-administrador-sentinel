package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.PerfilDAO;
import com.orbita.ws.adminws.model.dao.sp.seguridad.SpPerfil;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.InsertDTO;
import com.orbita.ws.adminws.model.dto.ListPerfilDTO;
import com.orbita.ws.adminws.model.dto.PerfilDTO;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilSaveRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerfilDAOImpl implements PerfilDAO {

    @Autowired
    private SpPerfil spPerfil;

    @Override
    public ListPerfilDTO findAll() {
        return spPerfil.findAll();
    }

    @Override
    public PerfilDTO findById(PerfilRequest request) {
        return spPerfil.findById(request);
    }

    @Override
    public InsertDTO save(PerfilSaveRequest request) {
        return spPerfil.save(request);
    }

    @Override
    public BaseResponse update(PerfilUpdateRequest request) {
        return spPerfil.update(request);
    }

    @Override
    public BaseResponse delete(DeleteRequest request) {
        return spPerfil.delete(request);
    }
}
