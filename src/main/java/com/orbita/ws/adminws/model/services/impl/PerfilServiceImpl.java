package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.PerfilDAO;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.InsertDTO;
import com.orbita.ws.adminws.model.dto.ListPerfilDTO;
import com.orbita.ws.adminws.model.dto.PerfilDTO;
import com.orbita.ws.adminws.model.dto.request.DeleteRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilSaveRequest;
import com.orbita.ws.adminws.model.dto.request.PerfilUpdateRequest;
import com.orbita.ws.adminws.model.services.PerfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PerfilServiceImpl implements PerfilService {

    @Autowired
    private PerfilDAO perfilDAO;

    @Override
    public ListPerfilDTO findAll() {
        return perfilDAO.findAll();
    }

    @Override
    public PerfilDTO findById(PerfilRequest request) {
        return perfilDAO.findById(request);
    }

    @Override
    public InsertDTO save(PerfilSaveRequest request) {
        return perfilDAO.save(request);
    }

    @Override
    public BaseResponse update(PerfilUpdateRequest request) {
        return perfilDAO.update(request);
    }

    @Override
    public BaseResponse delete(DeleteRequest request) {
        return perfilDAO.delete(request);
    }
}
