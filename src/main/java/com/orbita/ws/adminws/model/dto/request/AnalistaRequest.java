package com.orbita.ws.adminws.model.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("REQUEST - Analista")
public class AnalistaRequest {

    @ApiModelProperty(value = "Valor a buscar", allowableValues="Nombres, Apellidos" ,required = true)
    private String busqueda;
    @ApiModelProperty(value = "Código de agencia", required = true)
    private int codAgencia;

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public int getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(int codAgencia) {
        this.codAgencia = codAgencia;
    }
}
