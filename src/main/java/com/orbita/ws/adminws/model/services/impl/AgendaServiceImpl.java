package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.AgendaDAO;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListCalendarioDTO;
import com.orbita.ws.adminws.model.dto.ListCartAgendaDTO;
import com.orbita.ws.adminws.model.dto.ListRegAgendaCboDTO;
import com.orbita.ws.adminws.model.dto.request.AnalistaSentinelRequest;
import com.orbita.ws.adminws.model.dto.request.CalendarioRequest;
import com.orbita.ws.adminws.model.dto.request.CarteraAgendaRequest;
import com.orbita.ws.adminws.model.dto.request.RegCitaRequest;
import com.orbita.ws.adminws.model.services.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgendaServiceImpl implements AgendaService {

    @Autowired
    private AgendaDAO agendaDAO;

    @Override
    public ListCalendarioDTO execCalendario(CalendarioRequest request) {
        return agendaDAO.execCalendario(request);
    }

    @Override
    public ListRegAgendaCboDTO execute(AnalistaSentinelRequest request) {
        return agendaDAO.execute(request);
    }

    @Override
    public ListCartAgendaDTO listarCartera(CarteraAgendaRequest request) {
        return agendaDAO.listarCartera(request);
    }

    @Override
    public BaseResponse registrarAgenda(RegCitaRequest request) {
        return agendaDAO.registrarAgenda(request);
    }
}
