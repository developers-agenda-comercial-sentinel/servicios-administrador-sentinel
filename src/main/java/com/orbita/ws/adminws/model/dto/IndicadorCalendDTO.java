package com.orbita.ws.adminws.model.dto;

public class IndicadorCalendDTO {

    private int codColor;
    private int cantidad;

    public int getCodColor() {
        return codColor;
    }

    public void setCodColor(int codColor) {
        this.codColor = codColor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
