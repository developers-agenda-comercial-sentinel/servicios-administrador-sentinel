package com.orbita.ws.adminws.model.dao.impl;

import com.orbita.ws.adminws.model.dao.HomeDAO;
import com.orbita.ws.adminws.model.dao.sp.home.SpDashAnalista;
import com.orbita.ws.adminws.model.dao.sp.home.SpDashCartera;
import com.orbita.ws.adminws.model.dao.sp.home.SpDashTop;
import com.orbita.ws.adminws.model.dao.sp.home.SpDashUbiAnalista;
import com.orbita.ws.adminws.model.dto.*;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaBaseRequest;
import com.orbita.ws.adminws.model.dto.request.ListDashCarteraRequest;
import com.orbita.ws.adminws.model.dto.request.ListHomeAnalista;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeDAOImpl implements HomeDAO {

    @Autowired
    private SpDashCartera spDashCartera;

    @Autowired
    private SpDashAnalista spDashAnalista;

    @Autowired
    private SpDashTop spDashTop;

    @Autowired
    private SpDashUbiAnalista spDashUbiAnalista;

    @Override
    public ListDashActividadDTO execActividad() {
        return spDashCartera.execActividad();
    }

    @Override
    public ListBaseDTO execBase(BaseRequest request) {
        return spDashCartera.execBase(request);
    }

    @Override
    public ListDashAnalistaBaseDTO execAnalistaBase(ListAnalistaBaseRequest request) {
        return spDashCartera.execAnalistaBase(request);
    }

    @Override
    public ListDashCarteraDTO execCartera(ListDashCarteraRequest request) {
        return spDashCartera.execCartera(request);
    }

    @Override
    public ListDashAnalistaDTO execAnalista(ListHomeAnalista request) {
        return spDashAnalista.execAnalista(request);
    }

    @Override
    public ListDashTopVisitaDTO execTopVisita() {
        return spDashTop.execTopVisita();
    }

    @Override
    public ListDashTopEfectivaDTO execTopEfectiva() {
        return spDashTop.execTopEfectiva();
    }

    @Override
    public ListAnalistaUbicacionDTO execUbiAnalista() {
        return spDashUbiAnalista.execUbiAnalista();
    }
}
