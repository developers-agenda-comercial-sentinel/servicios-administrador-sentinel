package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListCalendarioDTO extends BaseResponse implements Serializable {

    private List<CalendarioDTO> calendario;

    public List<CalendarioDTO> getCalendario() {
        return calendario;
    }

    public void setCalendario(List<CalendarioDTO> calendario) {
        this.calendario = calendario;
    }
}
