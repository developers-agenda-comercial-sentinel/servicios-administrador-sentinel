package com.orbita.ws.adminws.model.dto;

import java.io.Serializable;
import java.util.List;

public class ListDashAnalistaBaseDTO extends BaseResponse implements Serializable {

    private List<DashAnalistaBaseDTO> lista;

    public List<DashAnalistaBaseDTO> getLista() {
        return lista;
    }

    public void setLista(List<DashAnalistaBaseDTO> lista) {
        this.lista = lista;
    }
}
