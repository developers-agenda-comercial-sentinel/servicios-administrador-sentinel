package com.orbita.ws.adminws.model.services.impl;

import com.orbita.ws.adminws.model.dao.SeguridadDAO;
import com.orbita.ws.adminws.model.dto.BaseResponse;
import com.orbita.ws.adminws.model.dto.ListAnalistaDTO;
import com.orbita.ws.adminws.model.dto.UsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.BaseRequest;
import com.orbita.ws.adminws.model.dto.OpcionXUsuarioDTO;
import com.orbita.ws.adminws.model.dto.request.ListAnalistaRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioRequest;
import com.orbita.ws.adminws.model.dto.request.UsuarioUpdateRequest;
import com.orbita.ws.adminws.model.services.SeguridadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeguridadServiceImpl implements SeguridadService {

    @Autowired
    private SeguridadDAO seguridadDAO;

    @Override
    public ListAnalistaDTO findAll(ListAnalistaRequest request) {
        return seguridadDAO.findAll(request);
    }

    @Override
    public UsuarioDTO findById(UsuarioRequest request) {
        return seguridadDAO.findById(request);
    }

    @Override
    public BaseResponse update(UsuarioUpdateRequest request) {
        return seguridadDAO.update(request);
    }

    @Override
    public OpcionXUsuarioDTO listarPrivilegios(BaseRequest request) {
        return seguridadDAO.listarPrivilegios(request);
    }
}
