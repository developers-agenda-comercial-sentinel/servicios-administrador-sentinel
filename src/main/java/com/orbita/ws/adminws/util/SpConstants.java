package com.orbita.ws.adminws.util;

public class SpConstants {
    public static final String PK_WEB = "PK_WEB";
    public static final String PK_WEB_LOAD = "PK_WEB_CARGA";

    // Procedimientos Almacenados
    // SEGURIDAD
    public static final String SP_LOGIN = "SP_VALIDAR_USUARIO";
    public static final String SP_SEL_OPCIONXUSUARIO = "SP_LISTAR_OPCION_WEB";
    public static final String SP_SEL_OPCION = "SP_LISTAR_OPCION";
    // SEG - Usuarios
    public static final String SP_SEL_USUARIO = "SP_LISTAR_USUARIO";
    public static final String SP_GET_USUARIO = "SP_OBTENER_USUARIO";
    public static final String SP_UPD_USUARIO = "SP_ACTUALIZAR_USUARIO";
    // SEG - Perfiles
    public static final String SP_SEL_PERFIL = "SP_LISTAR_PERFIL";
    public static final String SP_GET_PERFIL = "SP_OBTENER_PERFIL";
    public static final String SP_INS_PERFIL = "SP_GUARDAR_PERFIL";
    public static final String SP_UPD_PERFIL = "SP_ACTUALIZAR_PERFIL";
    public static final String SP_DEL_PERFIL = "SP_ELIMINAR_PERFIL";

    // HOME
    public static final String SP_SEL_DASH_ACTIVIDAD = "SP_LISTAR_DASH_ACTIVIDAD";
    public static final String SP_SEL_DASH_ANALISTAXBASE = "SP_LISTAR_DASH_ANALISTAXBASE";
    public static final String SP_SEL_DASH_CARTXANALISTA = "SP_LISTAR_DASH_CARTXANALISTA";
    public static final String SP_SEL_DASH_CITAS = "SP_LISTAR_DASH_CITAS";
    public static final String SP_SEL_DASH_EFECTIVAS = "SP_LISTAR_DASH_EFECTIVAS";
    public static final String SP_SEL_DASH_ANALISTA = "SP_LISTAR_DASH_ANALISTA";
    public static final String SP_SEL_DASH_SEGUIMIENTO = "SP_LISTAR_DASH_SEGUIMIENTO";

    // CARTERA
    public static final String SP_SEL_AGENCIAS = "SP_LISTAR_AGENCIA";
    public static final String SP_SEL_ANALISTAXAGENCIA = "SP_LISTAR_ANALISTAXAGENCIA";
    public static final String SP_SEL_ACTIVIDAD = "SP_LISTAR_ACTIVIDAD";
    public static final String SP_SEL_BASE = "SP_LISTAR_BASE";
    public static final String SP_SEL_CARTERA = "SP_LISTAR_CARTERA";
    public static final String SP_UPD_TRANSFERIR_CLIENTES = "SP_ACTUALIZAR_ANALISTA_CLIENTE";
    public static final String SP_SEL_OPORTUNIDADES = "SP_LISTAR_OPORTUNIDAD";
    public static final String SP_INS_ASIGNAR_OPORTUNIDAD = "SP_GUARDAR_GESTION_OPORTUNIDAD";
    public static final String SP_INS_ASIGNAR_CARTERA = "SP_GUARDAR_GESTION_CLIENTE";

    // ANALISTA
    public static final String SP_SEL_ANALISTAS = "SP_LISTAR_ANALISTA";
    public static final String SP_GET_UBICACION_ANALISTA = "SP_LISTAR_SEGUIMIENTO";

    // AGENDA
    public static final String SP_SEL_CITAS_CALENDARIO = "SP_LISTAR_CANT_CITAS_CALEND";
    public static final String SP_SEL_IND_ACT_CALENDARIO = "SP_LISTAR_INDICADOR_ACTIVIDAD";
    public static final String SP_SEL_DISTRITOS_CARTERA = "SP_LISTAR_CANTIDAD_BASES";
    public static final String SP_SEL_ACTIVIDAD_CARTERA = "SP_LISTAR_ACTIVIDAD_FILTRO";
    public static final String SP_SEL_CARTERA_AGENDA = "SP_LISTAR_CARTERA_AGENDA";
    public static final String SP_INS_AGENDAR_CITA = "SP_GUARDAR_CITA";

    // REPORTES
    public static final String SP_SEL_REPORTE = "SP_LISTAR_REPORTE";
    public static final String SP_SEL_REPORTE_VISITAS = "SP_LISTAR_REP_VISITAS";

    //COMMON
    public static final String SP_SEL_RUTAS_RECURSOS = "SP_LISTAR_RUTAS_RECURSOS";
    public static final String SP_SEL_COLOR = "SP_LISTAR_COLOR";
    
    // PARAMETRÍA
    // PARAM - Contactado / No Contactado
    public static final String SP_SEL_REACCION = "SP_LISTAR_REACCION";
    public static final String SP_SEL_REACCION_DETALLE = "SP_LISTAR_REACCION_DETALLE";
    public static final String SP_GET_REACCION = "SP_OBTENER_REACCION";
    public static final String SP_INS_REACCION_CONTACTADO = "SP_GUARDAR_REAC_CONTACTADO";
    public static final String SP_UPD_REACCION_CONTACTADO = "SP_ACTUALIZAR_R_CONTACTADO";
    public static final String SP_INS_REACCION_NOCONTACTADO = "SP_GUARDAR_REAC_NOCONTACTADO";
    public static final String SP_UPD_REACCION_NOCONTACTADO = "SP_ACTUALIZAR_R_NOCONTACTADO";
    public static final String SP_DEL_REACCION = "SP_ELIMINAR_REACCION";
    
    // CONFIGURACIÓN
    public static final String SP_SEL_CONFIG_ACTIVIDAD = "SP_LISTAR_ACTIVIDAD";
    public static final String SP_GET_CONFIG_ACTIVIDAD = "SP_OBTENER_ACTIVIDAD";
    public static final String SP_UPD_CONFIG_ACTIVIDAD = "SP_ACTUALIZAR_ACTIVIDAD";
    public static final String SP_SEL_CONFIG_BASE = "SP_LISTAR_BASE";
    public static final String SP_INS_CONFIG_BASE = "SP_GUARDAR_BASE";
    public static final String SP_GET_CONFIG_BASE = "SP_OBTENER_BASE";
    public static final String SP_UPD_CONFIG_BASE = "SP_ACTUALIZAR_BASE";
    public static final String SP_DEL_CONFIG_BASE = "SP_ELIMINAR_BASE";

    // CARGA MASIVA
    public static final String SP_LOAD_RECURRENTE = "SP_LOAD_RECURRENTE";
    public static final String SP_LOAD_COBRANZA = "SP_LOAD_COBRANZA";
    public static final String SP_LOAD_INACTIVO = "SP_LOAD_INACTIVO";
    public static final String SP_LOAD_REFERIDO = "SP_LOAD_REFERIDO";
    public static final String SP_SEL_CARGA_HISTORICA = "SP_LISTAR_CARGA_HISTORICA";
    public static final String SP_SEL_CARGA_ERRADA = "SP_LISTAR_CARGA_ERRADA";
    
    // INPUTS - OUTPUTS
    // INPUTS
    // Inputs Base
    public static final String I_COD_USUARIO_REGISTRO = "ID_USUARIO_REGISTRO_IN";
    public static final String I_USUARIO_REGISTRO = "USUARIO_REGISTRO_IN";
    public static final String I_USUARIO_MODIFICACION = "USUARIO_MODIFICACION_IN";
    public static final String I_BUSQUEDA = "BUSQUEDA_IN";
    // Códigos
    public static final String I_CODUSUARIO = "ID_USUARIO_IN";
    public static final String I_COD_PERFIL = "ID_PERFIL_IN";
    public static final String I_COD_AGENCIA = "ID_AGENCIA_IN";
    public static final String I_COD_EJECUTIVO = "ID_EJECUTIVO_IN";
    public static final String I_COD_ACTIVIDAD = "ID_ACTIVIDAD_IN";
    public static final String I_COD_REACCION = "ID_REACCION_IN";
    public static final String I_COD_REACCION_PADRE = "ID_REACCION_PADRE_IN";
    public static final String I_COD_TIPO_DOCUMENTO = "ID_TIPO_DOCUMENTO_IN";
    public static final String I_COD_BASE = "ID_BASE_IN";
    public static final String I_COD_UBIGEO = "UBIGEO_IN";
    public static final String I_COD_COLOR = "ID_COLOR_IN";
    public static final String I_CODIGO = "CODIGO_IN";
    public static final String I_CODIGO_ACTIVIDAD = "CODIGO_ACTIVIDAD_IN";
    public static final String I_CODIGO_CARGA = "ID_CARGA_IN";
    // Fields
    public static final String I_TIPO = "I_TIPO";
    public static final String I_USUARIO = "USUARIO_IN";
    public static final String I_DESC_PERFIL = "DESCRIPCION_IN";
    public static final String I_EST_PERFIL = "ESTADO_IN";
    public static final String I_DESC_REACCION = "REACCION_IN";
    public static final String I_ACTIVO = "ACTIVO_IN";
    public static final String I_MISTI = "MISTI_IN";
    public static final String I_PAGE_NUMBER = "PAGE_NUMBER_IN";
    public static final String I_PAGE_SIZE = "PAGE_SIZE_IN";
    public static final String I_NOMBRES = "NOMBRES_IN";
    public static final String I_APEPATERNO = "APELLIDO_PATERNO_IN";
    public static final String I_APEMATERNO = "APELLIDO_MATERNO_IN";
    public static final String I_NRO_DOCUMENTO = "NRO_DOCUMENTO_IN";
    public static final String I_EMAIL = "EMAIL_IN";
    public static final String I_TELEFONO = "TELEFONO_IN";
    public static final String I_ESTADO = "ESTADO_IN";
    public static final String I_CANT = "CANT_IN";
    public static final String I_FECHA = "FECHA_IN";
    public static final String I_MES = "MES_IN";
    public static final String I_ANNIO = "ANNIO_IN";
    public static final String I_NOM_REPORTE = "NOM_REPORTE_IN";
    public static final String I_ID_GROUP = "ID_GROUP_IN";
    public static final String I_TIPO_REACCION = "TIPO_REACCION_IN";
    public static final String I_ACTIVIDAD = "ACTIVIDAD_IN";
    public static final String I_HEXADECIMAL_COLOR = "HEXADECIMAL_COLOR_IN";
    public static final String I_COLOR = "COLOR_IN";
    public static final String I_DESCRIPCION = "DESCRIPCION_IN";
    public static final String I_NOM_ARCHIVO = "NOM_ARCHIVO_IN";

    // Arrays
    public static final String I_ARR_BASE = "ARR_BASE_IN";
    public static final String I_ARR_ACTIVIDAD = "ARR_ACTIVIDAD_IN";
    public static final String I_ARR_GESTION = "ARR_GESTION_IN";
    public static final String I_ARR_OPORTUNIDAD = "ARR_OPORTUNIDAD_IN";
    public static final String I_ARR_OPCION = "ARR_OPCION_IN";
    public static final String I_ARR_OPCION_ELIMINAR = "ARR_OPCION_ELIMINAR_IN";
    public static final String I_ARR_CITA = "ARR_CITA_IN";
    public static final String I_ARR_CARTERA = "ARR_CARTERA_IN";
    public static final String I_ARR_REACCION = "ARR_REACCION_DETA_IN";
    public static final String I_ARR_REACCION_ELIMINAR = "ARR_REACCION_DETA_ELIMINAR_IN";
    public static final String I_ARR_LOAD_RECURRENTE = "ARR_LOAD_RECURRENTE_IN";
    public static final String I_ARR_LOAD_COBRANZA = "ARR_LOAD_COBRANZA_IN";
    public static final String I_ARR_LOAD_INACTIVO = "ARR_LOAD_INACTIVO_IN";
    public static final String I_ARR_LOAD_REFERIDO = "ARR_LOAD_REFERIDO_IN";
    // Types
    public static final String TYPE_BASE = "TYPE_BASE";
    public static final String TYPE_ACTIVIDAD = "TYPE_ACTIVIDAD";
    public static final String TYPE_GESTION = "TYPE_GESTION";
    public static final String TYPE_OPORTUNIDAD = "TYPE_OPORTUNIDAD";
    public static final String TYPE_OPCION = "TYPE_OPCION";
    public static final String TYPE_CITA = "TYPE_CITA";
    public static final String TYPE_CARTERA = "TYPE_CARTERA";
    public static final String TYPE_REACCION = "TBL_REACCION_TYPE";
    public static final String TYPE_LOAD_RECURRENTE = "TYPE_LOAD_RECURRENTE";
    public static final String TYPE_LOAD_COBRANZA = "TYPE_LOAD_COBRANZA";
    public static final String TYPE_LOAD_INACTIVO = "TYPE_LOAD_INACTIVO";
    public static final String TYPE_LOAD_REFERIDO = "TYPE_LOAD_REFERIDO";
    // Type Table
    public static final String TYPE_BASE_TAB = "TYPE_BASE_TAB";
    public static final String TYPE_ACTIVIDAD_TAB = "TYPE_ACTIVIDAD_TAB";
    public static final String TYPE_GESTION_TAB = "TYPE_GESTION_TAB";
    public static final String TYPE_OPORTUNIDAD_TAB = "TYPE_OPORTUNIDAD_TAB";
    public static final String TYPE_OPCION_TAB = "TYPE_OPCION_TAB";
    public static final String TYPE_CITA_TAB = "TYPE_CITA_TAB";
    public static final String TYPE_CARTERA_TAB = "TYPE_CARTERA_TAB";
    public static final String TYPE_REACCION_TAB = "TBL_REACCION_TYPE_TAB";
    public static final String TYPE_LOAD_RECURRENTE_TAB = "TYPE_LOAD_RECURRENTE_TAB";
    public static final String TYPE_LOAD_COBRANZA_TAB = "TYPE_LOAD_COBRANZA_TAB";
    public static final String TYPE_LOAD_INACTIVO_TAB = "TYPE_LOAD_INACTIVO_TAB";
    public static final String TYPE_LOAD_REFERIDO_TAB = "TYPE_LOAD_REFERIDO_TAB";

    // OUTPUTS
    // Outputs Base
    public static final String O_COD_VAL = "CODIGO_VALIDACION_OUT";
    public static final String O_MSG = "MENSAJE_OUT";
    public static final String O_COD_MSG = "COD_MENSAJE_OUT";
    // Códigos
    public static final String O_COD_USUARIO = "ID_USUARIO_OUT";
    public static final String O_COD_PERFIL = "COD_PERFIL_OUT";
    public static final String O_COD_REACCION = "COD_REACCION_OUT";
    public static final String O_COD_TIPO_DOCUMENTO = "COD_TIPO_DOCUMENTO_OUT";
    public static final String O_COD_ACTIVIDAD = "ID_ACTIVIDAD_OUT";
    public static final String O_COD_COLOR = "ID_COLOR_OUT";
    // Fields
    public static final String O_NOMBRES = "NOMBRES_OUT";
    public static final String O_APEPATERNO = "APELLIDO_PATERNO_OUT";
    public static final String O_APEMATERNO = "APELLIDO_MATERNO_OUT";
    public static final String O_NRODOCUMENTO = "NRO_DOCUMENTO_OUT";
    public static final String O_EMAIL = "EMAIL_OUT";
    public static final String O_TELEFONO = "TELEFONO_OUT";
    public static final String O_PERFIL = "PERFIL_OUT";
    public static final String O_ESTADO = "ESTADO_OUT";
    public static final String O_EFECTIVAS = "PORC_EFECTIVAS_OUT";
    public static final String O_PENDIENTES = "PORC_PENDIENTES_OUT";
    public static final String O_REACCION = "REACCION_OUT";
    public static final String O_ACTIVIDAD = "ACTIVIDAD_OUT";
    public static final String O_MISTI = "MISTI_OUT";
    public static final String O_ACTIVO = "ACTIVO_OUT";
    public static final String O_HEXADECIMAL_COLOR = "HEXADECIMAL_COLOR_OUT";
    public static final String O_HEXADECIMAL = "HEXADECIMAL_OUT";
    public static final String O_COLOR = "COLOR_OUT";
    public static final String O_BASE = "BASE_OUT";
    public static final String O_REC_PATH = "PATH_OUT";
    
    // Outputs Cursor
    public static final String O_CUR_USUARIO = "CURSOR_USUARIO_OUT";
    public static final String O_CUR_PERFIL = "CURSOR_PERFIL_OUT";
    public static final String O_CUR_AGENCIA = "CURSOR_AGENCIA_OUT";
    public static final String O_CUR_ANALISTA = "CURSOR_ANALISTA_OUT";
    public static final String O_CUR_CARTERA = "CURSOR_CARTERA_OUT";
    public static final String O_CUR_ACTIVIDAD = "CURSOR_ACTIVIDAD_OUT";
    public static final String O_CUR_BASE = "CURSOR_BASE_OUT";
    public static final String O_CUR_OPORTUNIDAD = "CURSOR_OPORTUNIDAD_OUT";
    public static final String O_CUR_REACCION = "CURSOR_REACCION_OUT";
    public static final String O_CUR_OPCION = "CURSOR_OPCION_OUT";
    public static final String O_CUR_SEGUIMIENTO_ANALISTA = "CURSOR_SEG_ANALISTA_OUT";
    public static final String O_CUR_SEGUIMIENTO = "CURSOR_SEGUIMIENTO_OUT";
    public static final String O_CUR_DASHBOARD = "CURSOR_DASHBOARD_OUT";
    public static final String O_CUR_CITAS_CALENDARIO = "CURSOR_CITAS_PROGRAMADAS_OUT";
    public static final String O_CUR_INDICADOR_CALENDARIO = "CURSOR_INDICADOR_ACTIVIDAD_OUT";
    public static final String O_CUR_CARTERA_DISTRITO = "CURSOR_BASES_DISTRITO_OUT";
    public static final String O_CUR_CARTERA_ACTIVIDAD = "CURSOR_ACT_FILTRO_OUT";
    public static final String O_CUR_REPORTE = "CURSOR_REPORTE_OUT";
    public static final String O_CUR_CARGA = "CURSOR_CARGA_OUT";
    public static final String O_CUR_COLOR = "CURSOR_COLOR_OUT";

}
