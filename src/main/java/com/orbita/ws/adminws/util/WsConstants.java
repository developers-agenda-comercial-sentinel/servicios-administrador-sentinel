package com.orbita.ws.adminws.util;

public class WsConstants {

    //SEGURIDAD
    public static final String WS_VALIDAR_USUARIO = "/WsValidarUsuario";
    public static final String WS_LISTAR_USUARIO = "/WsListarUsuario";
    public static final String WS_OBTENER_USUARIO = "/WsObtenerUsuario";
    public static final String WS_ACTUALIZAR_USUARIO = "/WsActualizarUsuario";
    public static final String WS_LISTAR_PERFILES = "/WsListarPerfil";
    public static final String WS_OBTENER_PERFIL = "/WsObtenerPerfil";
    public static final String WS_GUARDAR_PERFIL = "/WsGuardarPerfil";
    public static final String WS_ACTUALIZAR_PERFIL = "/WsActualizarPerfil";
    public static final String WS_ELIMINAR_PERFIL = "/WsEliminarPerfil";
    public static final String WS_LISTAR_OPCIONXUSUARIO = "/WsListarOpcionXUsuario";
    // HOME
    public static final String WS_LISTAR_HOME_ACTIVIDAD = "/WsListarHomeActividad";
    public static final String WS_LISTAR_HOME_BASE = "/WsListarHomeBase";
    public static final String WS_LISTAR_HOME_ANALISTAXBASE = "/WsListarHomeAnalistaXBase";
    public static final String WS_LISTAR_HOME_CARTERA = "/WsListarHomeCartera";
    public static final String WS_LISTAR_HOME_ANALISTA = "/WsListarHomeAnalista";
    public static final String WS_LISTAR_HOME_TOPVISITA = "/WsListarTopVisitas";
    public static final String WS_LISTAR_HOME_TOPEFECTIVA = "/WsListarTopEfectivas";
    public static final String WS_LISTAR_HOME_UBICACION = "/WsListarHomeUbicaciones";
    // CARTERA
    public static final String WS_LISTAR_CARTERA = "/WsListarCartera";
    public static final String WS_LISTAR_OPORTUNIDADES = "/WsListarOportunidades";
    public static final String WS_TRANSFERIR_CLIENTES = "/WsTransferirClientes";
    public static final String WS_ASIGNAR_OPORTUNIDADES = "/WsAsignarOportunidades";
    public static final String WS_ASIGNAR_CARTERA = "/WsAsignarCartera";
    // ANALISTA
    public static final String WS_LISTAR_ANALISTA = "/WsListarAnalistas";
    public static final String WS_UBICACION_ANALISTA = "/WsObtenerUbicacionAnalista";
    // AGENDA
    public static final String WS_LISTAR_CALENDARIO = "/WsListarCalendario";
    public static final String WS_LISTAR_DISTACT_CBO = "/WsListarCboDistritoActividad";
    public static final String WS_LISTAR_CARTERA_AGENDA = "/WsListarCarteraAgenda";
    public static final String WS_AGENDAR_CITA = "/WsAgendarCita";
    // REPORTE
    public static final String WS_LISTAR_REPORTE = "/WsListarReportes";
    public static final String WS_OBTENER_REPORTE = "/WsObtenerReportes";
    public static final String WS_OBTENER_REP_VISITASREALIZADAS = "/WsObtenerReporteVisitasRealizadas";
    // PARAMETRIA
    public static final String WS_LISTAR_REAC_CONTACTADO = "/WsListarReaccionContactado";
    public static final String WS_OBTENER_REAC_CONTACTADO = "/WsObtenerReaccionContactado";
    public static final String WS_GUARDAR_REAC_CONTACTADO = "/WsGuardarReaccionContactado";
    public static final String WS_ACTUALIZAR_REAC_CONTACTADO = "/WsActualizarReaccionContactado";
    public static final String WS_ELIMINAR_REAC_CONTACTADO = "/WsEliminarReaccionContactado";
    public static final String WS_LISTAR_REAC_NOCONTACTADO = "/WsListarReaccionNoContactado";
    public static final String WS_OBTENER_REAC_NOCONTACTADO = "/WsObtenerReaccionNoContactado";
    public static final String WS_GUARDAR_REAC_NOCONTACTADO = "/WsGuardarReaccionNoContactado";
    public static final String WS_ACTUALIZAR_REAC_NOCONTACTADO = "/WsActualizarReaccionNoContactado";
    public static final String WS_ELIMINAR_REAC_NOCONTACTADO = "/WsEliminarReaccionNoContactado";
    // COMMON
    public static final String WS_CBO_AGENCIA = "/WsCboAgencias";
    public static final String WS_CBO_ANALISTAXAGENCIA = "/WsCboAnalistasXAgencia";
    public static final String WS_CMN_ACTIVIDAD = "/WsListarActividad";
    public static final String WS_CMN_RUTAS_EXCEL = "/WsListarRutasExcel";
    public static final String WS_CMN_COLORES = "/WsListarColores";
    //CONFIGURACIÓN
    public static final String WS_LISTAR_CONFIG_ACTIVIDAD = "/WsListarConfigActividad";
    public static final String WS_OBTENER_CONFIG_ACTIVIDAD = "/WsObtenerConfigActividad";
    public static final String WS_ACTUALIZAR_CONFIG_ACTIVIDAD = "/WsActualizarConfigActividad";
    public static final String WS_LISTAR_CONFIG_BASE= "/WsListarConfigBase";
    public static final String WS_OBTENER_CONFIG_BASE = "/WsObtenerConfigBase";
    public static final String WS_GUARDAR_CONFIG_BASE = "/WsGuardarConfigBase";
    public static final String WS_ACTUALIZAR_CONFIG_BASE = "/WsActualizarConfigBase";
    public static final String WS_ELIMINAR_CONFIG_BASE = "/WsEliminarConfigBase";
    // CARGA MASIVA
    public static final String WS_CARGAR_RECURRENTE = "/WsCargarRecurrente";
    public static final String WS_CARGAR_COBRANZA = "/WsCargarCobranza";
    public static final String WS_CARGAR_INACTIVO = "/WsCargarInactivo";
    public static final String WS_CARGAR_REFERIDO = "/WsCargarReferido";
    public static final String WS_LISTAR_CARGA_HISTORICO = "/WsCargaHistorica";
    public static final String WS_CARGA_ERRADA_RECURRENTE = "/WsCargaErradaRecurrente";
    public static final String WS_CARGA_ERRADA_COBRANZA = "/WsCargaErradaCobranza";
    public static final String WS_CARGA_ERRADA_INACTIVO = "/WsCargaErradaInactivo";
    public static final String WS_CARGA_ERRADA_REFERIDO = "/WsCargaErradaReferido";
    
    // SEGURIDAD SENTINEL
    public static final String WS_SEGURIDAD_NO_INGRESO = "/WsSeguridadNoIngreso";
    

}
