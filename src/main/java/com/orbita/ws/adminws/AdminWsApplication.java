package com.orbita.ws.adminws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


@SpringBootApplication
@ServletComponentScan
public class AdminWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminWsApplication.class, args);
    }

}
